# Deep learing Proposal

This folder contains the proposal for the
course deep learning which we have to submit
for revision before the 23. December 2023 or so.
Goal is to have it ready as soon as possible.

I (Nico) use the build_preview folder to build
the pdf file, the repos gitignore file will ignore
all temporary files during the compilation, so
commits should automatically only contain what is
important.
