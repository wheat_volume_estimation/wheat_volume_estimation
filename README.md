# Wheat Volume Estimations

This repo contains the code for the deep learning project in
the fall semester 2023.

## Git conventions
Please consider the following ...
1. make commits with thematic chunks (commit often and
after you implemented a new feature or made a change).
2. make meaningful commit messages
3. make a new branch and merge it afterwards when coding
4. pull before each push (don't force push)

## Coding conventions
Please keep your code ...
1. easy to read (comment, variable names)
2. modular (create methods for each implemented functionality).
3. clean (remove unnecessary lines of code)
4. consistent (naming of methods, constants, variables)
5. annotated (use pythons annotation whenever possible)

