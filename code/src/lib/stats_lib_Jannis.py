from typing import Any, Dict
import warnings
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as st
import matplotlib.patches as mpatches

from pathlib import Path
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from data_lib.data_utils import VolumeNorm
import shutil

def volume_statistics(data : pd.DataFrame, name : str, output_folder : Path):
    
    volume = np.array(data["volume"])
    
    # compute basic statistics over plants
    n_plants = volume.shape[0]
    mean = np.mean(volume)
    median = np.median(volume)
    variance = np.var(volume)
    
    mask_bad = (volume >= 8000) | (volume < 1500)
    
    # plot histogram with gaussian fit
    c = {
        "Number of plants": n_plants,
        "Mean volume": mean,
        "Median volume": median,
        "Std": np.sqrt(variance),
        "Number of outliers (<1500, >8000)": np.sum(mask_bad)
    }
    df_out = pd.DataFrame([c])
    
    x = np.linspace(np.min(volume), np.max(volume), 1000)
    norm_fit = st.norm.pdf(x, mean, np.sqrt(variance))

    fig, ax = plt.subplots()
    ax.hist(volume, bins=100, density=True, color="green", label="Volume Distribution")
    ax.plot(x,norm_fit, '-', c="red", label="Fitted Normal")
    ax.legend(loc='best', frameon=False)
    fig.savefig(f'{output_folder}/distribution_{name}.jpg')
    df_out.to_csv(f"{output_folder}/stats_{name}.csv")
    return df_out, fig

def statistics_to_df(statistics: Dict[str, Any]):
    statistics_df = pd.DataFrame()
    statistics_df["metric"] = statistics.keys()
    statistics_df["value"] = statistics.values()
    return statistics_df
    
def compare(mapping_true : pd.DataFrame, 
            mapping_predicted : pd.DataFrame, 
            name : str,
            output_name : str = "volume",
            output_folder : Path = None,
            create_plots: bool = True,
            verbose : bool = True):
    """Compute and print statistics

    Args:
        df_true (pd.DataFrame): True / measured volumes
        df_estim (pd.DataFrame): Estimated volumes
    """
    
   
    
    n_experiments = mapping_predicted.shape[0]
    
    
    mapping_predicted = mapping_predicted.loc[mapping_predicted['plant_id'].isin(mapping_true.index)]
    
    
    def get_seasons(row):
        p_id = row["plant_id"]
        range, row, p = p_id.split("_")
        range, row, p = int(range), int(row), int(p)
 
            
        if row >= 6 and row <= 10:
            if p in [1, 10, 2]:
                return 0
            elif p in [3, 4]:
                return 1
            else:
                return 2
        elif row >= 15 and row <= 19:
            if p in [1, 2, 3]:
                return 0
            elif p in [4, 5]:
                return 1
            else:
                return 2
            
        elif range >=21 and range <=23:
            if p in [5,6]: 
                return 0
            elif p in [4,3]:
                return 1
            else: 
                return 2
            
            
        else:
            #assert False, f"row was {row}"
            warnings.warn(f"Row that is not in range of 6-10, 15-19 has been found {p_id}")
            return 7
        
    
    
   
    seasons_ = mapping_predicted.apply(get_seasons, axis=1)
    seasons = seasons_.to_list()
    

    true_values = np.array(mapping_true.loc[mapping_predicted["plant_id"], output_name])
    pred_values = np.array(mapping_predicted[output_name])
    
 
    
    if "variance" in mapping_predicted.columns:
        std = np.array(mapping_predicted["variance"])

    if verbose:
        print(f"Stats for estimations")
        print(f"Number of compared experiments {mapping_predicted.shape[0]}")
        print(f"Number of total experiments {n_experiments}")

    diff_volumes = np.abs(pred_values - true_values)
    rel_diff_volumes = diff_volumes / true_values
    
    
    # Create a DataFrame
    if output_folder:
        out_data = {'plant_id': mapping_predicted["plant_id"],'True': true_values, 'Predicted': pred_values}
        if "variance" in mapping_predicted.columns:
            out_data['Confidence/Variance'] = mapping_predicted["variance"]
        save = pd.DataFrame(out_data)
        save.to_csv(f'{output_folder}/{name}_vs_true.csv', index=False)
    
    MAE = mean_absolute_error(true_values, pred_values)
    
    def scale_shift(a):
        return (a - VolumeNorm.Y_MEAN) / VolumeNorm.Y_STD
    MSE_scaled = mean_squared_error(scale_shift(true_values), scale_shift(pred_values)) * VolumeNorm.Y_STD
    MSE = mean_squared_error(true_values, pred_values)
    RMSE = np.sqrt(MSE)

    #MAPE:
    def mean_absolute_percentage_error(y_true, y_pred): 
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        mask = y_true != 0  # Avoid division by zero
        return np.mean(np.abs((y_true[mask] - y_pred[mask]) / y_true[mask])) * 100
    MAPE = mean_absolute_percentage_error(true_values, pred_values)
    r_squared = r2_score(true_values, pred_values)
    correlation = np.corrcoef(true_values, pred_values)
    cor = correlation[0,1]

    season_stats = {day: {
            "Correlation": np.corrcoef(true_values[seasons == day], pred_values[seasons == day])[0, 1],
            "MAE": np.abs(true_values[seasons == day] - pred_values[seasons == day]).mean(),
            "std_error": np.std(np.abs(true_values[seasons == day] - pred_values[seasons == day])),
        }   
        for day in np.unique(seasons)
        if np.sum(seasons == day) >= 2
    }
    mean_season_correlation = np.mean([x["Correlation"] for x in season_stats.values()])

    A = np.vstack([true_values, np.ones(len(true_values))]).T
    linregcoeff, _, _, _ = np.linalg.lstsq(A, pred_values, rcond=None)

    statistics = {
        "mean_error": np.mean(diff_volumes),
        "max_error": np.max(diff_volumes),
        "min_error": np.min(diff_volumes),
        "std_error": np.std(diff_volumes),
        "rel_mean_error": np.mean(rel_diff_volumes),
        "rel_max_error": np.max(rel_diff_volumes),
        "rel_min_error": np.min(rel_diff_volumes),
        "rel_std_error": np.std(rel_diff_volumes),
        "MAE": MAE,
        "MSE": MSE,
        "MSE_scaled": MSE_scaled,
        "RMSE": RMSE,
        "MAPE": MAPE, #“How much (what %) of the total variation in Y(target) is explained by the variation in X(regression line)”
        "R2": r_squared,
        "Correlation": cor,
        "mean_season_correlation": mean_season_correlation,
        **{f"season {season} {n}": v for season, sstats in season_stats.items() for n, v in sstats.items()},
        "Steepness": linregcoeff[0],
    }
    
    # Save the DataFrame to a CSV file with the specified path and variable name
    statistics_df = statistics_to_df(statistics)
    
    if output_folder:
        statistics_df.to_csv(f'{output_folder}/{name}_statistics.csv', index=False)
    
    if create_plots:
        # Plotting the scatter plot
        fig, ax = plt.subplots()
        if "variance" in mapping_predicted.columns:
            p80 = np.percentile(std, 80)
            min_std = std.min()
            w = 0.3
            m = (0.3 - 1) / (p80 - min_std)
            d = 1 - m * min_std
            alphas = np.clip(m * std + d, w, 1.0)
        else:
            alphas = None
        season_to_colors = {
            0: [1, 0, 0],
            1: [0, 1, 0],
            2: [0, 0, 1],
            7: [1, 0, 1]
        }
        colors = list(map(lambda x: season_to_colors[x], seasons))
        ax.scatter(true_values, pred_values, color=colors, label='True vs Predicted', marker='o', s=4, alpha=alphas)
        # Plotting the diagonal line (perfect predictions)
        ax.plot([min(true_values), max(true_values)], [min(true_values), max(true_values)], linestyle='--', color='red', label='Perfect Predictions')
        ax.plot([min(true_values), max(true_values)], [min(true_values) * linregcoeff[0] + linregcoeff[1], max(true_values) * linregcoeff[0] + linregcoeff[1]])
        # Adding labels and title
        ax.set_xlabel("True Volumes [mm³]")
        ax.set_ylabel("Predicted Volumes [mm³]")
        #plt.legend()
        # Adding R-squared value as text annotation
        ax.text(0.05, 0.9, f'Corr: {cor:.2f}', transform=plt.gca().transAxes, color='black')
        ax.set_facecolor("white")
        fig.set_facecolor("white")
        
        # Creating a custom legend
        legend_patches = [
        mpatches.Patch(color=[1, 0, 0], label="Sampling 1"),
        mpatches.Patch(color=[0, 1, 0], label="Sampling 2"),
        mpatches.Patch(color=[0, 0, 1], label="Sampling 3")
        ]
        ax.legend(handles=legend_patches, loc="lower right")
        
    else:
        fig = None
    
    if output_folder:
        file_name = f'{output_folder}/scatter_plot_{name}.png'

        # Save the plot to a file (e.g., PNG, PDF, etc.)
        fig.savefig(file_name)
        fig.tight_layout()
    
    if verbose:
        print(statistics_df)
        
    print(fig)
    print(statistics)
    
    return fig, statistics




def create_performance_bins(mapping_true : pd.DataFrame, 
            mapping_predicted : pd.DataFrame, 
            name : str,
            image_folder: Path,
            output_name : str = "volume",
            output_folder : Path = None,
            bins = 5,
            ):

    mapping_predicted = mapping_predicted.loc[mapping_predicted['plant_id'].isin(mapping_true.index)]
    true_values = np.array(mapping_true.loc[mapping_predicted["plant_id"], output_name])
    pred_values = np.array(mapping_predicted[output_name])

    diff_volumes = np.abs(pred_values - true_values)

    output_folder = output_folder / f"bins_{name}"
    output_folder.mkdir(parents=True, exist_ok=True)

    interval = [int(np.min(diff_volumes)), int(np.max(diff_volumes))]
    binsize = int((interval[1] - interval[0]) / bins)

    bins = [(start, start + binsize) for start in range(interval[0], interval[1], int(binsize))]

    for start, end in bins:
        bin_folder = output_folder / f"{start}-{end}"
        if bin_folder.exists():
            for file in bin_folder.glob("*.jpg"):
                file.unlink()
        bin_folder.mkdir(parents=True, exist_ok=True)

    for start, end in bins:
        bin_folder = output_folder / f"{start}-{end}"
        for index, (_, row) in enumerate(mapping_predicted.iterrows()):
            if start <= diff_volumes[index] < end:
                for image in row["images"]:
                    shutil.copy(image_folder / image, bin_folder)
