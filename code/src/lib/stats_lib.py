import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as st

from icecream import ic
from pathlib import Path
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from tueplots import bundles

def volume_statistics(data : pd.DataFrame, name : str, output_folder : Path):
    
    # group by plant based on image id
    id = data.index.str.extract(r'^(\d{1,2}_\d{1,2}_\d{1,2})', expand=False)
    data["p_id"] = id

    # get image stats
    mean_vol = data.groupby("p_id")["volume"].mean("vol")
    np_mean_vol = np.array(mean_vol)
    std_vol = data.groupby("p_id")["volume"].std()
    np_std_vol = np.array(std_vol)
    
    # compute basic statistics over plants
    n_plants = mean_vol.shape[0]
    mean = np.mean(np_mean_vol)
    variance = np.var(np_mean_vol)
    
    mask_bad = (mean_vol >= 8000) | (mean_vol < 1500)
    
    # plot histogram with gaussian fit
    print(f'Number of plants: {n_plants}')
    print(f'Mean volume: {mean}')
    print(f'Standard deviation in volume: {np.sqrt(variance)}')
    print(f'Number of bad points: {np.sum(mask_bad)}')
    
    x = np.linspace(np.min(np_mean_vol), np.max(np_mean_vol), 1000)
    norm_fit = st.norm.pdf(x, mean, np.sqrt(variance))

    ic(mean_vol[mask_bad])
    plt.hist(np_mean_vol, bins=100, density=True, color="green", label="Volume Distribution")
    plt.plot(x,norm_fit, '-', c="red", label="Fitted Normal")
    plt.legend(loc='best', frameon=False)
    plt.savefig(f'{output_folder}/distribution_{name}.jpg')
    
def compare(mapping_true : pd.DataFrame, 
            mapping_predicted : pd.DataFrame, 
            output_name : str,
            name : str,
            output_folder : Path = None,
            baseline : bool = False,
            verbose : bool = True):
    """Compute and print statistics

    Args:
        df_true (pd.DataFrame): True / measured volumes
        df_estim (pd.DataFrame): Estimated volumes
    """
    

    
      
    n_expreiments = mapping_predicted.shape[0]

    # Check if the filename contains "baseline"
    if baseline == True:
        # Remove rows with values smaller than 3000 in the "volume" column because segmentation failed 
        mapping_predicted = mapping_predicted[mapping_predicted['volume'] >= 3000]
    
    
    
    # Check initial DataFrame sizes
   
    # Check for valid plant_id matches
    mapping_predicted = mapping_predicted.loc[mapping_predicted['plant_id'].isin(mapping_true.index)]
   

    true_values = np.array(mapping_true.loc[mapping_predicted["plant_id"], output_name])
    pred_values = np.array(mapping_predicted[output_name])


    if verbose:
        print(f"Stats for estimations")
        print(f"Number of compared experiments {mapping_predicted.shape[0]}")
        print(f"Number of total experiments {n_expreiments}")

    diff_volumes = np.abs(pred_values - true_values)
    rel_diff_volumes = diff_volumes / true_values
    
    # Create a DataFrame
    save = pd.DataFrame({'True': true_values, 'Predicted': pred_values})
    save.to_csv(f'{output_folder}/{name}_vs_true.csv', index=False)

    # Check if the filename contains "baseline" and apply scaling 
    if baseline == True:
        mean_baseline = np.mean(output_name)
        mean_true = np.mean(output_name)
    
        #1)
        # Calculate the scaling factor using a logarithmic transformation
        #scaling_factor = np.log1p(estim_volumes.max()) / np.log1p(true_volumes.max())
        # Apply the scaling factor to the predicted values
        #estim_volumes = estim_volumes ** (1 / scaling_factor)
        
        #2)
        # Calculate the scaling factor using a square root transformation
        #scaling_factor = np.sqrt(estim_volumes.max() / true_volumes.max())
        # Apply the scaling factor to the predicted values
        #estim_volumes = estim_volumes ** (1 / scaling_factor)
        
        #3)
        #scaling_factor = mean_true/mean_baseline
        #estim_volumes = estim_volumes * scaling_factor
    
    # Calculate the mean
    #mean_y = np.mean(true_volumes)
    #MAE:
    #substract the mean of y of each predicted volume, and take the absolute value
    #abs = np.mean(np.abs(estim_volumes - mean_y))
    # Sum up the absolute values
    #sum_absolute_values = np.sum(abs)
    # Divide the sum by the number of elements
    #mean_absolute_value = sum_absolute_values / len(estim_volumes)
    print("True values")
    print(true_values)
    
    MAE = mean_absolute_error(true_values, pred_values)
    MSE = mean_squared_error(true_values, pred_values)
    RMSE = np.sqrt(MSE)

    #MAPE:
    def mean_absolute_percentage_error(y_true, y_pred): 
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        mask = y_true != 0  # Avoid division by zero
        return np.mean(np.abs((y_true[mask] - y_pred[mask]) / y_true[mask])) * 100
    MAPE = mean_absolute_percentage_error(true_values, pred_values)
    r_squared = r2_score(true_values, pred_values)
    correlation = np.corrcoef(true_values, pred_values)
    cor = correlation[0,1]
    
    #Dictionary with statistics:
    statistics = pd.DataFrame(columns=["metric", "value"])
    
    statistics.loc[0] = {"metric": "mean_error", "value": np.mean(diff_volumes)}
    statistics.loc[1] = {"metric": "max_error", "value": np.max(diff_volumes)}
    statistics.loc[2] = {"metric": "min_error", "value": np.min(diff_volumes)}
    statistics.loc[3] = {"metric": "std_error", "value": np.std(diff_volumes)}
    statistics.loc[4] = {"metric": "rel_mean_error", "value": np.mean(rel_diff_volumes)}
    statistics.loc[5] = {"metric": "rel_max_error", "value": np.max(rel_diff_volumes)}
    statistics.loc[6] = {"metric": "rel_min_error", "value": np.min(rel_diff_volumes)}
    statistics.loc[7] = {"metric": "rel_std_error", "value": np.std(rel_diff_volumes)}
    statistics.loc[8] = {"metric": "MAE", "value": MAE}
    statistics.loc[9] = {"metric": "MSE", "value": MSE}
    statistics.loc[10] = {"metric": "RMSE", "value": RMSE}
    statistics.loc[11] = {"metric": "MAPE", "value": MAPE} #“How much (what %) of the total variation in Y(target) is explained by the variation in X(regression line)”
    statistics.loc[12] = {"metric": "R2", "value": r_squared}
    statistics.loc[13] = {"metric": "Correlation", "value": cor}
    
    # Save the DataFrame to a CSV file with the specified path and variable name
    statistics.to_csv(f'{output_folder}/{name}_statistics.csv', index=False)
    
    #setting = bundles.icml2022(usetex=False)
    #plt.rcParams.update(setting)
    # Plotting the scatter plot
    fig, ax = plt.subplots()
    ax.scatter(true_values, pred_values, color='black', label='True vs Predicted', marker='o', s=4)
    # Plotting the diagonal line (perfect predictions)
    ax.plot([min(true_values), max(true_values)], [min(true_values), max(true_values)], linestyle='--', color='red', label='Perfect Predictions')
    # Adding labels and title
    ax.set_xlabel('True Volumes [mm³]')
    ax.set_ylabel('Predicted Volumes [mm³]')
    #plt.legend()
    # Adding R-squared value as text annotation
    ax.text(0.05, 0.9, f'Corr: {cor:.2f}', transform=plt.gca().transAxes, color='black')
    ax.set_facecolor("white")
    fig.set_facecolor("white")
    
    #name to save
    file_name = f'{output_folder}/scatter_plot_{name}.png'

    # Save the plot to a file (e.g., PNG, PDF, etc.)
    fig.savefig(file_name)
    fig.tight_layout()
    
    if verbose:
        print(statistics)
        
    return fig, statistics