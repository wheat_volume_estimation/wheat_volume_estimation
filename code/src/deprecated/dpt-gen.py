from transformers import DPTImageProcessor, DPTForDepthEstimation, DPTFeatureExtractor
import glob
import torch
import numpy as np
from PIL import Image
from pathlib import Path, WindowsPath
from icecream import ic


class DptGenerator:
    
    def __init__(self, inputPath : str | Path, outputPath : str | Path) -> None:
        """
        Class to generate depth-images from given images

        Args:
            inputPath (str | Path): Path to the images which should be transformed to depth image
            outputPath (str | Path): Path to folder where the depth-images should be stored
        """
        self.inputPath = Path(inputPath)
        self.outputPath = Path(outputPath)
        
        # create output if necessary
        self.outputPath.mkdir(parents=True, exist_ok=True)
        self.prediction = None
    
    
    def generate(self) -> None:
        """
        Function which generates the depth-image predictions
        """
        
        # Get a list of all image files in the specified path
        image_files = [f for f in self.inputPath.iterdir() if f.suffix in  ['.png', '.jpg', '.jpeg']]
        
        for file in image_files:
            image_path = file.absolute()

            # Open the image using PIL
            img = Image.open(image_path)
            
            # Convert the image to RGB if it's grayscale
            if img.mode == 'L':
                img = img.convert('RGB')
            ic(img)

            # Process the batch of images
            #feature_extractor = DPTFeatureExtractor.from_pretrained("Intel/dpt-hybrid-midas")
            #model = DPTForDepthEstimation.from_pretrained("Intel/dpt-hybrid-midas", low_cpu_mem_usage=True)
            processor = DPTImageProcessor.from_pretrained("Intel/dpt-large")
            model = DPTForDepthEstimation.from_pretrained("Intel/dpt-large")

            # prepare images for the model
            #inputs = feature_extractor(images=img, return_tensors="pt")
            inputs = processor(images=img, return_tensors="pt")

            with torch.no_grad():
                outputs = model(**inputs)
                predicted_depth = outputs.predicted_depth

            # interpolate to original size
            prediction_batch = torch.nn.functional.interpolate(
                predicted_depth.unsqueeze(1),
                size=img.size[::-1],
                mode="bicubic",
                align_corners=False,
            )
            self.save(prediction_batch, file)
            
            # Append the batch of predictions to the overall list
            if self.prediction is None:
                self.prediction = prediction_batch
            else:
                self.prediction = torch.cat([self.prediction, prediction_batch], dim=0)
        
        np.save("predictions.npy", arr=self.prediction)
    
    def save(self, prediction : torch.Tensor, image_file : Path) -> None:
        """
        Function which saves the generated depth-images

        Args:
            predictions (torch.Tensor): Predicted image
            image_file(Path): Original image path
        """
        # visualize the prediction
        output = prediction.squeeze().cpu().numpy()

        formatted = (output * 255 / np.max(output)).astype("uint8")
        depth = Image.fromarray(formatted)

        path = Path(self.outputPath, image_file.name)

        try:
            depth.save(path)
            ic("saved at: ", path)
        except Exception as e:
            ic(e, path)
                
        
if __name__ == "__main__":
    
    # when using Path, the paths also work for windows but 
    # always use / instead of \ because the latter doesn't work on mac and linux
    generator = DptGenerator( Path("data/test"), Path("data/dpt_images"))
    generator.generate()