

import warnings
from data_lib import data_sets_Jannis, split_dataset_Jannis, data_utils
from matplotlib import pyplot as plt
import model_lib.neural_nets 
import lib.stats_lib_Jannis as stats_lib
import numpy as np
import pandas as pd
import time
import torch
import wandb
import time 
from accelerate import Accelerator
import os

from data_lib.data_saver import Saver
from icecream import ic
from model_lib.inferrer_Jannis import Inferrer
from pathlib import Path
from torch.utils.data import DataLoader, Subset
from tqdm import tqdm
from typing import Any, Callable, Literal, Tuple, List, Dict, Optional
from wandb.wandb_run import Run
import copy
from accelerate.utils import set_seed

# Define a data loader for the dataset
def collate_fn(batch : List[Tuple[torch.Tensor, float, torch.Tensor]]):

    
    tensors, labels, masks, loss_scale = zip(*batch)
    

    # Check for empty tensors
    if any(t.numel() == 0 for t in tensors):
        raise ValueError("Empty tensor found in feature tensors!")

    if any(m.numel() == 0 for m in masks):
        raise ValueError("Empty tensor found in masks!")
    
    tensors = torch.stack(tensors=tensors, dim=0)
    labels = torch.tensor(labels, dtype=tensors.dtype)
    masks = torch.stack(tensors=masks, dim=0)
    loss_scale = torch.tensor(loss_scale, dtype=tensors.dtype)
    return tensors, labels, masks, loss_scale

class VolumeMapping:
    def __init__(self, mapping_path: Path | str):
        mapping_path = Path(mapping_path)
        self.save_mapping_plants = mapping_path / "mapping_all_plants.json"
        self.save_mapping_train = mapping_path / "mapping_train.json"
        self.save_mapping_val = mapping_path / "mapping_val.json"
        self.save_mapping_test = mapping_path / "mapping_test.json"
        self.save_mapping_all_val = mapping_path / "mapping_all_val.json"

    def create_split(self, mapping_file: str | Path, random_split_generator : np.random.Generator, min_sequence_length_train: int,
                     min_sequence_length_test, test_set_size=250, val_set_size=150):
        df_mapping = pd.read_csv(mapping_file)
        split_dataset_Jannis.compute_and_store_split(
            df_mapping=df_mapping,
            mapping_plants_path=self.save_mapping_plants,
            mapping_train_path=self.save_mapping_train,
            mapping_val_path=self.save_mapping_val,
            mapping_test_path=self.save_mapping_test,
            mapping_plants_val_path=self.save_mapping_all_val,
            value_column="volume",
            random_generator=random_split_generator,
            test_set_size=test_set_size,
            val_set_size=val_set_size,
            min_view_train=min_sequence_length_train,
            min_view_test=min_sequence_length_test,
            verbose=True,
        )

    def adapt_new_dataset(self, mapping_path_new: Path | str, mapping_file_new: Path | str, min_sequence_length_train: int,
                     min_sequence_length_test):
        mapping_path_new = Path(mapping_path_new)
        mapping_path_new.mkdir(parents=True, exist_ok=True)

        new_mapping = VolumeMapping(mapping_path_new)

        def create_sub_df_from_indices(new_df: pd.DataFrame, reference_df: str, out_df, name):
            reference_df = pd.read_json(reference_df, orient='index', convert_axes=False, dtype={"plant_id": str})
            sub_df = new_df.loc[new_df.index.intersection(reference_df.index)]
            sub_df = sub_df.apply(split_dataset_Jannis.remove_artificial, axis=1)
            sub_df = sub_df[sub_df["images"].apply(lambda x: len(x) >= min_sequence_length_test)]
            missing_count = len(reference_df.index.difference(sub_df.index))
            print(f"# Removed plants {name}: {missing_count}")

            sub_df.to_json(out_df, orient="index")

        def create_exclusive_sub_df(new_df: pd.DataFrame, sequence_of_dfs: List[str], out_df, name):
            all_indices = pd.Index([])
            for df in sequence_of_dfs:
                df = pd.read_json(df, orient='index', convert_axes=False, dtype={"plant_id": str})
                all_indices = all_indices.union(df.index)

            exclusive_df = new_df.loc[~new_df.index.isin(all_indices)]
            exclusive_df = exclusive_df[exclusive_df["images"].apply(lambda x: len(x) >= min_sequence_length_train)]
            print(f"# Plants added {name}: {len(exclusive_df)}")

            exclusive_df.to_json(out_df, orient="index")

        csv_data = pd.read_csv(mapping_file_new)
        df_new = split_dataset_Jannis.compute_plant_mapping_(csv_data)

        df_new.to_json(new_mapping.save_mapping_plants, orient="index")
        create_sub_df_from_indices(df_new, self.save_mapping_test, new_mapping.save_mapping_test, "test")
        create_sub_df_from_indices(df_new, self.save_mapping_val, new_mapping.save_mapping_val, "val")
        create_exclusive_sub_df(df_new, [new_mapping.save_mapping_test, new_mapping.save_mapping_val], new_mapping.save_mapping_train, "train")
        all_val = split_dataset_Jannis.create_all_val([new_mapping.save_mapping_train, new_mapping.save_mapping_test, new_mapping.save_mapping_val], min_sequence_length_test)
        all_val.to_json(new_mapping.save_mapping_all_val, orient="index")


class ExperimentPaths:
    def __init__(self, output_path: Path, verbose: bool):
        ExperimentPaths.check_create_folder(output_path, "output", verbose)

        self.output_path = output_path
        self.save_predictions_path = output_path / "predictions.json"
        self.charts_output_path = output_path / "results"

        ExperimentPaths.check_create_folder(self.charts_output_path, "charts output", verbose, False)

    @staticmethod
    def check_create_folder(path: Path, path_description: str, verbose: bool, block_existing: bool = True):
        if path.exists() and path.is_dir() and block_existing:
            print(f"{path_description} path {str(path)} already existed.")
            input("Press enter to continue with overwrite")
        elif path.exists() and not path.is_dir():
            print(f"{path_description} path {str(path)} is not a directory! Aborting...")
            exit(1)
        elif verbose:
            print(f"Creating {path_description} directory {str(path)}.")
        path.mkdir(parents=True, exist_ok=True)

class TrainRunSetup:
    def __init__(
                self,
                # path and metadata
                dataset_path : Path | str,
                transform_train : list,
                transform_eval: list,
                output_path : Path | str,
                mapping_dir : Path | str,
                pretrained_model : torch.nn.Module,
                # model input and model            
                max_seqence_lenght : int,
                min_seqence_lenght : int,
                random_seqence_lenght : bool,
                num_workers : int,
                # evaluation
                eval_random_sequence_len: bool,
                eval_min_sequence_len: int,
                eval_max_sequence_len: int,
                # feature cache
                cache_dir: str | Path,
                n_dupli_train: int = 1,
                # logging
                wandb_config: Dict[str, Any] = {},
                wandb_mode: str = "disabled",
                verbose : bool = False,
                quick_test_run: bool = False
                ):
        self.dataset_path = Path(dataset_path)
        output_path = Path(output_path)
        mapping_dir = Path(mapping_dir)
        value_column = "volume"
        cache_dir = Path(cache_dir)
        self.cache_path_train = cache_dir / "train.cache"
        self.cache_path_val = cache_dir / "val.cache"
        self.cache_path_test = cache_dir / "test.cache"
        self.cache_path_all = cache_dir / "all.cache"
        self.quick_test_run = quick_test_run
        
        # compute output paths for mapping files and predictions
        self.paths = ExperimentPaths(output_path, verbose)
        self.mapping = VolumeMapping(mapping_dir)
        

        # Saver Class for saving the output data
        self.saver = Saver(output_path / 'saved_models') 
        
        #---------------------------------------------------#
        # ------------------ Compute split ------------------#
        # ---------------------------------------------------#
        # generate data split
        

        self.num_workers = num_workers
            
        dataset_params = {
            'value_column' : value_column,
            'max_seq_len' : max_seqence_lenght,
            'min_seq_len' : min_seqence_lenght,
            'Y_SCALE' : data_utils.VolumeNorm.Y_STD,
            'Y_SHIFT' : data_utils.VolumeNorm.Y_MEAN,
            'dtype' : torch.float32,
        }
        self.dataset_params_train = dataset_params.copy()
        self.dataset_params_train["transform"] = transform_train
        self.dataset_params_train['random_seq_len'] = random_seqence_lenght
        self.train_dataset = data_sets_Jannis.MultiImageTrainDataset(image_dir=self.dataset_path,
                                                        plant_mapping_path=self.mapping.save_mapping_train,
                                                        **self.dataset_params_train)
        if verbose:
            print("Getting Train Cache")
        self.train_dataset.create_or_load_cache(pretrained_model, self.cache_path_train, n_dupli_train, num_workers=num_workers)

        self.dataset_params_eval = dataset_params.copy()
        self.dataset_params_eval["transform"] = transform_eval
        self.dataset_params_eval['random_seq_len'] = eval_random_sequence_len
        self.dataset_params_eval['max_seq_len'] = eval_max_sequence_len
        self.dataset_params_eval['min_seq_len'] = eval_min_sequence_len
        self.pretrained_model = pretrained_model
        
        self.run = wandb.init(
            project="wheat-volume-prediction",
            group="FIP_models",
            config=wandb_config | {
                "dataset" : type(self.train_dataset).__name__,
                "dataset_params_train" : self.dataset_params_train,
                "dataset_params_eval": self.dataset_params_eval,
                "dataset_path" : str(self.dataset_path),
            }, mode=wandb_mode, dir=self.paths.output_path)

        # save train, val, test split files as artifacts
        split_artifact = wandb.Artifact(name="data_split", type='mapping')
        split_artifact.add_file(str(self.mapping.save_mapping_train), name=self.mapping.save_mapping_train.name)
        split_artifact.add_file(str(self.mapping.save_mapping_val), name=self.mapping.save_mapping_val.name)
        split_artifact.add_file(str(self.mapping.save_mapping_test), name=self.mapping.save_mapping_test.name)
        self.run.log_artifact(split_artifact)

        self.verbose = verbose

    def finish(self):
        self.run.finish()

    def create_volume_statistics(self):
        def r(p):
            return pd.read_json(p, orient='index', convert_axes=False)
        vol_path = self.paths.output_path / "volume_statistics"
        self.paths.check_create_folder(vol_path, "Volume statistics", False)
        for name, df in [("train", r(self.mapping.save_mapping_train)), ("test", r(self.mapping.save_mapping_test)), ("val", r(self.mapping.save_mapping_val))]:
            df, fig = stats_lib.volume_statistics(df, name, vol_path)
            if self.verbose:
                print(df)
            results = wandb.Table(dataframe=df)
            self.run.log({f"results/{name}_volume_stats" : results})
            self.run.log({f"plots/{name}_volume_distribution" : wandb.Image(fig)})

class Evaluator:
    def __init__(self,
                 setup: TrainRunSetup,
                 split: Literal["all", "test", "val", "train"],
                 multi_use: bool = True,
                 ):
        self.mapping = setup.mapping
        self.paths = setup.paths
        self.setup = setup
        self.multi_use = multi_use

        # The index of the mapping files are plant_ids so convert_axes = False
        # otherwise it's converted to int
        self.df_train = pd.read_json(self.mapping.save_mapping_train, orient='index', convert_axes=False)
        self.df_test = pd.read_json(self.mapping.save_mapping_test, orient='index', convert_axes=False)
        self.df_val = pd.read_json(self.mapping.save_mapping_val, orient='index', convert_axes=False)
        def check_warn_artifical(df):
            if df["artificial_mask"].explode().any():
                warnings.warn("One of test or val contain artifical images!!! Sure you want do this?")

        check_warn_artifical(self.df_test)
        check_warn_artifical(self.df_val)
        self.set_split(split)
        
    
    def set_split(self, split: List[Literal["all", "test", "val", "train"]]):
       
        if split == "all":
            ds_map = self.mapping.save_mapping_all_val
            self.split_list = [(self.df_train, "train"), (self.df_test, "test"), (self.df_val, "val")]
            cache_path = self.setup.cache_path_all
        elif split == "test":
            ds_map = self.mapping.save_mapping_test
            self.split_list = [(self.df_test, "test")]
            cache_path = self.setup.cache_path_test
        elif split == "val":
            ds_map = self.mapping.save_mapping_val
            self.split_list = [(self.df_val, "val")]
            cache_path = self.setup.cache_path_val

        elif split == "train":
            ds_map = self.mapping.save_mapping_train
            self.split_list = [(self.df_train, "train")]
            cache_path = self.setup.cache_path_train
            
        else: 
                return
        
       

        self.test_dataset = data_sets_Jannis.MultiImageTrainDataset(image_dir=self.setup.dataset_path,
                                                        plant_mapping_path=ds_map,
                                                        validation_mode=True,
                                                        **self.setup.dataset_params_eval)
        
        print("Test dataset, check if there are no artificial images")
        print(f"Total samples in test_dataset: {len(self.test_dataset)}")
        for i in range(min(5, len(self.test_dataset))):  # Print up to 5 samples
            sample = self.test_dataset[i]
            print(f"Sample {i}: {sample}")



        if self.setup.verbose:
            print(f"Getting {split} cache")
        self.test_dataset.create_or_load_cache(pretrained_model=self.setup.pretrained_model,
                                        cache_path=cache_path,
                                        n_duplicates=1)
        self.inferrer = Inferrer(self.test_dataset, self.setup.num_workers if self.multi_use else 0, self.multi_use and self.setup.num_workers > 0)
        
    def evaluate(self,
                 model: str | Path | torch.nn.Module | model_lib.neural_nets.FakeNNModule,
                 model_description: str = "",
                save: bool = True,
                verbose: bool = False,
                create_performance_bins = False
                ):




        experiment_output = self.inferrer.infer_experiment(model=model, verbose=verbose)
        
   
        
        
        # --------- Save and log experiment output ----------#
        if save:
            experiment_output.to_json(self.paths.save_predictions_path, orient='index')
            prediction_artifact = wandb.Artifact(name="predictions", type='mapping')
            prediction_artifact.add_file(str(self.paths.save_predictions_path), name="predictions.json")
            self.setup.run.log_artifact(prediction_artifact)

        # --------- Compute experiment statistics -----------#
        # Make sure plant_ids are not converted to int in a weird manner
        if save:
            df_pred = pd.read_json(self.paths.save_predictions_path, orient='index' , dtype={'plant_id' : str})
        else:
            df_pred = experiment_output
        
        all_stats = {}
        
        
        
        
        for df_true, name in self.split_list:
            
                fig, stat = stats_lib.compare(mapping_true=df_true,
                                                                mapping_predicted=df_pred,
                                                                name=name,
                                                                output_folder=self.paths.charts_output_path if save else None,
                                                                create_plots=save,
                                                                verbose=verbose)
                        
               
                
                if save:
                        statistics_df = stats_lib.statistics_to_df(stat)
                        results = wandb.Table(dataframe=statistics_df)
                        self.setup.run.log({f"results/{model_description}_{name}_stats" : results})
                        self.setup.run.log({f"plots/{name}" : fig})
                        if create_performance_bins:
                                if name != "train": # Skip train by default (it's a lot of images)
                                        stats_lib.create_performance_bins(mapping_true=df_true,
                                                                        mapping_predicted=df_pred,
                                                                        name=name,
                                                                        image_folder=self.setup.dataset_path,
                                                                        output_folder=self.paths.charts_output_path)
                all_stats[name] = stat

        return all_stats

"""
model : torch.nn.Module,
              model_params : Dict[str, any],
              # training
              optimizer : torch.optim.Optimizer,
              optimizer_params : Dict[str, any],
              scheduler : Optional[torch.optim.lr_scheduler.LRScheduler],
              scheduler_params : Dict[str, any],
              loss_function : torch.nn.Module,
              num_epochs : int,
"""

def train_run_nn(
              train_setup: TrainRunSetup,
              # model input and model            
              model_fun: Callable[[TrainRunSetup, int], 
                                  Tuple[torch.nn.Module, Dict[str, any], torch.optim.Optimizer, Optional[torch.optim.lr_scheduler.LRScheduler], torch.nn.Module, int]],
              start: int = 0,
              num_phases: int = 1,
              batch_size : int = 256):
    """Compute one training run including training, inference and computation of stats

    Args:
        train_setup: A setup containing data and settings to be trained on
        model_fun: A function returning the model, optimizer, loss and so on to be trained on for a given distillation phase. 
        start: The distillation phase to start from
    """
    
    print("Available GPUs:", torch.cuda.device_count())
    print("Current Device:", torch.cuda.current_device())
    print("Device Name:", torch.cuda.get_device_name(torch.cuda.current_device()))
    
    if start > 0: # Restarting from a trained model. First infers the unlabeled data, then starts at given phase
        restart = True
        start -= 1
    else:
        restart = False
    set_seed(1071997)

    for distill_phase in range(start, num_phases):
        model, model_params, optimizer, scheduler, loss_function, num_epochs = model_fun(train_setup, distill_phase)

        if num_epochs > 0:
            # There is unlabeled data and self-distillation will be performed
            if "labeled" in train_setup.train_dataset.plant_mapping.columns and not train_setup.train_dataset.plant_mapping["labeled"].all():
                self_distillation = True
                all_index = np.arange(0, len(train_setup.train_dataset))
                if distill_phase == 0:
                    subset = all_index[train_setup.train_dataset.plant_mapping["labeled"]]
                else:
                    subset = None
                subset_nolabel = all_index[~train_setup.train_dataset.plant_mapping["labeled"]]
            else:
                self_distillation = False
                subset = None

            if restart:
                best_model, best_model_epoch, best_stats = model, 0, {}
            else:
                model, best_model, best_model_epoch, best_stats = train_model_nn(
                    train_setup=train_setup,
                    model=model,
                    optimizer=optimizer,
                    loss_function=loss_function,
                    scheduler=scheduler,
                    num_epochs=num_epochs,
                    batch_size=batch_size,
                    data_subset=subset
                )

            if self_distillation:
                if train_setup.verbose or train_setup.quick_test_run:
                    print(f"Val stats for distillation phase {distill_phase}")
                    print(best_stats)
                if distill_phase != num_phases - 1:
                    # Infer volumes for the unlabeled dataset
                    train_setup.train_dataset.validation_mode = True
                    distill_inferrer = Inferrer(Subset(train_setup.train_dataset, subset_nolabel), 0, False)
                    r = distill_inferrer.infer_experiment(best_model, train_setup.verbose)
                    train_setup.train_dataset.validation_mode = False
                    merged = train_setup.train_dataset.plant_mapping.merge(r, on="plant_id", how="left", )
                    train_setup.train_dataset.plant_mapping.loc[pd.notna(merged["volume_y"]), "volume"] = merged.loc[pd.notna(merged["volume_y"]), "volume_y"]
                    train_setup.train_dataset.update_labels()
                if restart:
                    restart = False
                    continue
            else:
                break
        else:
            best_model = model
            best_model_epoch = 0

        
        # Save models locally on disk
        if num_epochs > 0 and not train_setup.quick_test_run:
            _ = train_setup.saver.save(model=model,
                                    model_params=model_params,
                                    optimizer=None,
                                    optimizer_params=None,
                                    lr_scheduler=None,
                                    lr_scheduler_params=None,
                                    force_name=f"last_model_phase{distill_phase}.pth")
        
            _ = train_setup.saver.save(model=best_model,
                                        model_params=model_params,
                                        optimizer=None,
                                        optimizer_params=None,
                                        lr_scheduler=None,
                                        lr_scheduler_params=None,
                                        force_name=f"best_model_phase{distill_phase}.pth")
        else:
            best_model = model

        # End of distillation loop
        
    # Compute Predictions for all data including test data
    if not train_setup.quick_test_run:
        evaluator = Evaluator(train_setup,
                            split="all",
                            multi_use=False)
        
        evaluator.evaluate(best_model, f"best_model_epoch{best_model_epoch}", True, train_setup.verbose)
    
    # Finish the run
    train_setup.finish()

def is_better_model(stats, best_stats):
    corr_improve = stats["val"]["Correlation"] - best_stats["val"]["Correlation"]
    mae_improve =  best_stats["val"]["MAE"] - stats["val"]["MAE"]
    steep_improve = abs(1 - best_stats["val"]["Steepness"]) - abs(1 - stats["val"]["Steepness"])
    rate = corr_improve * (50 / 0.03) + mae_improve + steep_improve * (90 / 0.1)
    return rate > 0 or torch.isnan(torch.tensor(rate))

def train_model_nn(train_setup: TrainRunSetup,
                model: torch.nn.Module,
                optimizer: torch.optim.Optimizer,
                loss_function: torch.nn.Module,
                scheduler: torch.optim.lr_scheduler.LRScheduler,
                num_epochs: int,
                batch_size: int = 256,
                data_subset: List[int] = None,
                device: torch.device = torch.device('cpu')) -> Tuple[torch.nn.Module, torch.nn.Module, int]:
    """Train the given model and log the epoch losses.

    Args:
        model (torch.nn.Module): Neural network to be trained.
        optimizer (torch.optim.Opitmizer): optimizer used for weight updating.
        loss_funciton (torch.nn.Module): loss function to be minimized.
        scheduler (torch.optim.lr_scheduler): mechanisim to updated the learning rate over the epochs.
        num_epochs (int): Number of epochs to be trained
        batch_size (int, optional): Batch size for training. Defaults to 256.
        device (torch.device, optional): Device on which to do computations. Defaults to torch.device('cpu')
    Returns:
        the trained final model, the model with highest correlation on val, and the epoch the best model was created
    """
    
    import torch

    

    
    accelerator = Accelerator()
    if data_subset is not None:
        tds = Subset(train_setup.train_dataset, data_subset)
        vols = train_setup.train_dataset.plant_mapping.loc[data_subset, "volume"]
    else:
        tds = train_setup.train_dataset

        vols = train_setup.train_dataset.plant_mapping.loc[:, "volume"]
        
       


    train_loader = DataLoader(dataset=tds,
                              batch_size=batch_size,
                              num_workers=train_setup.num_workers,
                              persistent_workers=num_epochs > 1 and train_setup.num_workers > 0,
                              pin_memory=True,
                              shuffle=True,
                              collate_fn=collate_fn)
    
    
    evaluator = Evaluator(train_setup, split="val", multi_use=True)
    track_test_correlation = False
    if track_test_correlation:
        test_evaluator = Evaluator(train_setup, split="test", multi_use=True)

    model, train_loader = accelerator.prepare(
        model, train_loader
    )
 
    # training loop (tqdm is just for nice output but deactivated)
    best_stats = None
    best_model = None
    best_model_epoch = None
    for epoch in tqdm(range(num_epochs), disable=True):
        start_epoch = time.time()
        
        # Training
        eval_time = 0.0
        epoch_train_loss = 0.0
        
        model.train()
        for features, labels, mask, loss_scale in train_loader:
            
            start_eval_time = time.time()
            outputs = model(features, mask)
            eval_time += time.time() - start_eval_time
            loss = loss_function(outputs, labels, loss_scale)

            optimizer.zero_grad()
            accelerator.backward(loss)
            if accelerator.sync_gradients:
                accelerator.clip_grad_norm_(model.parameters(), 0.1)

            optimizer.step()
            epoch_train_loss += loss.item()
        if scheduler is not None:
            scheduler.step()

        stats = evaluator.evaluate(model)
        if track_test_correlation:
            test_stats = test_evaluator.evaluate(model)
        
        time_epoch = time.time() - start_epoch

        train_loss = epoch_train_loss / len(train_loader)
        
         # ReduceLROnPlateau step requires the validation loss
        #if scheduler is not None:
        #    scheduler.step(val_loss * loss_scaling)  # Pass the validation loss to the scheduler
        
        if best_stats is None or is_better_model(stats, best_stats):
            best_stats = stats
            best_model = copy.deepcopy(model).cpu()
            best_model_epoch = epoch

        learning_rate = optimizer.param_groups[0]['lr']
        if train_setup.verbose:
            print(f'\t{"Time: ":7s}{time_epoch:>6.4f}',end='')
            print(f'\t{"Evaluation time: ":7s}{eval_time:>6.4f}')
            print(f'{"epoch":7s}{epoch:>5n}\t{"Train Loss:":11s}{train_loss * data_utils.VolumeNorm.Y_STD:>7.4f}', end="")
            print(f'\t{"Val Correlation:":10}{stats["val"]["Correlation"]:>7.4f}', end="")
            print(f'\t{"Val Steepness:":10}{stats["val"]["Steepness"]:>7.4f}', end="")
            print(f'\t{"Val MAE:":10}{stats["val"]["MAE"]:>7.4f}', end="")
            if track_test_correlation:
                print(f'\t{"Test Correlation:":10}{test_stats["test"]["Correlation"]:>7.4f}', end="")
            print(f'\t{"Current lr:":>12s}{learning_rate:>10.0e}')        

        d_log = {
            "epoch": epoch,
            "learning_rate": learning_rate, 
            "train_loss": train_loss * data_utils.VolumeNorm.Y_STD,
            "val_loss": stats["val"]["MSE_scaled"],
            "val_correlation": stats["val"]["Correlation"],
            "epoch_time": time_epoch, 
            "eval_time": eval_time
        } 
        d_log = d_log | ({"test_correlation": test_stats["test"]["Correlation"]} if track_test_correlation else {})
        train_setup.run.log(d_log)

    if train_setup.quick_test_run or train_setup.verbose:
        print(f"Best model in epoch {best_model_epoch}")
        print(f'{"Val Correlation:":10}{best_stats["val"]["Correlation"]:>7.4f}')
    
    return model, best_model, best_model_epoch, best_stats

def train_model_fakenn(train_setup: TrainRunSetup,
                        model: model_lib.neural_nets.FakeNNModule,
                        num_passes: int = 1):

    # get data loaders
    quickstart = False
    train_loader = DataLoader(dataset=train_setup.train_dataset,
                              batch_size=16,
                              shuffle=True,
                              num_workers=1 if quickstart else 3,
                              persistent_workers=not quickstart,
                              collate_fn=collate_fn)
    
    with torch.no_grad():
        for i in tqdm(range(num_passes)):

            model.train()
            features = []
            labels = []
            mask = []
            for features_, labels_, mask_ in train_loader:
                features.append(features_)
                labels.append(labels_)
                mask.append(mask_)
            features = torch.concat(features, dim=0)
            labels = torch.concat(labels, dim=0)
            mask = torch.concat(mask, dim=0)
            model.train_call(features, mask, labels)
    
    evaluator = Evaluator(train_setup,
                          split="all", multi_use=False)
    evaluator.evaluate(model, "final_model", True, train_setup.verbose)
