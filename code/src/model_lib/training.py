from scipy.spatial import transform
import data_lib.data_sets as data_sets
import data_lib.split_dataset as split_dataset
import lib.stats_lib as stats_lib
import numpy as np
import pandas as pd
import time
import torch
import wandb
import time 
from torch import nn
from accelerate import Accelerator

from data_lib.data_saver import Saver
from icecream import ic
from model_lib.inferrer import Inferrer, ExperimentCreator
from pathlib import Path
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm
from typing import Tuple, List, Dict, Optional
from wandb.wandb_run import Run


def train_run(# path and metadata
              dataset_path : Path | str,
              pretrained_transform : list,
              output_path : Path | str,
              mapping_file : Path | str,
              value_column : str,
              project_name : str,
              run_notes : str,
              # embeddings and data augmentation
              random_split_generator : np.random.Generator,
              augmentation_factor : int,
              image_rotations : bool,
              vol_augmentation : bool, 
              aug_volume : float,
              noise_std : float,
              pretrained_model : torch.nn.Module,
              # model input and model            
              max_seqence_lenght : int,
              min_seqence_lenght : int,
              random_seqence_lenght : bool,
              model : torch.nn.Module,
              model_params : Dict[str, any],
              # training
              optimizer : torch.optim.Optimizer,
              optimizer_params : Dict[str, any],
              scheduler : Optional[torch.optim.lr_scheduler.LRScheduler],
              scheduler_params : Dict[str, any],
              loss_function : torch.nn.Module,
              num_epochs : int,
              batch_size : int,
              num_worker : int,
              # evaluation
              experiment_creator : ExperimentCreator,
              verbose : bool = False, 
              uncertainty: bool = False):
    """Compute one training run including training, inference and computation of stats

    Args:
        data_path (Path | str): Directory of the dataset
        pretrained_transform (list): Model specific transform settings which should include crop, normalize and resize.
        output_path (Path | str): Directory to store all the output
        mapping_file (Path | str): csv file containing Image -> Plant / value mapping 
        value_column (str): Column in mapping file to be regarded as value
        project_name (str): Project name for logging on wandb
        run_notes (str): Any notes that will be logged in wandb describing the run

        random_split_generator (np.random.Generator): Randomness used to create the train / val / test split
        augmentation_factor (int): TODO remove (for now set it to 1) ?!
        image_rotations (bool): Rotate the images during training in every epoch randomly
        vol_augmentation (bool) : Augmentation of large volumes
        aug_volume (float) : threshold of large volumes
        noise_std (flaot): Std of noise added to large volumes
        pretrained_model (torch.nn.Module): Pretrained model to extract features from.
        max_seqence_lenght (int): Maximal seqence lenght for one plant
        min_seqence_lenght (int): Minimal seqence lenght for one plant 
        random_seqence_lenght (bool): Whether or not to use random seqence lenth
        model (torch.nn.Module): model to be trained
        model_params (Dict[str, any]): model parameters with which the model was initialized
        optimizer (torch.optim.Optimizer): optimizer which trained the model
        optimizer_params (Dict[str, any]): initialization parameters for optimizer
        scheduler (torch.optim.lr_scheduler.LRScheduler | None): scheduler to adapt learning rate
        scheduler_params (Dict[str, any]): parameters for scheduler
        loss_function (torch.nn.Module): loss function used for training
        num_epochs (int): number of epochs
        batch_size (int): batch size
        num_worker (int): number of workers
        experiment_creator (ExperimentCreator): experiment creator for inference
        verbose (bool, optional): Defaults to False.
        uncertainty (bool): Default to False, if model should also predict uncertainty 
    """

    
    # ---------------------------------------------------#
    # ----------- Handle paths, in / outputs ------------#
    # ---------------------------------------------------#
    # convert to Path variables in case of given strings
    dataset_path = Path(dataset_path)
    output_path = Path(output_path)
    mapping_file = Path(mapping_file)

    if output_path.exists() and output_path.is_dir() and verbose:
        print(f"Output path {str(output_path)} already existed. Continue...")
    elif output_path.exists() and not output_path.is_dir():
        print(f"Output path {str(output_path)} is not a directory! Aborting...")
        return
    elif verbose:
        print(f"Creating output directory {str(output_path)}.")
    output_path.mkdir(parents=True, exist_ok=True)

    # Check mapping file
    if not mapping_file.exists():
        print(f'Mapping file at {str(mapping_file)} does not exits. Aborting...')
        return
    try:
        df_mapping = pd.read_csv(mapping_file)
    except Exception as e:
        print((f"Error while reading the mapping file. Aborting...\n\n"
               f"Got the following error {str(e)}"))
        return
    
    
    

    # compute output paths for mapping files and predictions
    save_mapping_plants = output_path / "mapping_all_plants.json"
    save_mapping_train = output_path / "mapping_train.json"
    save_mapping_val = output_path / "mapping_val.json"
    save_mapping_test = output_path / "mapping_test.json"
    save_predictions_path = output_path / "predictions.json"

    # Saver Class for saving the output data
    saver = Saver(output_path / 'saved_models') 

    # Create directory to store evaluation plots and statistics
    charts_output_path = output_path / "results"
    if charts_output_path.exists() and charts_output_path.is_dir():
        print((f"Output path {str(charts_output_path)} already existed. Continue...\n"
               f"Press ctrl-C to interrupt process if you wish to abort."))
    elif charts_output_path.exists() and not charts_output_path.is_dir():
        print(f"Output path {str(charts_output_path)} is not a directory! Aborting...")
        return
    elif verbose:
        print(f"Creating output directory {str(charts_output_path)}.")
    charts_output_path.mkdir(parents=True, exist_ok=True)
    
    #---------------------------------------------------#
    # ------------------ Compute split ------------------#
    # ---------------------------------------------------#
    # generate data split
    if verbose:
        print(f"Computing embeddings split.")
    split_dataset.compute_and_store_split(
        df_mapping=df_mapping,
        mapping_plants_path=save_mapping_plants,
        mapping_train_path=save_mapping_train,
        mapping_val_path=save_mapping_val,
        mapping_test_path=save_mapping_test,
        value_column=value_column,
        random_generator=random_split_generator,
        rel_test_set_size=0.2,
        rel_val_set_size=0.1,
        verbose=verbose,
    )

    
    # add extra scaling in the case of volume estimation
    Y_MEAN = 0
    Y_STD = 1
    if value_column == 'volume':
        Y_MEAN = 4630 # FIXED on empirical values
        Y_STD = 1163 # FIXED on empiriccal values
        #Y_MEAN = 0 # FIXED on empirical values
        #Y_STD = 1 # FIXED on empiriccal values
        
    dataset_params = {
        'value_column' : value_column,
        'max_seq_len' : max_seqence_lenght,
        'min_seq_len' : min_seqence_lenght,
        'random_seq_len' : random_seqence_lenght,
        'rotate' : image_rotations,
        'Y_SCALE' : Y_STD,
        'Y_SHIFT' : Y_MEAN,
        'dtype' : torch.float32,
        'transform' : pretrained_transform, 
        'large_augmentation' : vol_augmentation, 
        'aug_volume' : aug_volume, 
        'noise_std' : noise_std
    }
    train_dataset = data_sets.MultiImageTrainDataset(image_dir=dataset_path,
                                                     plant_mapping_path=save_mapping_train,
                                                     **dataset_params)
    val_dataset = data_sets.MultiImageTrainDataset(image_dir=dataset_path,
                                                     plant_mapping_path=save_mapping_val,
                                                     **dataset_params)
    
    # ---------------------------------------------------#
    # ------------- Setup logging in wandb  -------------#
    # ---------------------------------------------------#
    # setup logging with wandb and log training configuration
    run = wandb.init(
        project=project_name,
        group=type(model).__name__,
        config={
            "model_name": type(model).__name__,
            "model_params": model_params,
            "optimizer_name": type(optimizer).__name__,
            "optimizer_params" : optimizer_params,
            "scheduler_name" : type(scheduler).__name__,
            "scheduler_params" : scheduler_params,
            "loss_function" : type(loss_function).__name__,
            "batch_size" : batch_size,
            "num_epochs" : num_epochs,
            "num_worker" : num_worker,
            "dataset" : type(train_dataset).__name__,
            "dataset_params" : dataset_params,
            "dataset_path" : str(dataset_path),
            "notes" : run_notes,
        })

    # save train, val, test split files as artifacts
    split_artifact = wandb.Artifact(name="data_split", type='mapping')
    split_artifact.add_file(str(save_mapping_train), name=save_mapping_train.name)
    split_artifact.add_file(str(save_mapping_val), name=save_mapping_val.name)
    split_artifact.add_file(str(save_mapping_test), name=save_mapping_test.name)
    run.log_artifact(split_artifact)

    # ---------------------------------------------------#
    # --------------- training the model ----------------#
    # ---------------------------------------------------#
    model = train_model(
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        pretrained_model=pretrained_model,
        model_params=model_params,
        optimizer=optimizer,
        optimizer_params=optimizer_params,
        loss_function=loss_function,
        scheduler=scheduler,
        scheduler_params=scheduler_params,
        model=model,
        num_epochs=num_epochs,
        output_path=output_path,
        run_logging=run,
        num_workers=num_worker,
        batch_size=batch_size,
        loss_scaling = Y_STD if value_column == 'volume' else 1,
        verbose=True, 
        uncertainty=uncertainty
        )

    # ---------------------------------------------------#
    # --------------- Save trained model ----------------#
    # ---------------------------------------------------#
    
    
    
    
    # Save model locally on disk
    saved_model_path = saver.save_log(model=model,
                            model_params=model_params,
                            optimizer=optimizer,
                            optimizer_params=optimizer_params,
                            lr_scheduler=scheduler,
                            lr_scheduler_params=scheduler_params)
    # Log the model state and all parameters in wandb
    model_artifact = wandb.Artifact("end_model_state", type='model', 
                                description=("Contains dictonary with:"
                                            "model name, state and parameter"
                                            "optimizer name, state and parameter"
                                            "scheduler name, state and parameter"
                                            ),)
    model_artifact.add_file(str(saved_model_path))
    run.log_artifact(model_artifact)
    
    # ---------------------------------------------------#
    # --------- Model inferrence for evaluation ---------#
    # ---------------------------------------------------#
    # create / get experiment
    plant_mapping = pd.read_json(save_mapping_plants, orient='index', convert_axes=False)
    experiment = experiment_creator.create_experiment(plant_mapping=plant_mapping,)
    # Compute Predictions for all data including test data
    inferrer = Inferrer(saved_model_path=saved_model_path, 
                        output_name=value_column,
                        pretrained_model=pretrained_model,
                        verbose=verbose, 
                        uncertainty=uncertainty)
    dataset = data_sets.ImageInferenceDataset(image_dir=dataset_path,
                                              transform=pretrained_transform,
                                              model_sequence_size=max_seqence_lenght,
                                              experiment=experiment)
    experiment_output = inferrer.infer_experiment(dataset=dataset)
    
    # ---------------------------------------------------#
    # --------- Save and log experiment output ----------#
    # ---------------------------------------------------#
    experiment_output.to_json(save_predictions_path, orient='index')
    prediction_artifact = wandb.Artifact(name="predictions", type='mapping')
    prediction_artifact.add_file(str(save_predictions_path), name="predictions.json")
    run.log_artifact(prediction_artifact)

    # ---------------------------------------------------#
    # --------- Compute experiment statistics -----------#
    # ---------------------------------------------------#
    # Make sure plant_ids are not converted to int in a weird manner
    df_pred = pd.read_json(save_predictions_path, orient='index' , dtype={'plant_id' : str})

    # The index of the mapping files are plant_ids so convert_axes = False
    # otherwise it's converted to int
    df_train = pd.read_json(save_mapping_train, orient='index', convert_axes=False)
    df_test = pd.read_json(save_mapping_test, orient='index', convert_axes=False)
    df_val = pd.read_json(save_mapping_val, orient='index', convert_axes=False)
    
    #anschauen, was in diesem Teil passiert
    fig_train, stat_train = stats_lib.compare(mapping_true=df_train,
                                            mapping_predicted=df_pred,
                                            output_name=value_column,
                                            name="train",
                                            output_folder=charts_output_path,
                                            verbose=verbose)
    fig_test, stat_test = stats_lib.compare(mapping_true=df_test,
                                                mapping_predicted=df_pred,
                                                output_name=value_column,
                                                name="test",
                                                output_folder=charts_output_path)
    fig_val, stat_val = stats_lib.compare(mapping_true=df_val,
                                        mapping_predicted=df_pred,
                                        output_name=value_column,
                                        name="val",
                                        output_folder=charts_output_path)

    # ---------------------------------------------------#
    # ----------- Log experiment statistics -------------#
    # ---------------------------------------------------#
    results_train = wandb.Table(dataframe=stat_train)
    run.log({"results/train_stats" : results_train})
    run.log({"plots/train" : fig_train})
    results_test = wandb.Table(dataframe=stat_test)
    run.log({"results/test_stats" : results_test})
    run.log({"plots/test" : fig_test})
    results_val = wandb.Table(dataframe=stat_val)
    run.log({"results/val_stats" : results_val})
    run.log({"plots/val" : fig_val})

    # ---------------------------------------------------#
    # ------------ Finish a successful run --------------#
    # ---------------------------------------------------#
    run.finish()





def train_model(train_dataset: Dataset,
                val_dataset : Dataset,
                model: torch.nn.Module,
                pretrained_model : torch.nn.Module,
                model_params: Dict[str, any],
                optimizer: torch.optim.Optimizer,
                optimizer_params: Dict[str, any], 
                loss_function: torch.nn.Module,
                scheduler: torch.optim.lr_scheduler,
                scheduler_params: Dict[str, any],
                num_epochs: int,
                output_path: Path,
                run_logging : Run,
                num_workers: int = 1,
                batch_size: int = 256,
                loss_scaling : float = 1,
                device: torch.device = torch.device('cpu'),
                uncertainty: bool = False,
                verbose : bool = False) -> Tuple[torch.nn.Module, List[float], List[float]]:
                
    """Train the given model and log the epoch losses.

    Args:
        train_dataset (Dataset): Train data and labels.
        val_dataset (Dataset): Validation data and labels.
        model (torch.nn.Module): Neural network to be trained.
        optimizer (torch.optim.Opitmizer): optimizer used for weight updating.
        loss_funciton (torch.nn.Module): loss function to be minimized.
        scheduler (torch.optim.lr_scheduler): mechanisim to updated the learning rate over the epochs.
        num_epochs (int): Number of epochs to be trained
        run_logging (Run) : wandb logger instance to log the epochs.
        num_workers (int, optional): Number of threads to be used. Defaults to 8.
        batch_size (int, optional): Batch size for training. Defaults to 256.
        loss_scaling (float, optional): Scaling factor to bring the loss in an interpretable size. Defaults to 1.
        device (torch.device, optional): Device on which to do computations. Defaults to torch.device('cpu')
        verbose (bool, optional) : Defaults to False.
        uncertainty (bool) : Defaults to False, if model should predict uncertainty

    Returns:
        Tuple[torch.nn.Module, List[float], List[float]] : the trained model, training loss, and validation loss
    """
    
    saver = Saver(output_path)
    
    output_path = Path(output_path)
    accelerator = Accelerator()

    # Define a data loader for the dataset
    def collate_fn(batch : List[Tuple[torch.Tensor, float, torch.Tensor]]):
        tensors, labels, masks = zip(*batch)
        tensors = torch.stack(tensors=tensors, dim=0)
        labels = torch.tensor(labels, dtype=tensors.dtype)
        masks = torch.stack(tensors=masks, dim=0)
        return tensors, labels, masks

    # get data loaders
    train_loader = DataLoader(dataset=train_dataset,
                              batch_size=batch_size,
                              shuffle=True,
                              num_workers=num_workers,
                              persistent_workers=True,
                              pin_memory=True,
                              collate_fn=collate_fn)
    val_loader = DataLoader(dataset=val_dataset,
                            batch_size=batch_size,
                            shuffle=False,
                            num_workers=num_workers,
                            persistent_workers=True,
                            pin_memory=True,
                            collate_fn=collate_fn)

    #scheduler_params = {
    #    "total_iters": (num_epochs * len(train_loader)),
    
    
    #}
    
    #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=5, verbose=True)

    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)

    model, pretrained_model, train_loader, val_loader, scheduler = accelerator.prepare(
        model, pretrained_model, train_loader, val_loader, scheduler
    )
    
    best_val_loss = float(130) 

 
    # training loop (tqdm is just for nice output but deactivated)
    for epoch in tqdm(range(num_epochs), disable=True):
        start_epoch = time.time()
        

        # Training
        eval_time = 0.0
        epoch_train_loss = 0.0
        
        model.train()
        for features, labels, mask in train_loader:
            
            if features.dim() == 5:
                with torch.no_grad():
                    start_eval_time = time.time()
                    features = pretrained_model(features[~mask])
                    eval_time += time.time() - start_eval_time

                features = nn.utils.rnn.pad_sequence(
                    features.split((~mask).sum(dim=1).tolist()),
                    batch_first=True,
                )
                mask = ~(features.any(dim=-1))
                
                

            
            
            if uncertainty == True:
                outputs, var = model(features, mask)
                loss = loss_function(outputs, var, labels)
            else:
                outputs = model(features, mask).squeeze(1)
                loss = loss_function(outputs, labels)
                

            optimizer.zero_grad()
            accelerator.backward(loss)
            if accelerator.sync_gradients:
                accelerator.clip_grad_norm_(model.parameters(), 0.1)

            optimizer.step()
            epoch_train_loss += loss.item()
        if scheduler is not None:
            scheduler.step()
            
           
                
        # save the model
        #if epoch == 0: 
                # Save the model along with its parameters, optimizer, and scheduler
                #saver.save(
                    #model=model,
                    #model_params=model_params,
                    #optimizer=optimizer,
                    #optimizer_params=optimizer_params,
                    #lr_scheduler=scheduler,
                    #lr_scheduler_params=scheduler_params, 
                    #epoch = epoch)

           
        
        #if epoch % 5 == 0:
        epoch_val_loss = 0.0
        model.eval()
        with torch.no_grad():
            for features, labels, mask in val_loader:
                features = features[:, :6]
                mask = mask[:, :6]

                if features.dim() == 5:
                    
                    features = pretrained_model(features[~mask])
                    
                    features = nn.utils.rnn.pad_sequence(
                        features.split((~mask).sum(dim=1).tolist()),
                        batch_first=True,
                    )
                    
                 
                    
                    mask = ~(features.any(dim=-1))
                  

                if uncertainty == True: 
                    outputs_val, var_val = model(features, mask)
                    loss_val = loss_function(outputs_val, var_val, labels)
                else:
                    outputs_val = model(features, mask).squeeze(1)
                    loss_val = loss_function(outputs_val, labels)
                
                
                epoch_val_loss += loss_val.item()

        
        time_epoch = time.time() - start_epoch

        train_loss = epoch_train_loss / len(train_loader)
        val_loss = epoch_val_loss / len(val_loader)
        
         # ReduceLROnPlateau step requires the validation loss
        #if scheduler is not None:
        #    scheduler.step(val_loss * loss_scaling)  # Pass the validation loss to the scheduler
            
        if val_loss < best_val_loss:
            print(f"Validation loss improved from {best_val_loss:.4f} to {val_loss:.4f}. Saving model...")
            best_val_loss = val_loss  # Update best loss
            
            #if val_loss * loss_scaling < 130 and epoch > 260:
                # Create a filename that includes the validation loss
                #model_name = f"model_val_loss_{val_loss:.2f}.pt"  # Format validation loss to 2 decimal places

            # 'saver.save' is a custom method for saving models, pass the model name
            saver.save(
                model=model,
                model_params=model_params,
                optimizer=optimizer,
                optimizer_params=optimizer_params,
                lr_scheduler=scheduler,
                lr_scheduler_params=scheduler_params,
                epoch=epoch
            )
        else:
            print("Val loss did not improve")
        
        if epoch == 591 or epoch == 596 or epoch == 597 or epoch == 599:
            print(f"Validation loss improved from {best_val_loss:.4f} to {val_loss:.4f}. Saving model...")
            best_val_loss = val_loss  # Update best loss
            
        
            saver.save(
                model=model,
                model_params=model_params,
                optimizer=optimizer,
                optimizer_params=optimizer_params,
                lr_scheduler=scheduler,
                lr_scheduler_params=scheduler_params,
                epoch=epoch
            )
                

        learning_rate = optimizer.param_groups[0]['lr']
        if True:
            print(f'\t{"Time: ":7s}{time_epoch:>6.4f}',end='')
            print(f'\t{"Evaluation time: ":7s}{eval_time:>6.4f}')
            print(f'{"epoch":7s}{epoch:>5n}\t{"Train Loss:":11s}{train_loss * loss_scaling:>7.4f}', end="")
            print(f'\t{"Val Loss:":10}{val_loss * loss_scaling:>7.4f}', end="")
            print(f'\t{"Current lr:":>12s}{learning_rate:>10.0e}')

        run_logging.log({
            "epoch": epoch,
            "learning_rate": learning_rate, 
            "train_loss": train_loss * loss_scaling,
            "val_loss": val_loss * loss_scaling,
            "epoch_time": time_epoch, 
            "eval_time": eval_time})

    return model

