
import torch
import torch.nn as nn
from transformers import DPTForDepthEstimation
from PIL import Image
import numpy as np
import torch.hub

class Feature_gen(nn.Module):
    def __init__(self, BACKBONE_SIZE) -> None:
        super(Feature_gen, self).__init__()
        self.depth_model = DPTForDepthEstimation.from_pretrained("facebook/dpt-dinov2-small-kitti")
        self.depth_model.eval()  # Ensure the model is in eval mode

        backbone_archs = {
            "small": "vits14",
            "base": "vitb14",
            "large": "vitl14",
            "giant": "vitg14",
        }
        backbone_arch = backbone_archs[BACKBONE_SIZE]
        backbone_name = f"dinov2_{backbone_arch}"

        # Load the backbone model and set it to eval mode
        self.backbone_model = torch.hub.load(repo_or_dir="facebookresearch/dinov2", model=backbone_name)
        self.backbone_model.eval()

    def depth_gen(self, X):
        # Assuming X is on the correct device, check and move if necessary
        device = next(self.depth_model.parameters()).device
        X = X.to(device)

        # Process images in batches to generate depth maps
        batch_size, _, height, width = X.shape
        all_depth_maps = []
        
        with torch.no_grad():
            for i in range(0, batch_size, 6):  # Process each set of 6 images within the batch
                input_batch = X[i:i+6]  # This slices out 6 images at a time
                outputs = self.depth_model(input_batch)
                predicted_depth = outputs.predicted_depth

                # Interpolate to original size
                depth_resized = torch.nn.functional.interpolate(
                    predicted_depth.unsqueeze(1),
                    size=(height, width),  # Original height and width
                    mode="bicubic",
                    align_corners=False
                ).squeeze(1)  # Remove the extra dimension added earlier

                all_depth_maps.append(depth_resized)

        all_depth_maps = torch.cat(all_depth_maps, dim=0)  # Concatenate all depth maps into a single tensor
        print(f"Shape of output depth maps: {all_depth_maps.shape}")
        
        return all_depth_maps

    def forward(self, X):
        # Generate feature maps from the backbone model
        feature_maps = self.backbone_model(X)

        # Generate depth maps
        depth_maps = self.depth_gen(X)
        depth_map_sum = torch.sum(depth_maps, dim=(1, 2)).reshape((-1, 1))
        print(f"Dim depth_map: {depth_maps.shape}, Dim depth maps sum {depth_map_sum.shape}")
        
        # Concatenate feature maps and depth maps along the channel dimension
        output = torch.concat([feature_maps, depth_map_sum], dim=1)
        return output


model = Feature_gen("small")

