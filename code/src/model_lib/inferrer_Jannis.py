import numpy as np
import pandas as pd
import torch
import warnings

from contextlib import nullcontext
from icecream import ic
from lib.seed_setter import SeedSetter
from model_lib.neural_nets import FakeNNModule
from pathlib import Path
from data_lib.data_saver import Saver
from data_lib import data_sets, data_utils
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm
from typing import Tuple, List

def collate_fn(batch : List[Tuple[torch.Tensor, pd.Series, torch.Tensor]]):
    tensors, dict_tuple, masks = zip(*batch)
    tensors = torch.stack(tensors=tensors, dim=0)
    masks = torch.stack(tensors=masks, dim=0)
    return tensors, list(dict_tuple), masks

class Inferrer(SeedSetter):
    
    def __init__(self,
                 dataset : data_sets.MultiImageTrainDataset,
                 num_workers: int,
                 persistent_workers: bool
                 ):
        """Initializes an inferrer for a model given under the saved_model_path

        Args:
            saved_model_path (Path | str): Path to the saved model
            pretrained_model (Module) : Pretrained model to generate embeddings.
                The output of the model is expected the be the embeddings.
            verbose (bool) : Defaults to False.

        Raises:
            FileNotFoundError: If file saved model doesn't exsit.
        """

        # Fix inferrer seeds (should not make a difference)
        SeedSetter.__init__(self)

        # Define a data loader for the dataset
        
        self.data_loader = DataLoader(dataset=dataset,
                                 batch_size=256,
                                 shuffle=False,
                                 pin_memory=True,
                                 num_workers=num_workers,
                                 persistent_workers=persistent_workers and num_workers > 0,
                                 collate_fn=collate_fn)
        
        if torch.cuda.is_available():
            self.device = torch.device('cuda') 
        else:
            self.device = torch.device("cpu")
    
    def infer_experiment(self, model : Path | str | nn.Module | FakeNNModule, verbose) -> pd.DataFrame:
        """ Predict volume for each element in the specified experiment and
        returns the experiment with a added / overwritten `self.output_name` value for each experiment.
        
        Changes the experiment input

        Args:
            dataset (Dataset) : Dataset containing the data to be loaded, the get item method is supposed
                to return a pandas.Series including the plant_id and the image_names
        Raises:
            FileNotFoundError: If the image direcotry or some files in the experiments are not found

        """
        if isinstance(model, str):
            model = Path(model)
        if isinstance(model, Path):
            model, _ = Saver.load_model(model)
        else:
            model = model

        # switch to eval (because of dropout...)
        model.eval()
        model = model.to(self.device)

        # Extract the embeddings
        values = []
        vars = []
        outputs = []
        for (image, element_batch, mask) in tqdm(self.data_loader, disable=not verbose):
            with torch.no_grad():
                with nullcontext() if self.device.type == 'cpu' else torch.autocast(device_type=self.device.type, dtype=torch.float16, enabled=True):
                    features = image.to(self.device)
                    mask = mask.to(self.device)

                    batch_values = model(features, mask)
                    is_distribution = batch_values.shape[1] == 2
                    if is_distribution:
                        pred = batch_values[:, 0]
                        var = batch_values[:, 1]
                    else:
                        pred = batch_values
                
                # note that if not 'volume' is the column name, then std = 1 and mean = 0
                pred = pred.cpu().numpy().flatten() * data_utils.VolumeNorm.Y_STD + data_utils.VolumeNorm.Y_MEAN
                values.append(pred)
                if is_distribution:
                    var = var.cpu().numpy().flatten() * data_utils.VolumeNorm.Y_STD
                    vars.append(var)
                outputs = outputs + element_batch
        
        values = np.concatenate(values, axis=0)
        outputs = pd.DataFrame(outputs)
        outputs["volume"] = values
        if len(vars) > 0:
            vars = np.concatenate(vars, axis=0)
            outputs["variance"] = vars

        return outputs
