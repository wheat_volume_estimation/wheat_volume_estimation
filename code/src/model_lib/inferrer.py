import numpy as np
import pandas as pd
import torch
import warnings

from contextlib import nullcontext
from icecream import ic
from lib.seed_setter import SeedSetter
from pathlib import Path
from data_lib.data_saver import Saver
from data_lib.data_sets import ImageInferenceDataset
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm
from typing import Tuple, List

class Inferrer(SeedSetter):
    
    def __init__(self, saved_model_path : Path | str,
                 output_name : str,
                 pretrained_model : nn.Module,
                 verbose : bool = False,
                 uncertainty : bool = False 
                 ):
        """Initializes an inferrer for a model given under the saved_model_path

        Args:
            saved_model_path (Path | str): Path to the saved model
            output_name (str) : Name of the prediction column in the output csv file.
                If it is 'volume', scaling and mean shift will be applied
            pretrained_model (Module) : Pretrained model to generate embeddings.
                The output of the model is expected the be the embeddings.
            verbose (bool) : Defaults to False.

        Raises:
            FileNotFoundError: If file saved model doesn't exsit.
        """
        
        # Fix inferrer seeds (should not make a difference)
        SeedSetter.__init__(self)
        self.saved_model_path = Path(saved_model_path)
        
        if torch.cuda.is_available():
            self.device = torch.device('cuda:0') 
            if verbose:
                print("Compute with GPU")
        else:
            self.device = torch.device("cpu")
            if verbose:
                print("Compute with CPU")

        self.model, _ = Saver.load_model(self.saved_model_path)
        self.model = self.model.to(self.device)
        self.output_name = output_name.lower()
        self.pretrained_model = pretrained_model
        self.verbose = verbose
        self.uncertainty = uncertainty
        
        if self.output_name == 'volume':
            self.Y_MEAN_VOL = 4630 # FIXED on empirical values
            self.Y_STD_VOL = 1163 # FIXED on empiriccal values
        else:
            self.Y_MEAN_VOL = 0
            self.Y_STD_VOL = 1
    
    def infer_experiment(self, dataset : ImageInferenceDataset) -> pd.DataFrame:
        """ Predict volume for each element in the specified experiment and
        returns the experiment with a added / overwritten `self.output_name` value for each experiment.
        
        Changes the experiment input

        Args:
            dataset (Dataset) : Dataset containing the data to be loaded, the get item method is supposed
                to return a pandas.Series including the plant_id and the image_names
        Raises:
            FileNotFoundError: If the image direcotry or some files in the experiments are not found

        """

        # Define a data loader for the dataset
        def collate_fn(batch : List[Tuple[torch.Tensor, pd.Series, torch.Tensor]]):
            tensors, dict_tuple, masks = zip(*batch)
            tensors = torch.stack(tensors=tensors, dim=0)
            masks = torch.stack(tensors=masks, dim=0)
            return tensors, list(dict_tuple), masks
            
        data_loader = DataLoader(dataset=dataset,
                                 batch_size=1,
                                 shuffle=False,
                                 pin_memory=True,
                                 collate_fn=collate_fn)
        

        # switch to eval (because of dropout...)
        self.pretrained_model.eval()
        self.model.eval()

        self.pretrained_model = self.pretrained_model.to(self.device)

        # Extract the embeddings
        values = []
        outputs = []
        vars = []
        if self.verbose:
            print(f"Infer {len(data_loader)} batches.")
        for i, (image_batch, element_batch, mask_batch) in tqdm(enumerate(data_loader), disable=~self.verbose):
            
            # gradient computations in evaluation are pointless so just dont.
            with torch.no_grad():

                with nullcontext() if self.device.type == 'cpu' else torch.autocast(device_type=self.device.type, dtype=torch.float16, enabled=True):

                     # Set the device to CPU
                    #self.device = torch.device("cpu")
                    self.device = torch.device("cuda")


                    # Move model to CPU
                    #self.model = self.model.to(self.device)
                    
                    

                    # Move input tensor to float32 and the CPU
                    #image_batch = image_batch.to(torch.float32)
                    image_batch_on_device = image_batch[~mask_batch].to(self.device)  # Ensure tensor is on CPU

                    # Load the pretrained model
                    #pretrained_model_test = torch.hub.load("facebookresearch/dinov2", "dinov2_vits14")

                    # Convert the model to float32 and move it to the CPU
                    #pretrained_model_test = pretrained_model_test.to(self.device).float()
                    pretrained_model_test = self.pretrained_model.to(self.device)

                    image_batch_on_device = image_batch_on_device.to(self.device)

                    # Perform inference
                    feature_batch_on_device = pretrained_model_test(image_batch_on_device)
                    
                    #if i == 7: 
                    #    ic(image_batch_on_device)
                    #    ic(feature_batch_on_device)

                    # restore the collapsed seqence dimension and pad to have the same sequence lenght, using the mask
                    full_batch_size = (mask_batch.shape[0], mask_batch.shape[1],feature_batch_on_device.shape[1])
                    feature_batch_full = torch.zeros(full_batch_size, dtype=feature_batch_on_device.dtype, device=self.device)
                    feature_batch_full[~mask_batch] = feature_batch_on_device #batchsize x sequence length x # features of pretrained network
                    assert tuple(feature_batch_full.shape) == full_batch_size
                    
                    #if i == 7: 
                    #    ic(feature_batch_full)

                    # compute the results on the padded sequences
                    print("uncertainty")
                    print(self.uncertainty)
                    
                    if self.uncertainty == True:
                        batch_values, var_test = self.model(feature_batch_full, mask_batch.to(self.device))
                        print("uncertainty is true")
                        
                    else: 
                        batch_values = self.model(feature_batch_full, mask_batch.to(self.device))

                        
                    #print(f"Model device: {next(self.model.parameters()).device}")
                    #print(f"Input tensor device: {feature_batch_full.device}")
                    #print(f"Mask batch device: {mask_batch.device}")
                    #if i ==7: 
                    #    ic(batch_values)
                
                # note that if not 'volume' is the column name, then std = 1 and mean = 0
                #batch_values = batch_values.cpu().numpy().flatten() * self.Y_STD_VOL + self.Y_MEAN_VOL
                

                # Now apply .cpu() on batch_values (which is mu)
                batch_values = batch_values.cpu().numpy().flatten() * self.Y_STD_VOL + self.Y_MEAN_VOL
                values.append(batch_values)
                
                if self.uncertainty == True: 
                    var_values = var_test.cpu().numpy().flatten()
                    vars.append(var_values)
                    var_values = np.concatenate(vars, axis=0)

            
                outputs = outputs + element_batch
        
        values = np.concatenate(values, axis=0)
        outputs = pd.DataFrame(outputs)
        outputs[self.output_name] = values
        
        if self.uncertainty == True:
            outputs["uncertainty"] = var_values 

        return outputs

class ExperimentCreator():
    
    def create_experiment(self, plant_mapping : pd.DataFrame, **kwargs) -> pd.DataFrame:
        raise NotImplementedError("Implement your own experiment!!!!")


class SingleImageExperimentCreator(ExperimentCreator):
    """Creator for experiment / testing data with single images

    The class is, as all ExperimentCreator classes, used
    to create json experiment files, which consist of a list
    of dictonaries, containing the keys `plant_id` and 
    `images`. The `plant_id` value will contain a plant
    id, e.g. `10_10_1`, and `images` will be a list
    of images, e.g. `['10_10_1_0_b.jpg', '10_10_1_1_a.jpg']`
    to be used as input to the model during inference time.
    """
    
    def __init__(self, ignore_artificial : bool):
        """Store local variables

        The `ignore_artificial` variable indicates whether or not
        artificial images should be considered in the experiment.
        If False all artificial images will be neglected. Unless
        there are only artificial images in the dataset.

        Args:
            ignore_artificial (bool): Whether or not artificial images should be considered.
        """
        self.ignore_artificial = ignore_artificial
    
    def create_experiment(self, plant_mapping : pd.DataFrame,) -> pd.DataFrame:
        """Creates an experiment with single image experiments for each image in plant_mapping
        
        Artificial images are ignored if `self.ignore_artificial` is true unless
        all image in plant_mapping are artificial images.

        Args:
            plant_mapping (pd.DataFrame): Dataframe with one row per plant_id, with
            a column `plant_id` containing the plant id, another column `images` containing
            a list of image names depicting the corresponding plant, a column `artificial_mask`
            containing a list of boolean values indicating for each image whether it is
            an artificial image or not. The two list have the same lenght and the same order.

        Returns:
            pd.DataFrame: A Dataframe with one row per experiemnt, a column containing the
            `plant_id` and another column containing the `images` used as input for the experiment.
        """
        output_dict = []
        plant_mapping_exploded = plant_mapping.explode(['images', 'artificial_mask'])
        if self.ignore_artificial and (not plant_mapping_exploded.loc[:,"artificial_mask"].all()):
            plant_mapping_exploded = plant_mapping_exploded.iloc[~np.array(plant_mapping_exploded['artificial_mask'], dtype=bool)]

        for plant_id, row in plant_mapping_exploded.iterrows():
            temp_dict = {}
            temp_dict["plant_id"] = plant_id
            images = row["images"]
            temp_dict["images"] = [images]
            output_dict.append(temp_dict)
        
        output_dict = pd.DataFrame(output_dict)

        return output_dict
    
class MultiImageExperimentCreator(ExperimentCreator):
    """Creator for experiment / testing data with multiple images

    The class is, as all ExperimentCreator classes, used
    to create json experiment files, which consist of a list
    of dictonaries, containing the keys `plant_id` and 
    `images`. The `plant_id` value will contain a plant
    id, e.g. `10_10_1`, and `images` will be a list
    of images, e.g. `['10_10_1_0_b.jpg', '10_10_1_1_a.jpg']`
    to be used as input to the model during inference time.
    """
    
    def __init__(self, 
                 ignore_artificial : bool, 
                 eval_max_seq_len : int,
                 eval_min_seq_len : int,
                 eval_random_seq_len : bool = True,
                 random_choice : bool = False):
        """Store local variables

        The `ignore_artificial` variable indicates whether or not
        artificial images should be considered in the experiment.
        If False all artificial images will be neglected. Unless
        there are only artificial images in the dataset.

        Args:
            ignore_artificial (bool): Whether or not artificial images should be considered.
        """
        self.ignore_artificial = ignore_artificial
        self.eval_max_seq_len = eval_max_seq_len
        self.eval_min_seq_len = eval_min_seq_len
        self.eval_random_seq_len = eval_random_seq_len
        self.random_choice = random_choice
        
    
    def create_experiment(self, plant_mapping : pd.DataFrame,) -> pd.DataFrame:
        """Creates an experiment with single image experiments for each image in plant_mapping
        
        Artificial images are ignored if `self.ignore_artificial` is true unless
        all image in plant_mapping are artificial images.

        Args:
            plant_mapping (pd.DataFrame): Dataframe with one row per plant_id, with
            a column `plant_id` containing the plant id, another column `images` containing
            a list of image names depicting the corresponding plant, a column `artificial_mask`
            containing a list of boolean values indicating for each image whether it is
            an artificial image or not. The two list have the same lenght and the same order.

        Returns:
            pd.DataFrame: A Dataframe with one row per experiemnt, a column containing the
            `plant_id` and another column containing the `images` used as input for the experiment.
        """
        output_dict = []
                
        plant_mapping_exploded = plant_mapping.explode(['images', 'artificial_mask'])

        if self.ignore_artificial and (not plant_mapping_exploded.loc[:,"artificial_mask"].all()):
            #plant_mapping_exploded = plant_mapping_exploded.iloc[~plant_mapping_exploded['artificial_mask']]
            #plant_mapping_exploded = plant_mapping_exploded.iloc[~np.array(plant_mapping_exploded['artificial_mask'], dtype=bool)]
            plant_mapping_exploded = plant_mapping_exploded[plant_mapping_exploded['artificial_mask'] == False]
            #plant_mapping_exploded = plant_mapping_exploded.reset_index(drop=True)
        #ic(plant_mapping_exploded)


        for plant_id, row in plant_mapping_exploded.iterrows():
            temp_dict = {}
            temp_dict["plant_id"] = plant_id
            
            filtered_df = plant_mapping_exploded.loc[plant_id]
            # Extract the image names from the filtered DataFrame
            images = filtered_df['images'].tolist()

        
            eval_max_seq_len = np.minimum(self.eval_max_seq_len, len(images))
            eval_min_seq_len = np.minimum(self.eval_min_seq_len, eval_max_seq_len)
            
            #Give a warning if to much plants are requested:
            if eval_max_seq_len < self.eval_max_seq_len:
                warnings.warn(f"Plant {plant_id} only has {eval_max_seq_len} images but {self.eval_max_seq_len} are requested.")
            
            #Give a warning
            if eval_min_seq_len < self.eval_min_seq_len:
                warnings.warn((f"Images of plant {plant_id} has min sequence length of  {self.eval_min_seq_len}"
                            f"and max seqence lenght of {eval_max_seq_len} are requested."))
            
            #if random == True and min smaller than max: take a random integer between min and max-1
            if self.eval_random_seq_len and eval_min_seq_len < eval_max_seq_len:
                num_out = np.random.randint(eval_min_seq_len,eval_max_seq_len)
            #otherwhise we take the max number as output number
            else:
                num_out = eval_max_seq_len
            
            if self.random_choice: 
                #now we choose the image: 
                images_choice = np.random.choice(images, size=num_out, replace=False)
            else: 
                #choose the first few images 
                images_choice = images[:num_out]
                
            temp_dict["images"] = images_choice
            output_dict.append(temp_dict)
            
        #each element of the list will be one row, with columnn plant_id and images as list
        output_dict = pd.DataFrame(output_dict)
        output_dict = output_dict.drop_duplicates(subset=['plant_id'])        
        #print(output_dict.head(10))



        
        return output_dict