import data_lib.data_sets as data_sets
import lib.stats_lib as stats_lib
import pandas as pd
import torch
from torchvision.transforms import v2
from model_lib.neural_nets import LSTM_fixed_dimensions
from icecream import ic
from model_lib.inferrer import Inferrer, ExperimentCreator
from pathlib import Path
from model_lib.inferrer import SingleImageExperimentCreator, MultiImageExperimentCreator
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import matplotlib.patches as mpatches

#model settings
    
model_transform = [v2.Resize(size=256, antialias=True),
                       v2.CenterCrop(224),
                       v2.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ]

  # ------------- MLP ---------------- #
mlp_drop_out_rates = [0.5]
mlp_num_hidden_layers = [3]
mlp_hidden_div = 2
mlp_input_size = 384
    
    
    # --- model input --- #
mlp_max_sequence_len = 6
mlp_random_seqence_len = False
eval_random_sequence_len = False
random_choice = False

pretrained_model = torch.hub.load("facebookresearch/dinov2", "dinov2_vits14")     
mlp_experiment_creator = MultiImageExperimentCreator(ignore_artificial=True, 
                                                             eval_min_seq_len=mlp_max_sequence_len, 
                                                             eval_max_seq_len=mlp_max_sequence_len,
                                                             eval_random_seq_len=eval_random_sequence_len, 
                                                             random_choice=random_choice)


def evaluation(# path and metadata
              dataset_path : Path | str,
              pretrained_transform : list,
              output_path : Path | str,
              mapping_file : Path | str,
              value_column : str,
              # embeddings and data augmentation
              pretrained_model : torch.nn.Module,
              # model input and model            
              max_seqence_lenght : int,
              # evaluation
              model, 
              experiment_creator : ExperimentCreator,
              verbose : bool = False, 
              uncertainty : bool = False, 
              aug : bool = False):
    """Compute one training run including training, inference and computation of stats

    Args:
        data_path (Path | str): Directory of the dataset
        pretrained_transform (list): Model specific transform settings which should include crop, normalize and resize.
        output_path (Path | str): Directory to store all the output
        mapping_file (Path | str): csv file containing Image -> Plant / value mapping 
        value_column (str): Column in mapping file to be regarded as value
        project_name (str): Project name for logging on wandb
        run_notes (str): Any notes that will be logged in wandb describing the run

        random_split_generator (np.random.Generator): Randomness used to create the train / val / test split
        augmentation_factor (int): TODO remove (for now set it to 1) ?!
        image_rotations (bool): Rotate the images during training in every epoch randomly
        pretrained_model (torch.nn.Module): Pretrained model to extract features from.
        max_seqence_lenght (int): Maximal seqence lenght for one plant
        min_seqence_lenght (int): Minimal seqence lenght for one plant 
        random_seqence_lenght (bool): Whether or not to use random seqence lenth
        model (torch.nn.Module): model to be trained
        model_params (Dict[str, any]): model parameters with which the model was initialized
        optimizer (torch.optim.Optimizer): optimizer which trained the model
        optimizer_params (Dict[str, any]): initialization parameters for optimizer
        scheduler (torch.optim.lr_scheduler.LRScheduler | None): scheduler to adapt learning rate
        scheduler_params (Dict[str, any]): parameters for scheduler
        loss_function (torch.nn.Module): loss function used for training
        num_epochs (int): number of epochs
        batch_size (int): batch size
        num_worker (int): number of workers
        experiment_creator (ExperimentCreator): experiment creator for inference
        verbose (bool, optional): Defaults to False.
        uncertainty (bool): Defaults to False, if model predicted uncertainty of samples
        aug (bool): Defaults to False, if model was trained with augmentated samples 
    """

    
    # ---------------------------------------------------#
    # ----------- Handle paths, in / outputs ------------#
    # ---------------------------------------------------#
    # convert to Path variables in case of given strings
    dataset_path = Path(dataset_path)
    output_path = Path(output_path)
    mapping_file = Path(mapping_file)

    # compute output paths for mapping files and predictions
    save_mapping_plants = output_path / "mapping_all_plants.json"
    save_mapping_train = output_path / "mapping_train.json"
    save_mapping_val = output_path / "mapping_val.json"
    save_mapping_test = output_path / "mapping_test.json"
    save_predictions_path = output_path / "predictions.json"

    # ---------------------------------------------------#
    # --------- Model inferrence for evaluation ---------#
    # ---------------------------------------------------#

    
    # create / get experiment
    plant_mapping = pd.read_json(save_mapping_plants, orient='index', convert_axes=False)
    experiment = experiment_creator.create_experiment(plant_mapping=plant_mapping,)
    # Compute Predictions for all data including test data
   
    
    inferrer = Inferrer(saved_model_path=model, 
                        output_name=value_column,
                        pretrained_model=pretrained_model,
                        verbose=verbose, 
                        uncertainty=uncertainty)
    
    dataset = data_sets.ImageInferenceDataset(image_dir=dataset_path,
                                              transform=pretrained_transform,
                                              model_sequence_size=max_seqence_lenght,
                                              experiment=experiment)
    experiment_output = inferrer.infer_experiment(dataset=dataset)
    
    # ---------------------------------------------------#
    # --------- Save and log experiment output ----------#
    # ---------------------------------------------------#
    experiment_output.to_json(save_predictions_path, orient='index')
    #prediction_artifact = wandb.Artifact(name="predictions", type='mapping')
    #prediction_artifact.add_file(str(save_predictions_path), name="predictions.json")
    #run.log_artifact(prediction_artifact)

    # ---------------------------------------------------#
    # --------- Compute experiment statistics -----------#
    # ---------------------------------------------------#
    # Make sure plant_ids are not converted to int in a weird manner
    df_pred = pd.read_json(save_predictions_path, orient='index' , dtype={'plant_id' : str})

    # The index of the mapping files are plant_ids so convert_axes = False
    # otherwise it's converted to int
    df_train = pd.read_json(save_mapping_train, orient='index', convert_axes=False)
    df_test = pd.read_json(save_mapping_test, orient='index', convert_axes=False)
    df_val = pd.read_json(save_mapping_val, orient='index', convert_axes=False)

    if aug==True:
        df_train = df_train.set_index("plant_id")
    
    charts_output_path = output_path 
    
    #anschauen, was in diesem Teil passiert
    fig_train, stat_train = stats_lib.compare(mapping_true=df_train,
                                            mapping_predicted=df_pred,
                                            output_name=value_column,
                                            name="train",
                                            output_folder=charts_output_path,
                                            verbose=verbose)
    fig_test, stat_test = stats_lib.compare(mapping_true=df_test,
                                                mapping_predicted=df_pred,
                                                output_name=value_column,
                                                name="test",
                                                output_folder=charts_output_path)
    fig_val, stat_val = stats_lib.compare(mapping_true=df_val,
                                        mapping_predicted=df_pred,
                                        output_name=value_column,
                                        name="val",
                                        output_folder=charts_output_path)
    
    
    
    
    #stat_train.to_csv(charts_output_path / "results" / "stat_train_specific_model.csv", index=False)
    #stat_test.to_csv(charts_output_path / "results" / "stat_test_specific_model.csv", index=False)
    #stat_val.to_csv(charts_output_path / "results" / "val_stats_specific_model.csv", index=False)
    
    
    
    # Rename the first  column of dataframe with true values to 'plant_id'
    df_test.index.name = 'plant_id'
    df_test.reset_index(inplace=True)
    
    
    
    #rename column names of predicted dataframe to avoid confusion, add "predicted"
    df_test = df_test.rename(columns=lambda x: x + '_true' if x != 'plant_id' else x)
    
    #rename column names of predicted dataframe to avoid confusion, add "predicted"
    df_pred = df_pred.rename(columns=lambda x: x + '_predicted' if x != 'plant_id' else x)
    
    #merge the true and predicted dataframe
    merged_df = pd.merge(df_test, df_pred, on='plant_id', how='left')
    
 
    ##############################################
    #calculate statistics
    
    # Calculate the correlation, r2 and mape between the 'Volume' and 'predicted volume' columns
    volume_true=np.array(merged_df['volume_true'])
    volume_predicted=np.array(merged_df['volume_predicted'])
   

    r2_score_rescaled = r2_score(volume_true, volume_predicted)
    print(f'r2: {r2_score_rescaled}')

    correlation = np.corrcoef(volume_true, volume_predicted)
    cor = correlation[0,1]
    print(f'cor: {cor}')


    def mean_absolute_percentage_error(y_true, y_pred): 
            y_true, y_pred = np.array(y_true), np.array(y_pred)
            mask = y_true != 0  # Avoid division by zero
            return np.mean(np.abs((y_true[mask] - y_pred[mask]) / y_true[mask])) * 100
        

    MAPE = mean_absolute_percentage_error(volume_true, volume_predicted)
    print(f'mape: {MAPE}')
    
    #################################################
    #take merged_df and add havest date
    # Function to determine sampling_date
    def get_sampling_date(plant_id):
        last_digit = int(plant_id.split('_')[-1])  # Extract the last digit
        if last_digit in [1, 2, 10]:
            return 1
        elif last_digit in [3, 4]:
            return 2
        elif last_digit in [5, 6, 7, 8, 9]:
            return 3
        else:
            return None  # Handle unexpected cases
        
    merged_df["sampling_date"] = merged_df["plant_id"].apply(get_sampling_date)
    

    # Define colors based on sampling_date
    color_map = {1: 'blue', 2: 'green', 3: 'red'}  # Define colors for each category

    # Assign colors based on sampling_date
    colors = merged_df['sampling_date'].map(color_map)
    
    # Plotting
    # Normalize uncertainty values for alpha (transparency)
    
    if "uncertainty_predicted" in merged_df.columns:
        uncertainty_values = merged_df['uncertainty_predicted']
        alpha_values = 1 - (uncertainty_values - uncertainty_values.min()) / (uncertainty_values.max() - uncertainty_values.min())  # Normalize to [0,1]
        alpha_values = np.clip(alpha_values, 0.05, 1)  # Ensure values stay between 0.1 (transparent) and 1 (opaque)
        #make difference larger
        # Compute correlation between absolute error and uncertainty
        merged_df["abs_error"] = abs(merged_df["volume_predicted"] - merged_df["volume_true"])
        correlation = merged_df["abs_error"].corr(merged_df["uncertainty_predicted"])
        print(f"Correlation between uncertainty and absolute error: {correlation:.2f}")
            
    else:
        alpha_values = 1

    plt.figure(figsize=(8, 6))
    
    #plt.scatter(merged_df["abs_error"], merged_df["uncertainty_predicted"], alpha=0.6)
    #plt.xlabel("True Volume")
    #plt.ylabel("Uncertainty Predicted (σ²)")
    #plt.title("Checking Uncertainty Calibration")
    #plt.show()

    
    plt.scatter(merged_df['volume_true'], merged_df['volume_predicted'], color=colors, alpha = alpha_values, label="Sampling date")
    plt.plot(merged_df['volume_true'], merged_df['volume_true'], 'r--', label='x = y')
    # Set logarithmic scale for y-axis
    #plt.yscale('log')

    # Labeling
    plt.xlabel('True Volume', fontsize = 18)
    plt.ylabel('Predicted Volume', fontsize=18)
    plt.title('Predicted Volume vs True Volume', fontsize = 18)
    # Add the correlation coefficient and R² score to the plot
    plt.text(0.05, 0.95, f'r: {cor:.2f}', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')
    plt.text(0.05, 0.90, f'R²: {r2_score_rescaled:.2f}', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')
    
    # Create a manual legend for categorical colors
    legend_patches = [
        mpatches.Patch(color='blue', label='June 9th 2023'),
        mpatches.Patch(color='green', label='June 29th 2023'),
        mpatches.Patch(color='red', label='July 11th 2023')
    ]
    plt.legend(handles=legend_patches, title="Sampling Date", loc="lower right")
    # Show grid and plot

    # Show plot
    plt.grid(True)
    plt.show()

    plt.savefig('data/lstm_without_outliers_uncertainty_aug_newseed1/mlp_run_0/results/plot_best_model_24.png')
    # ---------------------------------------------------#
    # ----------- Log experiment statistics -------------#
    # ---------------------------------------------------#

    # ---------------------------------------------------#
    # ------------ Finish a successful run --------------#
    # ---------------------------------------------------#

    
    
    


evaluation(dataset_path="/projects/zumstego/1_datasets/images_no_bar_crop_2023_clean",
                        pretrained_transform=model_transform, 
                        output_path="data/lstm_without_outliers_uncertainty_aug_newseed1/mlp_run_0/results",
                        mapping_file="/projects/zumstego/1_datasets/images_no_bar_crop_2023_clean/vol_mapping_cleaned.csv",
                        value_column="volume",
                        max_seqence_lenght=6,
                        model = "data/lstm_without_outliers_uncertainty_aug_newseed1/mlp_run_0/model_24.pth",
                        pretrained_model=pretrained_model,
                        experiment_creator=mlp_experiment_creator,
                        verbose=False,
                        uncertainty=True, 
                        aug = True)



#seed4, model 17, model 591
#r2: 0.8833103100335629
#cor: 0.9484593773546445
#mape: 6.844856061813585






#basic aug, model 24, epoch 598
#r2: 0.8770772675077212
#cor: 0.945440818336525
#mape: 6.693728774162377


#uncertainty, model 21, epoch 591
#r2: 0.8898435427537623
#cor: 0.9473303287391709
#mape: 6.716199370589501