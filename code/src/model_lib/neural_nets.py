import torch
import torch.nn as nn
import torch.nn.init as init

from icecream import ic

class MLP_AH(nn.Module):

    def __init__(self,
                num_layer : int,
                activation : nn.Module,
                input_size : int = 512,
                hidden_div : int = 2, 
                output_size : int = 1, 
                normalize : nn.Module = nn.Identity,
                dropout_rate : float = 0.1):
        """Customized Multilayer perceptron with exponentially increasing layer sizes.

        Args:
            num_layer (int): Number of full layers, completed with an additional linear layer in the end.
            activation (nn.Module): Nonlinearities of full layers
            input_size (int, optional): Input size (needs to be adjusted for mutiple inputs). Defaults to 512.
            hidden_div (int, optional): Exponential rate of layer growth. Defaults to 2.
            output_size (int, optional): Number of outputs. Defaults to 1.
            normalize (nn.Module, optional): Class (not instance) of normilization. Defaults to nn.Identity.
            dropout_rate (float, optional): Dropout after every fully connected layer. Defaults to 0.1.
        """
        super(MLP_AH, self).__init__()

        self.normalize = normalize

        self.hidden_sizes = [int(input_size / (hidden_div ** i)) for i in range(1, num_layer + 1)]

        self.linear_layers = nn.ModuleList()
        self.norms = nn.ModuleList()
        self.activations = nn.ModuleList()

        for i in range(num_layer):
            input_size = self.hidden_sizes[i - 1] if i > 0 else input_size
            layer = nn.Linear(input_size, self.hidden_sizes[i])
            norm = normalize(self.hidden_sizes[i])

            setattr(self, f"layer{i + 1}", layer)
            setattr(self, f"batch_norm{i + 1}", norm)
            setattr(self, f"activ{i + 1}", activation)

            self.linear_layers.append(layer)
            self.norms.append(norm)
            self.activations.append(activation())

        self.output_layer = nn.Linear(self.hidden_sizes[-1], output_size)
        self.dropout = nn.Dropout(dropout_rate)

        # Initialize linear layers
        for layer in [*self.linear_layers, self.output_layer]:
            init.xavier_uniform_(layer.weight, gain=init.calculate_gain('relu'))

    def forward(self, x : torch.Tensor, mask : torch.Tensor = None):
        
        assert x.dim() == 3, "Wrong input dimension consider reshaping!" # batch x sequence x features
        in_shape = x.shape
        
        # apply mask to images not to be taken into account
        if not mask is None:
            x = torch.einsum('jkl,jk->jkl',x, ~mask)

        x = x.flatten(start_dim=1) # flatten to batch x (seqence . fatures)
        
        assert x.shape[0] == in_shape[0]
        assert x.shape[1] == in_shape[1] * in_shape[2]

        for layer, norm, activation in zip(self.linear_layers, self.norms, self.activations):
            x = self.dropout(activation(norm(layer(x))))

        x = self.output_layer(x)
        return x
    
    
class MLP_fixed_dimensions(nn.Module): 
    def __init__(self,
                activation : nn.Module,
                output_size : int = 1, 
                normalize : nn.Module = nn.Identity,
                dropout_rate : float = 0.1): 
        """Customized Multilayer perceptron with exponentially increasing layer sizes.

        Args:
            activation (nn.Module): Nonlinearities of full layers
            output_size (int, optional): Number of outputs. Defaults to 1.
            normalize (nn.Module, optional): Class (not instance) of normilization. Defaults to nn.Identity.
            dropout_rate (float, optional): Dropout after every fully connected layer. Defaults to 0.1.
        """
    
        super(MLP_fixed_dimensions, self).__init__()
        self.fc1 = nn.Linear(2304, 1024) 
        self.fc2 = nn.Linear(1024, 512) 
        self.fc3 = nn.Linear(512, 256) 
        
        self.norm1 = normalize(1024)
        self.norm2 = normalize(512)
        self.nomr3 = normalize(256)
        
        self.dropout = nn.Dropout(dropout_rate)
        self.activation = activation()
        self.output_layer = nn.Linear(256, output_size)

        
    def forward(self, x : torch.Tensor, mask : torch.Tensor = None):
        """
        The forward pass of the model.
        input: x: torch.Tensor, the input to the model
        output: x: torch.Tensor, the output of the model
        """
        
        assert x.dim() == 3, "Wrong input dimension consider reshaping!" # batch x sequence x features
        in_shape = x.shape
        
        # apply mask to images not to be taken into account
        if not mask is None:
            x = torch.einsum('jkl,jk->jkl',x, ~mask)

        x = x.flatten(start_dim=1) # flatten to batch x (seqence . fatures)
        
        assert x.shape[0] == in_shape[0]
        assert x.shape[1] == in_shape[1] * in_shape[2]
        
        x = self.dropout(self.activation(self.norm1(self.fc1(x)))) #normalize, activation?
        x = self.dropout(self.activation(self.norm2(self.fc2(x)))) #normalize?
        x = self.dropout(self.activation(self.nomr3(self.fc3(x)))) #normalize?
        x = self.output_layer(x)

        return x
    

class RNN(nn.Module):
    def __init__(self, output_size, bidirectional=True, activation=nn.Tanh, dropout_rate=0.1):
        """
        Args:
            num_layers (int): Number of recurrent layers.
            output_size (int): Number of outputs.
            bidirectional (bool, optional): If True, becomes a bidirectional RNN. Defaults to True.
            activation (nn.Module, optional): Activation function. Defaults to nn.Tanh.
            dropout_rate (float, optional): Dropout rate after each layer. Defaults to 0.1.
        """
        super(RNN, self).__init__()
        
        self.hidden_size = 512
        self.num_layers = 3

        self.rnn = nn.RNN(2304, self.hidden_size, self.num_layers, bidirectional=bidirectional, batch_first=True)
        # If bidirectional, the output size of the RNN layer will be doubled
        self.fc = nn.Linear(self.hidden_size * 2 if bidirectional else self.hidden_size, output_size)
        self.activation = activation()
        self.dropout = nn.Dropout(dropout_rate)
        
     

    def forward(self, x : torch.Tensor, mask : torch.Tensor = None):
        """
        Forward pass of the model.
        
        Args:
            x (torch.Tensor): Input tensor of shape (batch_size, seq_length, input_size)
        
        Returns:
            torch.Tensor: Output tensor of shape (batch_size, output_size)
        """
        
        assert x.dim() == 3, "Wrong input dimension consider reshaping!" # batch x sequence x features
        in_shape = x.shape
        
        # apply mask to images not to be taken into account
        if not mask is None:
            x = torch.einsum('jkl,jk->jkl',x, ~mask)

        x = x.flatten(start_dim=1) # flatten to batch x (seqence . fatures)
        
        assert x.shape[0] == in_shape[0]
        assert x.shape[1] == in_shape[1] * in_shape[2]
        
        # Initialize hidden state
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size)
        # Forward propagate RNN
        out, _ = self.rnn(x, h0)
        # Only take the output from the final time step
        out = self.activation(out[:, -1, :])
        # Apply dropout
        out = self.dropout(out)
        # Decode the hidden state of the last time step
        out = self.fc(out)
        
        return out


class LSTM_fixed_dimensions(nn.Module): 
    def __init__(self,
                activation : nn.Module,
                output_size : int = 1, 
                normalize : nn.Module = nn.Identity,
                dropout_rate : float = 0.1): 
        """Customized LSTM with fixed dimensions.

        Args:
            input_size (int): Size of the input features.
            hidden_size (int): Number of features in the hidden state.
            num_layers (int): Number of recurrent layers.
            activation (nn.Module): Nonlinearity to be used.
            output_size (int, optional): Number of outputs. Defaults to 1.
            normalize (nn.Module, optional): Class (not instance) of normalization. Defaults to nn.Identity.
            dropout_rate (float, optional): Dropout rate. Defaults to 0.1.
        """
    
        super(LSTM_fixed_dimensions, self).__init__()
        input_size = 384
        hidden_size = 512
        num_layers = 3
        bidirectional = False
        num_directions = 2 if bidirectional else 1
        
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True, bidirectional=bidirectional, dropout=dropout_rate)
        
        self.norm = normalize(hidden_size * num_directions)
        self.dropout = nn.Dropout(dropout_rate)
        self.activation = activation()
        self.output_layer = nn.Linear(hidden_size * num_directions, output_size)

        
    def forward(self, x : torch.Tensor, mask : torch.Tensor = None):
        """
        The forward pass of the model.
        
        Args:
            x (torch.Tensor): The input to the model with shape (batch_size, sequence_length, input_size).
            mask (torch.Tensor, optional): Mask to be applied. Defaults to None.
            
        Returns:
            torch.Tensor: The output of the model with shape (batch_size, output_size).
        """
        
        assert x.dim() == 3, "Wrong input dimension, consider reshaping!" # batch x sequence x features
        
        # apply mask to images not to be taken into account
        if not mask is None:
            x = torch.einsum('jkl,jk->jkl',x, ~mask)

        # LSTM input shape: (batch_size, sequence_length, input_size)
        
        # Pass through LSTM
        lstm_out, _ = self.lstm(x)
        
        # LSTM output shape: (batch_size, sequence_length, hidden_size)
        
        # Select the last output in the sequence
        lstm_out_last = lstm_out[:, -1, :]
  
        
        # Apply dropout, normalization, activation, and output layer
        x = self.dropout(self.activation(self.norm(lstm_out_last)))
     
        x = self.output_layer(x)

        return x
    

class LSTM_uncertainty(nn.Module): 
    def __init__(self,
                activation : nn.Module,
                output_size : int = 1, 
                normalize : nn.Module = nn.Identity,
                dropout_rate : float = 0.1): 
        """Customized LSTM with fixed dimensions.

        Args:
            input_size (int): Size of the input features.
            hidden_size (int): Number of features in the hidden state.
            num_layers (int): Number of recurrent layers.
            activation (nn.Module): Nonlinearity to be used.
            output_size (int, optional): Number of outputs. Defaults to 1.
            normalize (nn.Module, optional): Class (not instance) of normalization. Defaults to nn.Identity.
            dropout_rate (float, optional): Dropout rate. Defaults to 0.1.
        """
    
        super(LSTM_uncertainty, self).__init__()
        input_size = 384
        hidden_size = 512
        num_layers = 3
        bidirectional = False
        num_directions = 2 if bidirectional else 1
        
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True, bidirectional=bidirectional, dropout=dropout_rate)
        
        self.norm = normalize(hidden_size * num_directions)
        self.dropout = nn.Dropout(dropout_rate)
        self.activation = activation()
        self.output_layer = nn.Linear(hidden_size * num_directions, 2)

        
    def forward(self, x : torch.Tensor, mask : torch.Tensor = None):
        """
        The forward pass of the model.
        
        Args:
            x (torch.Tensor): The input to the model with shape (batch_size, sequence_length, input_size).
            mask (torch.Tensor, optional): Mask to be applied. Defaults to None.
            
        Returns:
            torch.Tensor: The output of the model with shape (batch_size, output_size).
        """
        
        assert x.dim() == 3, "Wrong input dimension, consider reshaping!" # batch x sequence x features
        
        # apply mask to images not to be taken into account
        if not mask is None:
            x = torch.einsum('jkl,jk->jkl',x, ~mask)

        # LSTM input shape: (batch_size, sequence_length, input_size)
        
        # Pass through LSTM
        lstm_out, _ = self.lstm(x)
        
        # LSTM output shape: (batch_size, sequence_length, hidden_size)
        
        # Select the last output in the sequence
        lstm_out_last = lstm_out[:, -1, :]
  
        
        # Apply dropout, normalization, activation, and output layer
        x = self.dropout(self.activation(self.norm(lstm_out_last)))
     
        x = self.output_layer(x)
        
        mu, log_var = x[:, 0], x[:, 1]  # Split outputs
        var = torch.exp(log_var)  # Ensure variance is positive
        var = torch.clamp(var, min=1e-6, max=2.0)  # Clipping max variance

        return mu, var

        
        
        

class AttentionBased(nn.Module):
    
    def __init__(self, 
                 dropout_rate : float = 0.5,
                 layer: int = 4,
                 head: int = 2,
                 dim_forward: int = 110):
        super(AttentionBased, self).__init__()
        
        self.single_img_mlp = nn.Linear(384, 384)
        encoder_layer = nn.TransformerEncoderLayer(
            d_model=384,
            nhead=head,
            dim_feedforward=dim_forward,
            dropout=dropout_rate,
            batch_first=True,
        )
        self.encoder = nn.TransformerEncoder(
            encoder_layer=encoder_layer, num_layers=layer, norm=None
        )

        self.volume_token = nn.Parameter(torch.randn(1, 1, 384))

        self.last_lin = nn.Sequential(
            nn.Linear(384, 384),
            nn.GELU(),
            nn.Linear(384, 2)
        )

    def forward(self, x: torch.Tensor, mask: torch.Tensor):
        
        x = self.single_img_mlp(x)

        x = torch.cat((x, self.volume_token.repeat(x.size(0), 1, 1)), dim=1)
        mask_n = torch.zeros((mask.shape[0], mask.shape[1] + 1), dtype=torch.bool, device=mask.device)
        mask_n[:, -1] = 0
        mask_n[:, 0:-1] = mask

        x = self.encoder(src=x, src_key_padding_mask=mask_n)
        x = x[:, -1]

        x = self.last_lin(x)
        
        return x


        
    
    
    
    

class AttentionBased_Olivia(nn.Module):
    
    def __init__(self, 
                 dropout_rate : float = 0.1):
        super(AttentionBased, self).__init__()
        
        self.projection = nn.Linear(384, 64)
        self.positional_encoding = nn.Embedding(
            num_embeddings=13, embedding_dim=64
        )  # 1 x 512 x 512
        encoder_layer = nn.TransformerEncoderLayer(
            d_model=64,
            nhead=4,
            dim_feedforward=256,
            dropout=dropout_rate,
            batch_first=True,
            norm_first=True,
        )
        self.encoder = nn.TransformerEncoder(
            encoder_layer=encoder_layer, num_layers=2, norm=None
        )

        self.volume_token = nn.Parameter(torch.randn(1, 1, 64))

        self.last_lin = nn.Linear(64, 1)

        self._reset_parameters()

    def _reset_parameters(self):
        nn.init.xavier_uniform_(self.projection.weight)
        nn.init.constant_(self.projection.bias, 0)

        # from Transformer class
        for p in self.encoder.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

        nn.init.xavier_uniform_(self.last_lin.weight)
        nn.init.constant_(self.last_lin.bias, 0)

    def forward(self, x: torch.Tensor, mask: torch.Tensor):
        x = self.projection(x)
        x = torch.cat((self.volume_token.repeat(x.size(0), 1, 1), x), dim=1)
        mask = torch.cat(
            (torch.zeros(x.size(0), 1, device=mask.device, dtype=torch.bool), mask),
            dim=1,
        )

        x = x + self.positional_encoding(torch.arange(x.size(1), device=x.device))

        x = self.encoder(src=x, src_key_padding_mask=mask)  # batch x imgs x 512
        x = nn.functional.relu(x)
        x = x[:, 0]
        
        #self.encoder = nn.TransformerEncoderLayer(d_model=512,
        #                                          nhead=8,
        #                                          dim_feedforward=2048,
        #                                          dropout=dropout_rate,
        #                                          activation=nn.ReLU(),
        #                                          layer_norm_eps=1e-5,
        #                                          batch_first=True,
        #                                          norm_first=True,
        #                                          bias=True)
        #
        # #self.last_lin = nn.Linear(512, 1)
    
    #def forward(self, x : torch.Tensor, mask = torch.Tensor):

     #   x = self.encoder(src =  x, src_key_padding_mask =  mask) # batch x imgs x 512
     #   x = self.encoder(src = x, src_key_padding_mask = mask) # batch x imgs x 512
     #   x = torch.einsum('jkl,jk->jkl', x, ~mask) # ignore masked content in the sequel
     #   x = torch.sum(x, dim = 1) # batch x 512
     #   #x = torch.sum(x, dim = 1)/torch.sum(~mask) # batch x 512




        x = self.last_lin(x)
        return x



class SingleMlp(nn.Module):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.subdim = 192

        fmlp1 = lambda: nn.Sequential(
            nn.Linear(384, 384),
            nn.GELU(),
            nn.Dropout(0.5),
            nn.Linear(384, self.subdim),
        )
        fmlp2 = lambda: nn.Sequential(
            nn.Linear(self.subdim, 1)
        )
        fmlp3 = lambda: nn.Sequential(
            nn.Linear(self.subdim * 2, self.subdim * 2),
            nn.GELU(),
            nn.Linear(self.subdim * 2, 1)
        )
        self.subdim_pred = fmlp1()
        self.subdim_conf = fmlp1()
        self.head_pred = fmlp2()
        self.head_conf = fmlp2()
        encoder_layer = nn.TransformerEncoderLayer(
            d_model=self.subdim * 2,
            nhead=2,
            dim_feedforward=110,
            dropout=0.5,
            batch_first=True,
        )
        self.encoder = nn.TransformerEncoder(
            encoder_layer=encoder_layer, num_layers=4, norm=None
        )
        self.volume_token = nn.Parameter(torch.randn(1, 1, self.subdim * 2))
        self.last_pred = fmlp3()
        self.last_conf = fmlp3()

    def unflat(self, shape, mask, input):
        unflat_array = torch.zeros(shape, device=input.device, dtype=input.dtype)
        unflat_array[~mask] = input
        return unflat_array

    def forward(self, x: torch.Tensor, mask: torch.Tensor):
        pred_sdim: torch.Tensor = self.subdim_pred(x[~mask])
        conf_sdim = self.subdim_conf(x[~mask])

        direct_pred = self.head_pred(pred_sdim)
        direct_conf = self.head_conf(conf_sdim)

        direct_pred_unflat = self.unflat((x.shape[0], x.shape[1]), mask, direct_pred.squeeze())
        direct_conf_unflat = self.unflat((x.shape[0], x.shape[1]), mask, direct_conf.squeeze())
        pred_sdim_unflat = self.unflat((x.shape[0], x.shape[1], self.subdim), mask, pred_sdim)
        conf_sdim_unflat = self.unflat((x.shape[0], x.shape[1], self.subdim), mask, conf_sdim)
        conf_sdim_unflat = self.unflat((x.shape[0], x.shape[1], self.subdim), mask, conf_sdim)

        pred_conf_cat = torch.cat((pred_sdim_unflat, conf_sdim_unflat), dim=2)
        transformer_input = torch.cat((pred_conf_cat, self.volume_token.repeat(x.shape[0], 1, 1)), dim=1)
        mask_n = torch.zeros((mask.shape[0], mask.shape[1] + 1), dtype=torch.bool, device=mask.device)
        mask_n[:, -1] = 0
        mask_n[:, 0:-1] = mask

        transformer_output = self.encoder(src=transformer_input, src_key_padding_mask=mask_n)
        vol_pred_token = transformer_output[:, -1]

        tpred = self.last_pred(vol_pred_token)
        tconf = self.last_conf(vol_pred_token)

        if self.training:
            return direct_pred_unflat, direct_conf_unflat, tpred, tconf
        else:
            w = torch.concat((tpred, tconf), dim=1)
            return w
            """
            msum = (~mask).sum(dim=1, keepdim=True)
            pred = torch.sum(pred, dim=1, keepdim=True) / msum
            conf = torch.sum(conf, dim=1, keepdim=True) / msum
            w = torch.concat((pred, conf), dim=1)
            return w
            """


class FakeNNModule:
    def __init__(self) -> None:
        self.training = True

    def train(self):
        self.training = True

    def eval(self):
        self.training = False

    def to(self, device):
        return self

    def train_call(self, x: torch.Tensor, mask: torch.Tensor, y: torch.Tensor):
        raise NotImplemented
