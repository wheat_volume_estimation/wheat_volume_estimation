import torch

# Load the saved file
saved_data = torch.load("data/lstm_without_outliers_uncertainty_newseed1/mlp_run_0/model_24.pth")

# Inspect keys in the saved data
print(saved_data.keys())

#dict_keys(['model_class', 'model_state_dict', 'model_params', 'optimizer_class', 
# 'optimizer_state_dict', 'optimizer_params', 'lr_scheduler_class', 'lr_scheduler_state_dict', 'lr_scheduler_params', 'version'])
print(saved_data['epoch'])  # Replace 'model_params' with the actual key name if different

print(saved_data['lr_scheduler_state_dict'])