import hashlib
import pandas as pd
import torch

from contextlib import nullcontext
from icecream import ic
from pathlib import Path
from tqdm import tqdm
from typing import Tuple, List
from torch import nn
from torch.utils.data import DataLoader
from .data_sets import SpikeImageLoader

def hash_embeddings_setup(pretrained_model: nn.Module,
                          image_folder : Path,
                          mapping_file : Path,
                          embeddings_path : Path,
                          value_column : str,
                          n_upsample : int,
                          rotate_images : bool,
                          device : torch.device):
    # create embeddings hash
    plant_mapping = pd.read_json(mapping_file, orient='index', convert_axes=False, dtype={"plant_id" : str})
    plant_mapping = plant_mapping.map(lambda x : tuple(x) if isinstance(x,list) else x)
    hash_ = hashlib.sha256(pd.util.hash_pandas_object(plant_mapping, index=True).values)
    hash_.update(str(pretrained_model).encode())
    hash_.update(str(pretrained_model.state_dict()).encode())
    hash_list = [image_folder, embeddings_path, value_column, n_upsample, rotate_images, device]
    for x in hash_list: hash_.update(str(x).encode()) 
    hash_ = hash_.hexdigest()
    return hash_

def generate_embeddings_(
                        pretrained_model : nn.Module,
                        image_folder : Path,
                        mapping_file : Path,
                        embeddings_path : Path,
                        value_column : str,
                        n_upsample : int = 1,
                        rotate_images : bool = False,
                        device : torch.device = torch.device('cpu'),
                        verbose : bool = False,
                        ):
    """
    Generates a file containing the embeddings for the given images and pretrained model

    This is, all images found in the `mapping_file` are processed with the model.
    Note that the method does not change the model at all, it just
    uses it as a function to apply on the images.

    Then it stores the generated embeddings (= features i.e. outputs of the model)
    as part of a dictonary in the file specified in `embeddings_path`.
    Each embedding will be one row of the tensor stored at key `embeddings`.
    Moreover, the key `plant_ids` contains a implicit mapping from the embedding with
    the same index in the `emeddings` Tensor. In one more tensor with the key
    `labels` the values are store.

    Args:
        model (nn.Module): pretrained model.
        image_folder (Path): Folder containing images.
        plant_mapping (Path): Json file containig the mappings from plants to value / images must
        embeddings_path (Path): File to store the embeddings.
        value_column (str): Value column to be chosen to extract labels from mapping file.
        upsample (int, optional): Increase dataset by taking images multiple time (makes only sens in combination with rotation). Defaults to 0.
        rotate_images (bool, optional) : If True, images are randomly rotated.
        device (torch.device, optional): device as usual in pytorch. Defaults to torch.device('cpu').
        verbose (bool, optional) : Whether or not to print messages
    """
    # make elements to path if they are not already
    image_folder = Path(image_folder)
    mapping_file = Path(mapping_file)
    plant_mapping = pd.read_json(mapping_file, orient='index', convert_axes=False, dtype={"plant_id" : str})
    
    hash_ = hash_embeddings_setup(pretrained_model=pretrained_model,
                          image_folder=image_folder,
                          mapping_file=mapping_file,
                          embeddings_path=embeddings_path,
                          value_column=value_column,
                          n_upsample=n_upsample,
                          rotate_images=rotate_images,
                          device=device)

    # Define a data loader for the dataset
    spike_data = SpikeImageLoader(image_dir=image_folder,
                                  plant_mapping=plant_mapping,
                                  value_column=value_column,
                                  ignore_artificial=False,
                                  rotate=rotate_images,
                                  n_duplicates=n_upsample,
                                  verbose=verbose)

    data_loader = DataLoader(dataset=spike_data,
                             batch_size=32,
                             shuffle=False,
                             pin_memory=True,
                             num_workers = 4)

    # switch to eval (because of dropout...)
    pretrained_model.eval()
    pretrained_model = pretrained_model.to(device)
    
    

    # Extract the embeddings
    embeddings = []
    labels = []
    plant_ids = []
    for i, (image_batch, value_batch, plant_id_batch)  in tqdm(enumerate(data_loader)):

        # gradient computations in evaluation are pointless so just dont.
        with torch.no_grad():

            with nullcontext() if device.type == 'cpu' else torch.autocast(device_type=device.type, dtype=torch.float16, enabled=True):
                # create feature by applying model
                image_batch_on_device = image_batch.to(device)
                feature_batch_on_device = pretrained_model(image_batch_on_device)

        # move data to cpu and numpy
        feature_batch = feature_batch_on_device.cpu()

        # add last column containing the labells
        embeddings.append(feature_batch)
        labels.append(value_batch)
        plant_ids += plant_id_batch

    # the list embeddings will contain np tensors of size (n_batch, n_features)
    # then we need to get a vector of size (n_images, n_features)
    # so np.concatenate(..., axis=0) glues the elements in the list together along the first axis
    embeddings = torch.concatenate(embeddings, axis=0)
    labels = torch.concatenate(labels, axis = 0)

    assert labels.shape[0] == embeddings.shape[0]
    assert labels.shape[0] == len(plant_ids)

    dict_ = {'embeddings' : embeddings, 'labels' : labels, 'plant_ids' : plant_ids, 'hash' : hash_}

    # Save the embeddings to a file
    torch.save(dict_, embeddings_path)


def get_embedings_(embeddings_path : Path) -> Tuple[torch.Tensor, torch.Tensor, List[str]]:
    """Loads embeddings given in `embeddings_path` 

    The method assumes the embeddings exist at the given place and hence will
    crash if they are not previously computed

    Args:
        embeddings_path (Path): Path at which the embeddings should be stored.

    Returns:
        Tuple[torch.Tensor, torch.Tensor, List[str]]: embeddings tensor, value tensor, plant_ids
    """
    if isinstance(embeddings_path, str):
        embeddings_path = Path(embeddings_path)

    if not embeddings_path.is_file():
        raise FileExistsError(f'File {embeddings_path.absolute()} does not exists.')

    # Load the embeddings
    embeddings_dict = torch.load(embeddings_path)
    
    # extract embeddings
    embeddings = embeddings_dict['embeddings']
    
    # last column is the value label
    values = embeddings_dict['labels']
    
    # get plant_indices
    plant_indices = embeddings_dict['plant_ids']

    # Convert the lists to NumPy arrays
    return embeddings, values, plant_indices

def get_embeddings_split(
        plant_mapping_train : Path,
        plant_mapping_val : Path,
        embeddings_path_train : Path,
        embeddings_path_val : Path,
        pretrained_model : nn.Module,
        image_folder : Path,
        value_column : str,
        n_upsample : int = 1,
        rotate_images :bool = False,
        device : torch.device = torch.device('cpu'),
        verbose : bool = False,
        ):
    
    
    # check if embeddings already exist
    train_hash_ = hash_embeddings_setup(pretrained_model=pretrained_model,
                          image_folder=image_folder,
                          mapping_file=plant_mapping_train,
                          embeddings_path=embeddings_path_train,
                          value_column=value_column,
                          n_upsample=n_upsample,
                          rotate_images=rotate_images,
                          device=device)

    generate_train_embeddings = True
    if embeddings_path_train.is_file():
        dict_ = torch.load(embeddings_path_train)    
        if 'hash' in dict_.keys():
            generate_train_embeddings = dict_['hash'] != train_hash_

    if generate_train_embeddings:
        if verbose:
            print(f"Train embeddings have changes and must be recomputed.")
        embeddings_path_train.parents[0].mkdir(parents=True, exist_ok=True)
        generate_embeddings_(pretrained_model=pretrained_model,
                             image_folder=image_folder,
                             mapping_file=plant_mapping_train,
                             embeddings_path=embeddings_path_train,
                             value_column=value_column,
                             n_upsample=n_upsample,
                             rotate_images=rotate_images,
                             device=device,
                             verbose=verbose)
    elif verbose:
        print(f"Train embeddings have not changed and can simply be loaded.")

    val_hash_ = hash_embeddings_setup(pretrained_model=pretrained_model,
                          image_folder=image_folder,
                          mapping_file=plant_mapping_val,
                          embeddings_path=embeddings_path_val,
                          value_column=value_column,
                          n_upsample=n_upsample,
                          rotate_images=rotate_images,
                          device=device)

    generate_val_embeddings = True
    if embeddings_path_val.is_file():
        dict_ = torch.load(embeddings_path_val)    
        if 'hash' in dict_.keys():
            generate_val_embeddings = dict_['hash'] != val_hash_
        
    if generate_val_embeddings:
        if verbose:
            print(f"Validation embeddings have changes and must be recomputed.")
        embeddings_path_val.parents[0].mkdir(parents=True, exist_ok=True)
        generate_embeddings_(pretrained_model=pretrained_model,
                             image_folder=image_folder,
                             mapping_file=plant_mapping_val,
                             embeddings_path=embeddings_path_val,
                             value_column=value_column,
                             n_upsample=1,
                             rotate_images=False,
                             device=device,
                             verbose=verbose)
    elif verbose:
        print(f"Validation embeddings have not changed and can simply be loaded.")
        

    # load embeddings
    X_all_train, y_all_train, plant_indices_train = get_embedings_(embeddings_path=embeddings_path_train)
    X_all_val, y_all_val, plant_indices_val = get_embedings_(embeddings_path=embeddings_path_val)
    assert len(set(plant_indices_train).intersection(plant_indices_val)) == 0

    if verbose:
        n_train = X_all_train.shape[0]
        n_val = X_all_val.shape[0]
        print(f"Found {n_train} embeddings in the train set and {n_val} embeddings in the validation set")
   
    return X_all_train, y_all_train, plant_indices_train, X_all_val, y_all_val, plant_indices_val
       
