from typing import List
from torchvision.transforms import v2, functional as F
import torch
import cv2

def resize_pad_transform(x):
        w, h = x.shape[-1], x.shape[-2]
        if w > h:
            n_w = 224
            n_h = int(224 * h / w)
        else:
            n_h = 224
            n_w = int(224 * w / h)

        x = F.resize(x, (n_h, n_w), antialias=True)
        x = F.pad(x, (0, 0, 224 - n_w, 224 - n_h))
        return x

def get_transform(train: bool) -> List:
     start = [v2.Lambda(resize_pad_transform)]
     augmentation = [
          v2.RandomRotation(180, v2.InterpolationMode.BILINEAR),
     ] 
     end = [v2.ToDtype(torch.float32, scale=True),
            v2.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])]
     return start + augmentation + end if train else start + end
