
import copy
import numpy as np
from torchvision import transforms
from torch.utils.data import DataLoader, TensorDataset, random_split
import os
import torch
from torchvision import transforms
import torchvision.datasets as datasets
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models import resnet50, ResNet50_Weights
import torch.optim as optim
import pytorch_lightning as pl
import lightning as L
from lightning.pytorch.callbacks import LearningRateFinder
import pandas as pd



num_workers = 12
batch_size = 256

def generate_embeddings():
    """
    Transform, resize and normalize the images and then use a pretrained model to extract 
    the embeddings.
    """
    # Define a transform to pre-process the images
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])

    # Load the dataset and apply the transform
    dataset = datasets.ImageFolder(root="../../data/", transform=transform)

    # Define a data loader for the dataset
    loader = torch.utils.data.DataLoader(dataset, batch_size=64, shuffle=False, num_workers=4)

    # Load a pretrained Food101 model
    model = resnet50(weights=ResNet50_Weights.IMAGENET1K_V1)
    # Remove the last layer to access the embeddings
    model.eval()
    model = model.to(device)

    for param in model.parameters():
        param.requires_grad = False

    model.fc = nn.Identity()

    # Extract the embeddings
    embeddings = []
    i = 0
    for images, _ in loader:
        print(f"Working: {(i/len(dataset))*100}%")

        with torch.no_grad():
            features_transformed = images.to(device)
            features_extracted = model(features_transformed)
            print(f"Shape of features_extracted: {features_extracted.shape}")
            embeddings.append(features_extracted.cpu().numpy())
        i += 1
    embeddings = np.concatenate(embeddings, axis=0)
    print(f"Embeddings: {embeddings.shape}")    
    # Save the embeddings to a file
    np.save('../../data/embeddings.npy', embeddings)
    
def create_loader_from_np(X, y = None, train = True, batch_size=64, shuffle=True, num_workers = 4):
    """
    Create a torch.utils.data.DataLoader object from numpy arrays containing the data.

    input: X: numpy array, the features
           y: numpy array, the labels
    
    output: loader: torch.data.util.DataLoader, the object containing the data
    """
    if train:
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float), 
                                torch.from_numpy(y).type(torch.long))
    else:
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float))
    loader = DataLoader(dataset=dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        pin_memory=True, num_workers=num_workers)
    return loader

    
def get_data(embeddings_path):
    """
    Preprocessing the data and the connected labels

    input: embeddings_path: String which contains the path of the embeddings.npy file
    
    output: input_frame: Input-data for the model; label: labels which are connected to the input-data
    """
    #TODO Take only 2 pictures for the inputframe instead of 6

    #Load the embeddings
    embeddings = np.load(embeddings_path)
    
    #create data-frame
    df = pd.read_csv('../../data/vol_mapping.csv')
    print(df)  
    # Group the DataFrame by the 'plant_id' column and aggregate the 'id' and 'volume' values into lists
    plant_id_data = df.groupby('plant_id').agg({'id': list, 'volume': list}).reset_index()
    print(plant_id_data) 
    input_frame= []
    label = []
    target_shape = (len(plant_id_data), 10240)

    for _, row in plant_id_data.iterrows():
        plant_id = row['plant_id']
        first_id = row['id'][0]
        last_id = row['id'][-1]
        volume = row['volume'][0]
        
        
        # Get the embeddings slice and ensure it's the desired shape
        embeddings_slice = embeddings[first_id:last_id].reshape(1, -1)
        if embeddings_slice.shape[1] > target_shape[1]:
            embeddings_slice = embeddings_slice[:, :target_shape[1]]
        elif embeddings_slice.shape[1] < target_shape[1]:
            # Pad with zeros if it's smaller
            embeddings_slice = np.pad(embeddings_slice, ((0, 0), (0, target_shape[1] - embeddings_slice.shape[1])), 'constant')

        input_frame.append(embeddings_slice)
        label.append(volume)
    
    # Convert the lists to NumPy arrays
    input_frame = np.array(input_frame).reshape(target_shape)
    label = np.array(label)
    
    print("Shape of input_frame:", input_frame.shape)
    print("Shape of label:", label.shape)
    
    return input_frame, label

class Net(nn.Module):
    """
    The model class, which defines our classifier.
    """
    def __init__(self):
        """
        The constructor of the model.
        """
        super(Net, self).__init__()
        #TODO Go down with overall input size
        self.lin1 = nn.Linear(10240, 5120)
        self.lin2 = nn.Linear(5120, 2560)
        self.lin3 = nn.Linear(2560, 1280)
        self.lin4 = nn.Linear(1280, 640)
        self.lin5 = nn.Linear(640, 320)
        self.lin6 = nn.Linear(320, 1)


    def forward(self, x):
        """
        The forward pass of the model.

        input: x: torch.Tensor, the input to the model

        output: x: torch.Tensor, the output of the model
        """
        x = F.celu(self.lin1(x))
        x = F.celu(self.lin2(x))
        x = F.celu(self.lin3(x))
        x = F.celu(self.lin4(x))
        x = F.celu(self.lin5(x))
        x = self.lin6(x)
        return x
    
    def num_flat_features(self, x):
        size = x.size()[1:]
        num = 1
        for i in size:
            num *= i
        return num

    
def train_model(train_loader):
    """
    The training procedure of the model; it accepts the training data, defines the model 
    and then trains it.

    input: train_loader: torch.data.util.DataLoader, the object containing the training data
    
    output: model: torch.nn.Module, the trained model
    """

    #TODO Seed notation and amount of iterations

    if os.path.isfile('current_model.pt'):
        model = torch.load('current_model.pt')
        
    model = Net()
    model.train()
    model.to(device)
    n_epochs = 10000
    
    # define loss function and optimizer
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    
    # define validation split
    val_split = 0.2
    n_train = len(train_loader.dataset)
    split_idx = int(np.floor(n_train * (1 - val_split)))
    train_data, val_data = random_split(train_loader.dataset, [split_idx, n_train-split_idx])
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    val_loader = DataLoader(val_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    
    best_val_loss = np.inf
    counter = 0
    val_loss_array = []

    
    for epoch in range(n_epochs):
        epoch_loss = 0.0
        for i, [X, y] in enumerate(train_loader):
            X, y = X.to(device), y.to(device)
            
            optimizer.zero_grad()
            outputs = model(X).squeeze()
            print(f"Output: {outputs.shape}, y: {y.shape}")
            
            # Convert the target values (y) to Float
            y = y.to(torch.float)
            
            loss = criterion(outputs, y)
            loss.backward()
            optimizer.step()
            
            epoch_loss += loss.item()
        
        # calculate validation loss
        val_loss = 0.0
        with torch.no_grad():
            for X, y in val_loader:
                X, y = X.to(device), y.to(device)
                
                outputs = model(X)
                loss = criterion(outputs, y)
                
                val_loss += loss.item()
        
        torch.save(model, 'current_model.pt')
        val_loss /= len(val_loader.dataset)
        val_loss_array.append(val_loss)
        print(f"Epoch {epoch+1} - Train loss: {epoch_loss/len(train_loader.dataset):.4f} - Val loss: {val_loss:.4f}")
        #TODO Compare median of the last several val_losses with the median of the last 100 
        if epoch + 1 >= 100:
            if np.median(val_loss_array[len(val_loss_array) - 101:]) < np.median(val_loss_array[len(val_loss_array) - 21:]):
                print(f"Overfit at epoch: {epoch + 1}")
    return model
         
        
    
    
    
    
if __name__ == "__main__":
    
    #Check if cuda is available
    if torch.cuda.is_available():
        device = torch.device('cuda')
        print("I have the GPU")
    else:
        device = torch.device('cpu')
        print("Have to use CPU")
        
        
    embeddings_path = '../../data/embeddings.npy'
    
    #Check if the embeddings already exist
    if not os.path.exists(embeddings_path):
        generate_embeddings()
    
    X, y = get_data(embeddings_path)
    
    data_loader = create_loader_from_np(X, y, train=True, batch_size=64, shuffle=True)
    
    train_model(data_loader)
    
        
    
    

    
