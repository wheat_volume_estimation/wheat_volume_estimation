
from PIL import Image
import os

# Set the target size for the images
target_width = 224
target_height = 224

# Input and output directories
input_directory = '/home/aaronh/DeepLearning/weize_volume_gugus/data/lab_images/'
output_directory = '/home/aaronh/DeepLearning/weize_volume_gugus/data/lab_images_resized/'

# Ensure the output directory exists, or create it if necessary
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

# List all files in the input directory
file_list = os.listdir(input_directory)

# Loop through each file in the directory
for filename in file_list:
    if filename.endswith(('.jpg', '.jpeg', '.png', '.gif')):
        # Open the image file
        with Image.open(os.path.join(input_directory, filename)) as img:
            # Resize the image to the target size
            img = img.resize((target_width, target_height))

            # Save the resized image to the output directory
            img.save(os.path.join(output_directory, filename))

print("Rescaling complete. Images are saved in the output directory.")
