
import torch
import matplotlib.pyplot as plt
from icecream import ic
import os
import re
from tueplots import bundles

def plotter(path):
    plt.rcParams.update(bundles.icml2022(usetex=False))

    # Get a list of all files with .pth extension in the folder
    file_names = [f for f in os.listdir(path) if f.endswith('.pth')]
    labels = ['artificial color', 'mixed color', 'mixed grayscale', 'real images']
    ic(file_names)

    # Define hexadecimal colors for train and validation losses
    train_colors = ['#00F000', '#0000FF', '#FF0000', '#000000']
    val_colors = ['#8AFF00', '#0080FF', '#FF8383', '#797979']  

    # Initialize lists to store train and validation losses for each file
    all_train_losses = []
    all_val_losses = []

    # Load data from each .pth file
    for file_name in file_names:
        checkpoint = torch.load(os.path.join(path, file_name))
        train_losses = checkpoint['train_loss'][:1000]
        val_losses = checkpoint['val_loss'][:1000]
        all_train_losses.append(train_losses)
        all_val_losses.append(val_losses)

    # Plotting
    epochs = range(1, 1001)

    # Plot train losses
    for i, train_loss in enumerate(all_train_losses):
        plt.plot(epochs, train_loss, label=f'{labels[i]} train loss', color=train_colors[i])

    # Plot validation losses
    for i, val_loss in enumerate(all_val_losses):
        plt.plot(epochs, val_loss, label=f'{labels[i]} val loss', color=val_colors[i])

    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend(loc='lower left')
    plt.show()

if __name__ == "__main__":
    plotter("/home/aaronh/DeepLearning/weize_volume_gugus/code/data/best_models/")


