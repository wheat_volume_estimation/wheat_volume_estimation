import numpy as np

import model_lib.neural_nets as neural_nets
import model_lib.training as training
import torch.nn as nn
import torch

from pathlib import Path
from accelerate.utils import set_seed
from data_lib.transforms import get_transform

def warmup_multiplicative_schedule(e):
     return 0.001 if e < 40 else 1/ (e * 0.05)

def get_model(path):
    from data_lib import data_saver
    path = Path(path)
    saver = data_saver.Saver(path.parent)
    return saver.load_model(path)

     

"""
@click.command()
@click.option("--dataset-path", type=str, required=True, help="Path to the dataset, required to contain the images and a mapping file.")
@click.option("--output-path", type=str, required=True, help="Path to output the computed results.")
@click.option("--mapping-file", type=str, required = True, help = "Path to mapping file" )
@click.option("--counting", is_flag=True, type=bool, help="Use this flag when the model predicts the number of spikelets. There will be no normalization of the label values" )
@click.option("--notes", type=str, default="-", help='Optional description of the run to be logged.')
@click.option("--trained-model-path", type=str, default=None, help="Path to saved checkpoint on which to continue training.")
"""
def main_training(dataset_path : str | Path,
                  output_path : str | Path,
                  mapping_file : str | Path,  
                  counting : bool = False,
                  notes : str = "-",
                  trained_model_path : str | None = None):
    
    # GENERAL
    """
    v = training.VolumeMapping(r"F:\Boxes-ds\spike_dataset_distance_seg288\split_without2024")
    #v.create_split(r"F:\Boxes-ds\spike_dataset_distance_seg288_artifical12\vol_mapping.csv", np.random.default_rng(14), 6, 6, 250, 150)
    v.adapt_new_dataset(r"F:\Boxes-ds\dmap_encoded2\split_without2024", r"F:\Boxes-ds\dmap_encoded2\vol_mapping.csv", 6, 6)
    exit(0)
    """
    """
    t = training.TrainRunSetup(
        dataset_path=r"F:\Boxes-ds\spike_dataset_manual_20_pad_segment",
        transform_train=get_transform(True),
        transform_eval=get_transform(False),
        output_path=Path(output_path) / fr"Someout",
        mapping_dir=r"F:\Boxes-ds\spike_dataset_manual_20_pad\split_without2024",
        pretrained_model=None,
        max_seqence_lenght=6,
        min_seqence_lenght=6,
        random_seqence_lenght=False,
        num_workers=1,
        eval_random_sequence_len=6,
        eval_min_sequence_len=6,
        eval_max_sequence_len=6,
        cache_dir=Path(r"F:\Boxes-ds\_cache_dataset_embeddings"),
        n_dupli_train=5,
        wandb_config={
        },
        wandb_mode="disabled",
        verbose=True
    )
    t.create_volume_statistics()
    """

    # Verbose logging
    verbose = True
    # Path to precomputed image features. Regenerated automatically if dataset configuration changed.
    cache_dir = Path(r"F:\Boxes-ds\_cache_dataset_embeddings")

    # Fix seeds to make all the code reproducible
    set_seed(1071997)
    random_split_generator = np.random.default_rng(14)
    # Type of training
    
    # Feature extractor backbone
    pretrained_model = torch.hub.load("facebookresearch/dinov2", "dinov2_vits14") #if s =>l s = 384, b: 768, l: 1024, g: 1536, inputsize = 1024, s => b: 7.., g => ......

    # DATASET

    # Min number of images, max number of images
    min_sequence_len = 3
    max_sequence_len = 6
    # Randomize the number of images between min and max during training
    random_sequence_len = True

    # Evaluation
    eval_min_sequence_len=6
    eval_max_sequence_len=12
    eval_random_sequence_len = False
    # Note that images are always randomly choosen from all available images.
    # If max_sequence_len < total images => A random subset will be choosen. The order is also random.

    # TRAINING

    num_epochs = 1
    num_worker = 3
    batch_size = 256


    
    class MseLoss(nn.Module):
        def __init__(self, *args, **kwargs):
              super().__init__(*args, **kwargs)

        def forward(self, input: torch.Tensor, target: torch.Tensor, loss_scale) -> torch.Tensor:
            prediction = input[:, 0].squeeze()
            loss = ((prediction - target) ** 2 * loss_scale).mean()
            return loss
        
    class SingleMlpMseLoss(nn.Module):
        def __init__(self, weights = (1, 0.5, 0.3, 0.05), *args, **kwargs):
              super().__init__(*args, **kwargs)
              self.weights = weights

        def conf_err_fun(self, err, conf):
             e = err - conf
             mask = e < 0
             e[mask] = torch.abs(e[mask]) * 0.1
             e[~mask] = e[~mask] ** 2
             return e

        def forward(self, input: torch.Tensor, target: torch.Tensor, loss_scale: torch.Tensor) -> torch.Tensor:
            input, conf, tpred, tconf = input
            mask = input != 0
            target = target.unsqueeze(1)
            loss_scale = loss_scale.unsqueeze(1)
            err_single_img = ((input - target) ** 2)
            conf_single_img = self.conf_err_fun(err_single_img, conf)
            err_single_img = (err_single_img * loss_scale)[mask]
            conf_single_img = (conf_single_img * loss_scale)[mask]
            err_combined = (tpred - target) ** 2 * loss_scale
            conf_combined = self.conf_err_fun(err_combined, tconf) * loss_scale
            wsi, wci, wec, wcc = self.weights
            return err_single_img.mean() * wsi + conf_single_img.mean() * wci + err_combined.mean() * wec + conf_combined.mean() * wcc
        
   

    Att = True
    if Att:

        min_sequence_len, max_sequence_len = (4, 12)
        train_setup = training.TrainRunSetup(
            dataset_path=dataset_path,
            transform_train=get_transform(True),
            transform_eval=get_transform(False),
            output_path=Path(output_path) / fr"DmapEncoded",
            mapping_dir=mapping_file,
            pretrained_model=pretrained_model,
            max_seqence_lenght=max_sequence_len,
            min_seqence_lenght=min_sequence_len,
            random_seqence_lenght=random_sequence_len,
            num_workers=num_worker,
            eval_random_sequence_len=eval_random_sequence_len,
            eval_min_sequence_len=eval_min_sequence_len,
            eval_max_sequence_len=eval_max_sequence_len,
            cache_dir=cache_dir,
            n_dupli_train=5,
            wandb_config={},
            wandb_mode="disabled",
            verbose=True
        )

        def att_fun(train_setup: training.TrainRunSetup, distill_phase: int):
            if distill_phase == 0:
                 model = neural_nets.SingleMlp()
                 epochs = 300
                 loss = SingleMlpMseLoss()
                 #model, _ = get_model(r"F:\experiments_output\checks_without_2024\DistillTests\saved_models\best_model_phase0.pth")
            elif distill_phase == 1:
                train_setup.train_dataset.no_label_scale = 0.9
                model = neural_nets.AttentionBased()
                loss = MseLoss()
                train_setup.train_dataset.max_seq_len = 4
                train_setup.train_dataset.min_seq_len = 3
                epochs = 500
                model, _ = get_model(r"F:\experiments_output\checks_without_2024\DistillTests\saved_models\best_model_phase1.pth")
            elif distill_phase == 2:
                train_setup.train_dataset.no_label_scale = 0.9
                model = neural_nets.AttentionBased()
                loss = MseLoss()
                train_setup.train_dataset.max_seq_len = 4
                train_setup.train_dataset.min_seq_len = 3
                epochs = 500
                model, _ = get_model(r"F:\experiments_output\checks_without_2024\DistillTests\saved_models\best_model_phase2.pth")
            else:
                pass
            optimizer_params = {
                'lr' : 0.001,
            }
            optimizer = torch.optim.Adam(model.parameters(), **optimizer_params)
            scheduler_params = {
                    'lr_lambda': warmup_multiplicative_schedule,
            }
            scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, **scheduler_params)
            return model, {}, optimizer, scheduler, loss, epochs

        training.train_run_nn(
            train_setup=train_setup,
            model_fun=att_fun,
            start=0,
            num_phases=1
        )

    

if __name__ == "__main__":
    """
    config_fip_no_pad = (r"F:\Boxes-ds\artifical_ext",
                  r"F:\experiments_output\checks_without_2024",
                  r"F:\Boxes-ds\artifical_ext\split",
                  False,
                "Test run", None)
    """
    config_fip_no_pad = (r"F:\Boxes-ds\dmap_encoded2",
                  r"F:\experiments_output\checks_without_2024",
                  r"F:\Boxes-ds\dmap_encoded2\split_without2024",
                  False,
                "Test run", None)
    main_training(*config_fip_no_pad)
