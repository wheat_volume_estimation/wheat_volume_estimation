import click 
import numpy as np
import random

from scipy.spatial import transform
import model_lib.neural_nets as neural_nets
import model_lib.training as training
import model_lib.training_Jannis as training_Jannis

import data_lib.data_saver as data_saver
import torch.nn as nn
import torch

from torchvision.transforms import v2
from icecream import ic
from model_lib.inferrer import SingleImageExperimentCreator, MultiImageExperimentCreator
from pathlib import Path
from torchvision import models
from accelerate.utils import set_seed
from model_lib.feature_gen import Feature_gen
from data_lib.transforms import get_transform


@click.command()
@click.option("--dataset-path", type=str, required=True, help="Path to the dataset, required to contain the images and a mapping file.")
@click.option("--output-path", type=str, required=True, help="Path to output the computed results.")
@click.option("--mapping-file", type=str, required = True, help = "Path to mapping file" )
@click.option("--counting", is_flag=True, type=bool, help="Use this flag when the model predicts the number of spikelets. There will be no normalization of the label values" )
@click.option("--notes", type=str, default="-", help='Optional description of the run to be logged.')
@click.option("--trained-model-path", type=str, default=None, help="Path to saved checkpoint on which to continue training.")
def main_training(dataset_path : str | Path,
                  output_path : str | Path,
                  mapping_file : str | Path,  
                  counting : bool,
                  notes : str,
                  trained_model_path : str | None):
    
    verbose = True

    # Fix seeds to make all the code reproducible
    #torch.manual_seed(99)
    #np.random.seed(99)
    #random.seed(99)
    set_seed(11397)
    random_split_generator = np.random.default_rng(41445344)
    
    #set_seed(1341397)
    #random_split_generator = np.random.default_rng(41444)
    
    #seeed4: 

    #14444: doesnt seem too bad, still a bit off at the end
    #3948: ok, train: 0.86, 0.83 / same: 398949898
    
    
    #390988: train: 0.86, test: 0.87, val: 0.83


    # ---------------------------------------------------#
    # --------- Setup of class and meta data  -----------#
    # ---------------------------------------------------#
    if counting:
        value_column = 'total'
        project_name = 'wheat-spiklets-prediction'
    else:
        value_column = 'volume'
        project_name = 'wheat-volume-prediction'
        
    
    # ---------------------------------------------------#
    # ----- Pretrained Model and data augmentation ------#
    # ---------------------------------------------------#
    # choose some model and initialize PRETRAINED weights
    #pretrained_model = models.resnet18(weights=models.ResNet18_Weights.IMAGENET1K_V1) 
    features = False
    
    if features:
        pretrained_model = Feature_gen("small")

    else:
        pretrained_model = torch.hub.load("facebookresearch/dinov2", "dinov2_vits14") #if s =>l s = 384, b: 768, l: 1024, g: 1536, inputsize = 1024, s => b: 7.., g => ......

    
    model_transform = [v2.Resize(size=256, antialias=True),
                       v2.CenterCrop(224),
                       v2.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    ]

    # number of times the image should be included
    # values > 1 make sense if we have random rotations
    augmentation_factor = 1 
    random_rotations = True
    

    # ---------------------------------------------------#
    # --------------------- Training --------------------#
    # ---------------------------------------------------#
    num_epochs = 5000
    num_worker = 16 

    # number of threads / cores used
    batch_size = 32
    loss_function = torch.nn.MSELoss()
    vol_augmentation = False
    uncertainty = False
    aug_volume = 6000
    noise_std = 0.01
    
    # ---------------------------------------------------#
    # --------------------- Inferrence ------------------#
    # ---------------------------------------------------#
    
    #Prediction based in single images ("single") 
    # or multiple images ("multiple")
    num_of_img_inference = "multiple"
    

    # ---------------------------------------------------#
    # -------------------- MLP setup --------------------#
    # ---------------------------------------------------#

    # ------------- MLP ---------------- #
    mlp_drop_out_rates = [0.5]
    mlp_activations = [nn.CELU]
    mlp_num_hidden_layers = [3]
    mlp_normalization = nn.LayerNorm
    mlp_hidden_div = 2
    
  
    mlp_input_size = 384

    
    # --- model input --- #
    mlp_max_sequence_len = 6
    mlp_random_seqence_len = False
    #in case eval_random_sequence_len is True, a random number of images between
    #min and max is chosen for inference. 
    #in case eval_random_sequence_len is False, the max number is chosen
    eval_random_sequence_len = False
    #if random_choice = False, then the first images of the plant are chosen
    #if random_choice = True, the images of the plants are choosen randomly
    random_choice = False

    # ---  Evaluation settings ---#
    if num_of_img_inference == "single": 
        mlp_experiment_creator = SingleImageExperimentCreator(ignore_artificial=True)
    else: 
        mlp_experiment_creator = MultiImageExperimentCreator(ignore_artificial=True, 
                                                             eval_min_seq_len=mlp_max_sequence_len, 
                                                             eval_max_seq_len=mlp_max_sequence_len,
                                                             eval_random_seq_len=eval_random_sequence_len, 
                                                             random_choice=random_choice)
        
   
    mlp_training = False
    if mlp_training:
        cnt = 0
        for activation in mlp_activations:
            for dropout_rate in mlp_drop_out_rates: 
                for layers in mlp_num_hidden_layers:

                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    
                    model_params_mlp_ah = {
                            'num_layer' : layers,
                            'activation' : activation,
                            'input_size' : mlp_input_size * mlp_max_sequence_len,
                            'hidden_div' : mlp_hidden_div,
                            'output_size' : 1,
                            'normalize' : mlp_normalization,
                            'dropout_rate' : dropout_rate,
                            }
                    mlp_model = neural_nets.MLP_AH(**model_params_mlp_ah)
                        

                    # ---- Training ----- #
                    optimizer_params = {
                        'lr' : 0.0001,
                    }
                    optimizer = torch.optim.Adam(mlp_model.parameters(), **optimizer_params)

                    scheduler_params = {
                        'start_factor' : 1.0,
                        'end_factor' : 0.001,
                        'total_iters' : num_epochs,
                    }
                    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
                    training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = mlp_model,
                        model_params=model_params_mlp_ah,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=loss_function,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose
                    )

                    cnt += 1
                    
    # Get the current seed
    #current_seed = torch.initial_seed()
    #print("Current seed:", current_seed)
    
    #*******************************************************************************************************************

    
    training_features = False
    if training_features:
        cnt = 0
        
                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    
        model_params_features = {
                'BACKBONE_SIZE' : 'small'
                }
        depth_model = Feature_gen(**model_params_features)
                        

        # ---- Training ----- #
        optimizer_params = {
            'lr' : 0.0001,
        }
        optimizer = torch.optim.Adam(depth_model.parameters(), **optimizer_params)

        scheduler_params = {
            'start_factor' : 1.0,
            'end_factor' : 0.001,
            'total_iters' : num_epochs,
        }
        scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
        training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = depth_model,
                        model_params=model_params_features,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=loss_function,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose
                    )

        cnt += 1
    
    
    #*******************************************************************************************************************
        
    mlp_training_fixed = False
    if mlp_training_fixed:
        cnt = 0
        for activation in mlp_activations:
            for dropout_rate in mlp_drop_out_rates: 
                

                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    
                    
                    model_params_mlp_fp = {
                            'activation' : activation,
                            'output_size' : 1,
                            'normalize' : mlp_normalization,
                            'dropout_rate' : dropout_rate,
                            }
                    mlp_model = neural_nets.MLP_fixed_dimensions(**model_params_mlp_fp)    

                    # ---- Training ----- #
                    optimizer_params = {
                        'lr' : 0.001,
                    }
                    optimizer = torch.optim.Adam(mlp_model.parameters(), **optimizer_params)

                    scheduler_params = {
                        'start_factor' : 1.0,
                        'end_factor' : 0.1,
                        'total_iters' : num_epochs,
                    }
                    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
                    training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = mlp_model,
                        model_params=model_params_mlp_fp,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=loss_function,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose
                    )

                    cnt += 1
                 
    
    # ---------------------------------------------------#
    # ---------------------RNN Training -----------------#
    # ---------------------------------------------------#
   
    # ---------------------------------------------------#
    # --------------------- Inferrence ------------------#
    # ---------------------------------------------------#
    
 

    # ---------------------------------------------------#
    # -------------------- RNN setup --------------------#
    # ---------------------------------------------------#

    # ------------- RNN ---------------- #
    RNN_drop_out_rates = [0.5]
    RNN_activations = [nn.CELU]
    RNN_normalization = nn.LayerNorm
    
    RNN = False
    if RNN:
        cnt = 0
        for activation in RNN_activations:
            for dropout_rate in RNN_drop_out_rates: 
                

                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    model_params_RNN_fp = {
                            'activation' : activation,
                            'output_size' : 1,
                            'normalize' : RNN_normalization,
                            'dropout_rate' : dropout_rate,
                            }
                    RNN_model = neural_nets.RNN(**model_params_RNN_fp)    

                    # ---- Training ----- #
                    optimizer_params = {
                        'lr' : 0.0001,
                    }
                    optimizer = torch.optim.Adam(RNN_model.parameters(), **optimizer_params)

                    scheduler_params = {
                        'start_factor' : 1.0,
                        'end_factor' : 0.001,
                        'total_iters' : num_epochs,
                    }
                    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
                    training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = RNN_model,
                        model_params=model_params_RNN_fp,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=loss_function,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose
                    )

                    cnt += 1
    
    
    
    # ---------------------------------------------------#
    # --------------------- LSTM: Training --------------#
    # ---------------------------------------------------#

    # ---------------------------------------------------#
    # --------------------- Inferrence ------------------#
    # ---------------------------------------------------#
    


    # ---------------------------------------------------#
    # -------------------- LSTM setup -------------------#
    # ---------------------------------------------------#

    # ------------- LSTM ---------------- #
    LSTM_drop_out_rates = [0.5]
    LSTM_activations = [nn.CELU]
    LSTM_normalization = nn.LayerNorm
    
    LSTM = True
    if LSTM:
        cnt = 0
        for activation in LSTM_activations:
            for dropout_rate in LSTM_drop_out_rates: 
                

                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    model_params_LSTM_fp = {
                            'activation' : activation,
                            'output_size' : 1,
                            'normalize' : LSTM_normalization,
                            'dropout_rate' : dropout_rate,
                            }
                    LSTM_model = neural_nets.LSTM_fixed_dimensions(**model_params_LSTM_fp)    

                    # ---- Training ----- #
                    optimizer_params = {
                        'lr' : 0.0001,
                    }
                    optimizer = torch.optim.Adam(LSTM_model.parameters(), **optimizer_params)

                    scheduler_params = {
                        'start_factor' : 1,
                        'end_factor' : 0.001,
                        'total_iters' : num_epochs,
                    }
                    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
                    training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        vol_augmentation= vol_augmentation,
                        aug_volume = aug_volume,
                        noise_std = noise_std,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = LSTM_model,
                        model_params=model_params_LSTM_fp,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=loss_function,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose, 
                        uncertainty=uncertainty
                    )

                    cnt += 1
    
    
    
    
    
    
    # ---------------------------------------------------#
    # -------------------- LSTM setup UNCERTAINTY  ------#
    # ---------------------------------------------------#
    
    
    class UncertaintyWeightedLoss(nn.Module):
        def __init__(self, weights=(1, 0.5, 0.1)):
            """
            Loss function that dynamically adjusts sample importance based on uncertainty.
            
            Args:
                weights (tuple): Weights for (prediction loss, confidence loss, uncertainty error correction).
            """
            super().__init__()
            self.weights = weights 

        def conf_err_fun(self, err, var):
            """
            
            Args:
                err (torch.Tensor): Squared error between prediction and target.
                var (torch.Tensor): Model variance (uncertainty).
            
            Returns:
                torch.Tensor: Scaled error based on uncertainty
            """
            e = err - var #if var is small, model is very confident, but if error is large, this penalty gets squared! 
            mask = e < 0  # Identify negative errors
            #if e is negative: uncertainty is too large for given error, we reduce penalty in this case: by muliplying by 0.1
            e[mask] = torch.abs(e[mask]) * 0.1  # Reduce penalty for negative errors
            e[~mask] = e[~mask] ** 2  # model is too confident! Square positive errors to penalize overconfidence
            return e

        def forward(self, mu, var, target):
            """            
            Args:
                mu (torch.Tensor): Predicted mean.
                var (torch.Tensor): Predicted variance (uncertainty).
                target (torch.Tensor): True values.
            
            Returns:
                torch.Tensor: Loss value.
            """
            # Ensure variance is positive (numerical stability)
            var = torch.exp(var) + 1e-6  # Exponentiate because var is stored as log(σ²)

            # squared error
            err = (mu - target) ** 2

            # onfidence error correction
            conf_weighted_err = self.conf_err_fun(err, var)
            #***********************************************************************

            # Uncertainty-weighted prediction loss, downweight high-variance samples --> 
            # if uncertainty is high, pred_loss is smaller, model should learn less form it... 
            pred_loss = err / (2 * var)  

            # Confidence regularization loss:Penalize excessive uncertainty
            conf_loss = 0.5 * torch.log(var)  

            # Final loss: weighted sum
            w_pred, w_conf, w_corr = self.weights
            return (w_pred * pred_loss.mean()) + (w_conf * conf_loss.mean()) + (w_corr * conf_weighted_err.mean())


    
    
    class ImprovedLoss(nn.Module):
        def __init__(self, w_pred=1.0, w_uncertainty=0.5, w_reg=0.1):
            """
            Improved loss function that penalizes excessive uncertainty.
            
            Args:
                w_pred (float): Weight for prediction accuracy.
                w_uncertainty (float): Weight for uncertainty modeling.
                w_reg (float): Weight for penalizing unnecessary uncertainty.
            """
            super().__init__()
            self.w_pred = w_pred
            self.w_uncertainty = w_uncertainty
            self.w_reg = w_reg  # New regularization term

        def forward(self, mu, var, target):
            """
            Compute the total loss.
            
            Args:
                mu (torch.Tensor): Predicted mean.
                var (torch.Tensor): Predicted variance (uncertainty).
                target (torch.Tensor): True values.
            
            Returns:
                torch.Tensor: Total loss.
            """
         

            # Mean Squared Error for volume prediction
            pred_loss = (mu - target) ** 2  

            # Gaussian Negative Log-Likelihood Loss
            nll_loss = (pred_loss / (2 * var)) + 0.5 * torch.log(var)

            # **New Regularization Term**: Penalize large `σ²` when `err` is small
            small_error_mask = (pred_loss < 0.05)  # If error is small, large uncertainty is penalized
            uncertainty_penalty = small_error_mask * var  # Directly penalizing high `σ²`

            # Weighted sum of losses
            total_loss = (
                self.w_pred * pred_loss.mean()
                + self.w_uncertainty * nll_loss.mean()
                + self.w_reg * uncertainty_penalty.mean()  # Penalizing unnecessary uncertainty
            )

            return total_loss

    uncertain_loss = ImprovedLoss(w_pred=1, w_uncertainty=0.6, w_reg=0.3)
    #uncertain_loss = ImprovedLoss(w_pred=1, w_uncertainty=0.5, w_reg=0.1)




    # ------------- LSTM ---------------- #
    LSTM_drop_out_rates = [0.5]
    LSTM_activations = [nn.CELU]
    LSTM_normalization = nn.LayerNorm

    
    LSTM = False
    if LSTM:
        cnt = 0
        for activation in LSTM_activations:
            for dropout_rate in LSTM_drop_out_rates: 
                

                    # Initialize network make sure to not set the parameters
                    # dicrectly in the constructor calls but in the forseen
                    # ..._params dictionaries, for storing and logging reasons
                    # specify neural network and optimizer
                    
                    model_params_LSTM_fp = {
                            'activation' : activation,
                            'output_size' : 1,
                            'normalize' : LSTM_normalization,
                            'dropout_rate' : dropout_rate
                            }
                    LSTM_model = neural_nets.LSTM_uncertainty(**model_params_LSTM_fp)    

                    # ---- Training ----- #
                    optimizer_params = {
                        'lr' : 0.0001,
                    }
                    optimizer = torch.optim.Adam(LSTM_model.parameters(), **optimizer_params)

                    scheduler_params = {
                        'start_factor' : 1,
                        'end_factor' : 0.001,
                        'total_iters' : num_epochs,
                    }
                    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
                    #scheduler = None 
                    
                    training.train_run(
                        dataset_path=dataset_path,
                        pretrained_transform=model_transform, 
                        output_path=Path(output_path) / f"mlp_run_{cnt}",
                        mapping_file=mapping_file,
                        value_column=value_column,
                        project_name=project_name,
                        run_notes=notes,
                        random_split_generator=random_split_generator,
                        augmentation_factor=augmentation_factor,
                        image_rotations=random_rotations,
                        vol_augmentation= vol_augmentation,
                        aug_volume = aug_volume,
                        noise_std = noise_std,
                        pretrained_model=pretrained_model,
                        min_seqence_lenght=mlp_max_sequence_len, # same as max seqence deliberately
                        max_seqence_lenght=mlp_max_sequence_len,
                        random_seqence_lenght=mlp_random_seqence_len,
                        model = LSTM_model,
                        model_params=model_params_LSTM_fp,
                        optimizer=optimizer,
                        optimizer_params=optimizer_params,
                        scheduler=scheduler,
                        scheduler_params=scheduler_params,
                        loss_function=uncertain_loss,
                        num_epochs=num_epochs,
                        batch_size=batch_size,
                        num_worker=num_worker,
                        experiment_creator=mlp_experiment_creator,
                        verbose=verbose, 
                        uncertainty=uncertainty
                    )

                    cnt += 1
    
    
    
    
    

    # ---------------------------------------------------#
    # ---------------- Attention setup ------------------#
    # ---------------------------------------------------#

    

    # -------------- Attention ------------ #
    if trained_model_path is None:
        attention_params = {
        'dropout_rate' : 0.5 
        }
        attention_model = neural_nets.AttentionBased(**attention_params)
    else:
        attention_model, attention_params = data_saver.Saver.load_model(trained_model_path)

    # --- model input --- #
    att_min_sequence_len = 12
    att_max_sequence_len = 12
    att_random_seqence_len = False
    #in case eval_random_sequence_len is True, a random number of images between
    #min and max is chosen for inference. 
    eval_min_sequence_len = 12
    eval_max_sequence_len = 12
    
    if not eval_max_sequence_len <= att_max_sequence_len: 
        raise IndexError("eval_max_sequence_len has to be smaller than att_max_sequence_len")

    #in case eval_random_sequence_len is False, the max number is chosen
    eval_random_sequence_len = False
    #if random_choice = False, then the first images of the plant are chosen
    #if random_choice = True, the images of the plants are choosen randomly
    random_choice = False
                            
    # ---- Training ----- #
    optimizer_params = {
        'lr' : 0.001, #10^-3 /10^-4
    }
    optimizer = torch.optim.AdamW(attention_model.parameters(), **optimizer_params)

    scheduler_params = {
        'start_factor' : 1,
        'end_factor' : 0.001,
        'total_iters' : 10,
    }
    scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
    
    #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=5, verbose=True)
    
    # ---  Evaluation settings ---#
    if num_of_img_inference == "single": 
        att_experiment_creator = SingleImageExperimentCreator(ignore_artificial=True)
    else: 
        att_experiment_creator = MultiImageExperimentCreator(ignore_artificial=True, 
                                                             eval_min_seq_len=eval_min_sequence_len, 
                                                             eval_max_seq_len=eval_max_sequence_len,
                                                             eval_random_seq_len=eval_random_sequence_len, 
                                                             random_choice=random_choice)
    attention = False
    if  attention:
        training.train_run(
            dataset_path=dataset_path,
            pretrained_transform=model_transform,
            output_path=Path(output_path),
            mapping_file=mapping_file,
            value_column=value_column,
            project_name=project_name,
            run_notes=notes,
            random_split_generator=random_split_generator, 
            augmentation_factor=augmentation_factor,
            image_rotations=random_rotations,
            vol_augmentation= vol_augmentation,
            aug_volume = aug_volume,
            pretrained_model=pretrained_model,
            min_seqence_lenght=att_min_sequence_len,
            max_seqence_lenght=att_max_sequence_len,
            random_seqence_lenght=att_random_seqence_len,
            model = attention_model,
            model_params=attention_params,
            optimizer=optimizer,
            optimizer_params=optimizer_params,
            scheduler=scheduler,
            scheduler_params=scheduler_params,
            loss_function=loss_function,
            num_epochs=num_epochs,
            batch_size=batch_size,
            num_worker=num_worker,
            experiment_creator=att_experiment_creator,
            verbose=verbose
        )
        
        
    #*************************************************************************
    #Code Jannis

    def warmup_multiplicative_schedule(e):
        return 0.001 if e < 40 else 1/ (e * 0.05)

    def get_model(path):
        from data_lib import data_saver
        path = Path(path)
        saver = data_saver.Saver(path.parent)
        return saver.load_model(path)


    random_sequence_len = True
    cache_dir = Path(r"data/cache")

    class MseLoss(nn.Module):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

            def forward(self, input: torch.Tensor, target: torch.Tensor, loss_scale) -> torch.Tensor:
                prediction = input[:, 0].squeeze()
                loss = ((prediction - target) ** 2 * loss_scale).mean()
                return loss
            
    class SingleMlpMseLoss(nn.Module):
        def __init__(self, weights = (1, 0.5, 0.3, 0.05), *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.weights = weights

        def conf_err_fun(self, err, conf):
                e = err - conf
                mask = e < 0
                e[mask] = torch.abs(e[mask]) * 0.1
                e[~mask] = e[~mask] ** 2
                return e

        def forward(self, input: torch.Tensor, target: torch.Tensor, loss_scale: torch.Tensor) -> torch.Tensor:
            input, conf, tpred, tconf = input
            mask = input != 0
            target = target.unsqueeze(1)
            loss_scale = loss_scale.unsqueeze(1)
            err_single_img = ((input - target) ** 2)
            conf_single_img = self.conf_err_fun(err_single_img, conf)
            err_single_img = (err_single_img * loss_scale)[mask]
            conf_single_img = (conf_single_img * loss_scale)[mask]
            err_combined = (tpred - target) ** 2 * loss_scale
            conf_combined = self.conf_err_fun(err_combined, tconf) * loss_scale
            wsi, wci, wec, wcc = self.weights
            return err_single_img.mean() * wsi + conf_single_img.mean() * wci + err_combined.mean() * wec + conf_combined.mean() * wcc

    Att = False
    if Att:

        min_sequence_len, max_sequence_len = (4, 6)
        train_setup = training_Jannis.TrainRunSetup(
            dataset_path=dataset_path,
            transform_train=get_transform(True),
            transform_eval=get_transform(False),
            output_path=Path(output_path) / fr"DmapEncoded",
            mapping_dir=mapping_file,
            pretrained_model=pretrained_model,
            max_seqence_lenght=max_sequence_len,
            min_seqence_lenght=min_sequence_len,
            random_seqence_lenght=random_sequence_len,
            num_workers=num_worker,
            eval_random_sequence_len=eval_random_sequence_len,
            eval_min_sequence_len=eval_min_sequence_len,
            eval_max_sequence_len=eval_max_sequence_len,
            cache_dir=cache_dir,
            n_dupli_train=5,
            wandb_config={},
            wandb_mode="disabled",
            verbose=True
        )


        def att_fun(train_setup: training_Jannis.TrainRunSetup, distill_phase: int):
            if distill_phase == 0:
                model = neural_nets.SingleMlp()
                epochs = 1000
                loss = SingleMlpMseLoss()
                    #model, _ = get_model(r"F:\experiments_output\checks_without_2024\DistillTests\saved_models\best_model_phase0.pth")
            elif distill_phase == 1:
                train_setup.train_dataset.no_label_scale = 0.9
                model = neural_nets.AttentionBased_Jannis()
                loss = MseLoss()
                train_setup.train_dataset.max_seq_len = 6
                train_setup.train_dataset.min_seq_len = 3
                epochs = 500
                model, _ = get_model(r"data/Model_Jannis/best_model_phase1.pth")
            elif distill_phase == 2:
                train_setup.train_dataset.no_label_scale = 0.9
                model = neural_nets.AttentionBased_Jannis()
                loss = MseLoss()
                train_setup.train_dataset.max_seq_len = 6
                train_setup.train_dataset.min_seq_len = 3
                epochs = 500
                model, _ = get_model(r"data/Model_Jannis/best_model_phase1.pth")
            else:
                pass
            optimizer_params = {
                'lr' : 0.001,
            }
            optimizer = torch.optim.Adam(model.parameters(), **optimizer_params)
            scheduler_params = {
                    'lr_lambda': warmup_multiplicative_schedule,
            }
            scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, **scheduler_params)
            return model, {}, optimizer, scheduler, loss, epochs
    

        training_Jannis.train_run_nn(
            train_setup=train_setup,
            model_fun=att_fun,
            start=0,
            num_phases=1
        )
            
        
        

        

if __name__ == "__main__":
    main_training()
    
   