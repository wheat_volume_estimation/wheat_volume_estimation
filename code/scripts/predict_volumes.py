import click
import pandas as pd
import torch

from model_lib.inferrer import Inferrer, SingleImageExperimentCreator, MultiImageExperimentCreator
from data_lib.data_sets import ImageInferenceDataset
from icecream import ic
from pathlib import Path
from torchvision import models

# ---------------------------------------------------#
# --------------------- Inferrence ------------------#
# ---------------------------------------------------#
    
#Prediction based in single images ("single") 
# or multiple images ("multiple")
num_of_img_inference = "single"

#in case eval_random_sequence_len is True, a random number of images between
#min and max is chosen for inference. 
eval_min_sequence_len = 3
eval_max_sequence_len = 6
#in case eval_random_sequence_len is False, the max number is chosen
eval_random_sequence_len = False
#if random_choice = False, then the first images of the plant are chosen
#if random_choice = True, the images of the plants are choosen randomly
random_choice = False


@click.command()
@click.option('--image-path', type=str, required=True, help='Path to the folder containing images')
@click.option('--model-path', type=str, required=True, help='Path to the folder containing the pretrained model')
@click.option('--output-path', type=str, required=True, help='Path to the output folder')
@click.option("--plant-mapping", type=str, required=True, help="Path to the plant mapping to create the test inputs")
@click.option("--output-name", type=click.Choice(["volume", "total"], case_sensitive=False), default="volume", help="Define what to predict: volume or number of spikelets (total)")
def main(image_path : str, model_path : str, output_path : str, output_name : str, plant_mapping : str):
    output_name = output_name.lower()
    image_path = Path(image_path)
    output_path = Path(output_path)
    model_path = Path(model_path)
    plant_mapping = Path(plant_mapping)
    if output_path.exists():
        print(f"Make sure the output path does not already exist!")
        return

    # choose some model and initialize PRETRAINED weights
    pretrained_model = models.resnet18(weights=models.ResNet18_Weights.IMAGENET1K_V1) 
    # Remove the last layer to access the embeddings
    pretrained_model.fc = torch.nn.Identity()

    # Compute Predictions for all data including test data
    inferrer = Inferrer(saved_model_path=model_path, output_name=output_name, pretrained_model=pretrained_model)
    plant_mapping = pd.read_json(plant_mapping, orient='index', convert_axes=False)
    if num_of_img_inference == "single": 
        experiment_creator = SingleImageExperimentCreator(ignore_artificial=True)
    else: 
        experiment_creator = MultiImageExperimentCreator(ignore_artificial=True, 
                                                             eval_min_seq_len=eval_min_sequence_len, 
                                                             eval_max_seq_len=eval_max_sequence_len,
                                                             eval_random_seq_len=eval_random_sequence_len, 
                                                             random_choice=random_choice)
        
    experiment = experiment_creator.create_experiment(plant_mapping=plant_mapping)
    dataset = ImageInferenceDataset(image_dir=image_path, experiment=experiment)
    experiment_output = inferrer.infer_experiment(dataset=dataset)
    experiment_output.to_json(output_path, orient='index')

if __name__ == "__main__":
    main()
