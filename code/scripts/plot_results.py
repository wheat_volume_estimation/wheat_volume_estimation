import click
from pathlib import Path
import pandas as pd
import numpy as np
from icecream import ic
import matplotlib.pyplot as plt
import scipy.stats as st
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from tueplots import bundles
import re 
    
def plot_results(true_volume: pd.DataFrame, df_true_1 : str, df_estim_1 : pd.DataFrame, df_true_2 : str, df_estim_2: pd.DataFrame, df_true_3 : str, df_estim_3 : pd.DataFrame, df_true_4 : str, df_estim_4 : pd.DataFrame):
    """Compute and print statistics

    Args:
        df_true (pd.DataFrame): True / measured volumes
        df_estim (pd.DataFrame): Estimated volumes
    """
    #if baseline results are used, remove the all volumes < 3000 where the segmentation failed
    
    # Check if the filename contains "baseline"
    #if baseline == True:
        # Remove rows with values smaller than 3000 in the "volume" column because segmentation failed 
        # df_estim = df_estim[df_estim['volume'] >= 3000]
       
    #intersection of dataframe 1: 
    df_true_1_ids = df_true_1.index
    df_estim_1_ids = df_estim_1.index
    intersection_ids_1 = set(df_true_1_ids) & set(df_estim_1_ids)
    ic(len(intersection_ids_1))
    #print(f"Stats for estimations")
    #print(f"Number of true images {df_true.shape[0]}")
    #print(f"Number of estimated images {df_estim.shape[0]}")
    #print(f"Images contained in both {len(intersection_ids)}")
    np_intersection_ids_1 = np.array(intersection_ids_1)
    true_volumes_1 = np.array(df_true_1.loc[np_intersection_ids_1,"volume"])
    ic(len(true_volumes_1))
    estim_volumes_1 = np.array(df_estim_1.loc[np_intersection_ids_1,"volume"])
    ic(len(estim_volumes_1))
    
    #readd scaling that was done to calculate mape based on the training set 
    #estim_volumes_1 = estim_volumes_1 + 139.611725431183
    
    #intersection of dataframe 1: 
    df_true_2_ids = df_true_2.index
    df_estim_2_ids = df_estim_2.index
    intersection_ids_2 = set(df_true_2_ids) & set(df_estim_2_ids)
    ic(len(intersection_ids_2))
    #print(f"Stats for estimations")
    #print(f"Number of true images {df_true.shape[0]}")
    #print(f"Number of estimated images {df_estim.shape[0]}")
    #print(f"Images contained in both {len(intersection_ids)}")
    np_intersection_ids_2 = np.array(intersection_ids_2)
    true_volumes_2 = np.array(df_true_2.loc[np_intersection_ids_2,"volume"])
    ic(len(true_volumes_2))
    estim_volumes_2 = np.array(df_estim_2.loc[np_intersection_ids_2,"volume"])
    ic(len(estim_volumes_2))
    
    #readd scaling that was done to calculate mape based on the training set 
    #estim_volumes_2 = estim_volumes_2 - 122.15693727814596
    
    #intersection of dataframe 1: 
    df_true_3_ids = df_true_3.index
    df_estim_3_ids = df_estim_3.index
    intersection_ids_3 = set(df_true_3_ids) & set(df_estim_3_ids)
    ic(len(intersection_ids_3))
    #print(f"Stats for estimations")
    #print(f"Number of true images {df_true.shape[0]}")
    #print(f"Number of estimated images {df_estim.shape[0]}")
    #print(f"Images contained in both {len(intersection_ids)}")
    np_intersection_ids_3 = np.array(intersection_ids_3)
    true_volumes_3 = np.array(df_true_3.loc[np_intersection_ids_3,"volume"])
    ic(len(true_volumes_3))
    estim_volumes_3 = np.array(df_estim_3.loc[np_intersection_ids_3,"volume"])
    ic(len(estim_volumes_3))
    
    #readd scaling that was done to calculate mape based on the training set 
    #estim_volumes_3 = estim_volumes_3 - 1437.23581735877
    
    #intersection of dataframe 1: 
    df_true_4_ids = df_true_4.index
    df_estim_4_ids = df_estim_4.index
    intersection_ids_4 = set(df_true_4_ids) & set(df_estim_4_ids)
    ic(len(intersection_ids_4))
    #print(f"Stats for estimations")
    #print(f"Number of true images {df_true.shape[0]}")
    #print(f"Number of estimated images {df_estim.shape[0]}")
    #print(f"Images contained in both {len(intersection_ids)}")
    np_intersection_ids_4 = np.array(intersection_ids_4)
    true_volumes_4 = np.array(df_true_4.loc[np_intersection_ids_4,"volume"])
    ic(len(true_volumes_4))
    estim_volumes_4 = np.array(df_estim_4.loc[np_intersection_ids_4,"volume"])
    ic(len(estim_volumes_4))
    
    #readd scaling that was done to calculate mape based on the training set 
    #estim_volumes_4 = estim_volumes_4 - 40.11666832495757
        
    # Create a list of true and predicted value arrays and colors
    true_arrays = [true_volumes_1, true_volumes_2, true_volumes_3, true_volumes_4]
    predicted_arrays = [estim_volumes_1, estim_volumes_2, estim_volumes_3, estim_volumes_4]
    colors = ['red', 'green', 'blue', 'black']
    
    # Create a scatterplot
    setting = bundles.icml2022(usetex=False)
    plt.rcParams.update(setting)
    fig, ax = plt.subplots()
    
    labels = ["artificial color", "mixed grayscale", "mixed color", "real images"]

    for i in range(len(true_arrays)):
        ax.scatter(true_arrays[i], predicted_arrays[i], label=labels[i], s=0.1, color=colors[i])
        # Plotting the diagonal line (perfect predictions)
        # Adding labels and title
    plt.xlabel('True Volumes [mm³]')
    plt.ylabel('Predicted Volumes [mm³]')
    
    plt.legend()
    ax.plot([min(true_volume["volume"]), max(true_volume["volume"])],
                [min(true_volume["volume"]), max(true_volume["volume"])],
                linestyle='--', color='red', label='Perfect Predictions', linewidth=0.2)
    
    # Save the plot to a file (e.g., PNG, PDF, etc.)
    plt.savefig("/home/zumstego/public/Evaluation/Projects/KP0032_zumstego/Results_model2.png")
    
    # Plotting the scatter plot
    #plt.scatter(true_volumes_1, estim_volumes_1, color='black', label='True vs Predicted', marker='o', s=4)
    # Plotting the diagonal line (perfect predictions)
    #plt.plot([min(true_volume), max(true_volume)], [min(true_volume), max(true_volume)], linestyle='--', color='red', label='Perfect Predictions')
    # Adding labels and title
    #plt.xlabel('True Volumes [mm³]')
    #plt.ylabel('Predicted Volumes [mm³]')
    #plt.title(name)
    #plt.legend()
    # Adding R-squared value as text annotation
    #plt.text(0.05, 0.9, f'R²: {r_squared:.2f}', transform=plt.gca().transAxes, color='black')

    # Display the plot
    #plt.show()
    
    
@click.command()
@click.option("--true_volume", type=str, default=None, help="Path to csv file containing true volumes")
@click.option("--estimation_1", type=str, default=None, help="Path to csv file containing estimated volumes")
@click.option("--volume_1", type=str, required=True, help="Path to csv file containing measured volumes")
@click.option("--estimation_2", type=str, default=None, help="Path to csv file containing estimated volumes")
@click.option("--volume_2", type=str, required=True, help="Path to csv file containing measured volumes")
@click.option("--estimation_3", type=str, default=None, help="Path to csv file containing estimated volumes")
@click.option("--volume_3", type=str, required=True, help="Path to csv file containing measured volumes")
@click.option("--estimation_4", type=str, default=None, help="Path to csv file containing estimated volumes")
@click.option("--volume_4", type=str, required=True, help="Path to csv file containing measured volumes")

def main(true_volume : str, volume_1 : str, estimation_1 : str, volume_2 : str, estimation_2 : str, volume_3 : str, estimation_3 : str, volume_4 : str, estimation_4 : str):
    true_volume = Path(true_volume)
    volume_1 = Path(volume_1)
    volume_2 = Path(volume_2)
    volume_3 = Path(volume_3)
    volume_4 = Path(volume_4)
    df_true_volume = pd.read_csv(true_volume, index_col="img_name")
    df_true_1 = pd.read_csv(volume_1, index_col="img_name")
    df_true_2 = pd.read_csv(volume_2, index_col="img_name")
    df_true_3 = pd.read_csv(volume_3, index_col="img_name")
    df_true_4 = pd.read_csv(volume_4, index_col="img_name")
    
    estimation_1 = Path(estimation_1)
    estimation_2 = Path(estimation_2)
    estimation_3 = Path(estimation_3)
    estimation_4 = Path(estimation_4)
    df_estim_1 = pd.read_csv(estimation_1, index_col="img_name")
    df_estim_2 = pd.read_csv(estimation_2, index_col="img_name")
    df_estim_3 = pd.read_csv(estimation_3, index_col="img_name")
    df_estim_4 = pd.read_csv(estimation_4, index_col="img_name")

    
    
    plot_results(true_volume=df_true_volume, df_true_1=df_true_1, df_estim_1=df_estim_1, df_true_2=df_true_2, df_estim_2=df_estim_2, df_true_3=df_true_3, df_estim_3=df_estim_3, df_true_4=df_true_4, df_estim_4=df_estim_4)
    
if __name__ == "__main__":
    #main(true_volumes="data/vol_mapping.csv", estimations="data/baseline_volumes.csv")
    main()