import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import numpy as np
from sklearn.preprocessing import MinMaxScaler

#aim: plot baseline, area or geometrical baseline. And the distribution
#todo: adjust paths


# Read the dataframe from a file (e.g., CSV)
df = pd.read_csv('data/area_volume/area_volume.csv')


# Plot the distribution of the column
plt.hist(df["Number_of_Ones"], bins=20, color='skyblue', edgecolor='black')
plt.xlabel('Values')
plt.ylabel('Frequency')
plt.title('Distribution of {}'.format("Number_of_Ones"))
plt.grid(True)
plt.show()

plt.savefig('data/area_volume/distribution_plot.png')

#values larger than 650000 seem to be a bit strange lets check them 

# Set the threshold
threshold = 650000

# Filter the DataFrame based on the threshold condition
filtered_rows = df[df['Number_of_Ones'] < 300000]
#larger than 650000
#2_6_8
#3_6_3

#smaller than 300000
#23_6_3
#8_10_10
#8_10_2
#lets check the segmented images of this plants, nothing seems to be weird

# Print the filtered rows
print(filtered_rows)

########################################################################################
#Plot
########################################################################################

# Calculate the correlation, r2 and mape between the 'Volume' and 'Number_of_Ones' columns
df_pred = pd.read_csv('data/baseline/vol_baseline.csv')
df_true = pd.read_csv('data/baseline/volume.csv')


# Function to extract the first three parts of the img_id
def get_plant_id(img_id):
    parts = img_id.split('_')
    plant_id = '_'.join(parts[:3])
    return plant_id

df_pred['Plant_id'] = df_pred['img_id'].apply(get_plant_id)

df_grouped = df_pred.groupby('Plant_id')['volume'].mean().reset_index()
# Renaming the column
#df_grouped.rename(columns={'Plant_id': 'Plant_id'}, inplace=True)

# Merge the dataframes based on the 'Plant_id' column
merged_df = pd.merge(df_true, df_grouped, on='Plant_id', how='inner')

# Remove rows where 'volume' is less than 500
merged_df = merged_df[merged_df['volume'] >= 2000]


#save the dataframe
merged_df.to_csv('data/baseline/merged.csv', index=False)

volume_true=np.array(merged_df['Volume'])
volume_pred=np.array(merged_df['volume'])

scale_factor = np.mean(volume_true) / np.mean(volume_pred)
y_pred_rescaled = volume_pred * scale_factor
print(y_pred_rescaled)

r2_score_rescaled = r2_score(volume_true, y_pred_rescaled)
print(r2_score_rescaled)

correlation = np.corrcoef(volume_true, y_pred_rescaled)
cor = correlation[0,1]
print(cor)


def mean_absolute_percentage_error(y_true, y_pred): 
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        mask = y_true != 0  # Avoid division by zero
        return np.mean(np.abs((y_true[mask] - y_pred[mask]) / y_true[mask])) * 100
    

MAPE = mean_absolute_percentage_error(volume_true, y_pred_rescaled)
print(MAPE)



# Plotting
plt.figure(figsize=(8, 6))
plt.scatter(volume_true, y_pred_rescaled, color="black")

# Set logarithmic scale for y-axis
# plt.yscale('log')

# Labeling
plt.xlabel('True volume', fontsize = 18)
plt.ylabel('Predicted volume', fontsize=18)
#plt.title('Area vs Volume', fontsize = 18)
# Add the correlation coefficient and R² score to the plot
plt.plot(volume_true, volume_true, 'r--', label='x = y')
    
plt.text(0.15, 0.92, f'r: {cor:.2f}', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')
plt.text(0.15, 0.87, f'R²: {r2_score_rescaled:.2f}', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')


# Show plot
plt.grid(True)
plt.show()

plt.savefig('data/baseline/correlation.png')




