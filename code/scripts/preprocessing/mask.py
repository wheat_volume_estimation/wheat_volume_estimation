import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import json

#extract the number of 1, sum them up to get the area and save the correlation and the 
#dataframes


# Load the JSON file
with open('data/lstm_good_epoch/mlp_run_0/mapping_val.json', 'r') as f:
    data = json.load(f)
    
# Initialize empty lists to store the data
plant_ids = []
volumes = []

# Iterate through each entry in the JSON data
for plant_id, info in data.items():
    # Extract the 'volume' information
    volume = info['volume']
    
    # Append the 'Plant_id' and 'volume' to the lists
    plant_ids.append(plant_id)
    volumes.append(volume)

# Create a dataframe from the lists
df_json = pd.DataFrame({'Plant_id': plant_ids, 'Volume': volumes})

#save the dataframe
df_json.to_csv('data/area_volume/volume.csv', index=False)

#########################################################################

directory = Path("data/bar_masks")

# Initialize an empty list to store the data
data_list = []
data_test = []

# Iterate through each .npz file in the directory
for file_path in directory.glob("*.npz"):
    
    
    filename = file_path.name

    data = np.load(file_path)

    array = data['arr_0']
    
    # Convert boolean values to integers (True -> 1, False -> 0)
    array_int = array.astype(int)
    #plt.imshow(array_int, cmap='gray')
    #plt.savefig('data/area_volume/test.png')
    
    array_int_inverted = ~array_int
    #plt.imshow(array_int_inverted, cmap='gray')
    #plt.savefig('data/area_volume/test_inverted.png')
    
    # Compute the sum of ones in the matrix
    sum_of_ones = np.sum(array_int)

    # Append the filename and the sum of ones to the data list
    data_list.append({'Filename': filename, 'Number_of_Ones': sum_of_ones})
    
    # Close the .npz file
    data.close()

# Create a dataframe from the data list
df = pd.DataFrame(data_list)
print(df)

# Remove the '.npz' extension from the filenames
df['Filename'] = df['Filename'].str.rstrip('.npz')

# Create a new column with the modified filenames
df['Plant_id'] = df['Filename'].str.rsplit('_', n=1).str[0]

# Group the rows based on the modified filename and calculate the mean of 'Number_of_Ones'
df_grouped = df.groupby('Plant_id')['Number_of_Ones'].mean().reset_index()

#save the dataframe
df_grouped.to_csv('data/area_volume/area.csv', index=False)

###################################################################



# Merge the dataframes based on the 'Plant_id' column
merged_df = pd.merge(df_json, df_grouped, on='Plant_id', how='inner')

#save the dataframe
merged_df.to_csv('data/area_volume/area_volume.csv', index=False)

# Calculate the correlation between the 'Volume' and 'Number_of_Ones' columns
correlation = merged_df['Volume'].corr(merged_df['Number_of_Ones'])

# Print the correlation coefficient
print("Correlation between Volume and Number_of_Ones:", correlation)

#correlation 0.85!