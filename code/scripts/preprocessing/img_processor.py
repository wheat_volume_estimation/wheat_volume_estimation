
from PIL import Image
import os
import argparse

class Preprocessor:

    def __init__(self, PATH: str, output: str) -> None:
        self.path = PATH
        self.output = output

    def process_images(self, size=256):
        """
        Iterates through every picture in the folder at self.path and resizes the image to the dimension given by size.
        """
        for i, filename in enumerate(os.listdir(self.path)):
            if i % 100 == 0:
                print(f"Processing #{i}th file.")
            if filename.endswith(('.jpg', '.jpeg', '.png')):
                file_path = os.path.join(self.path, filename)
                image = Image.open(file_path)
                
                # Center crop to smaller dimension
                # PIL.size = (width, height)
                width_lt_height = image.size[0] <= image.size[1]
                smaller_dimension = min(image.size)
                larger_dimnesion = max(image.size)
                difference_2 = (larger_dimnesion - smaller_dimension) // 2
                
                # compute corp area
                left = 0 if width_lt_height else difference_2
                right = smaller_dimension if width_lt_height else smaller_dimension + difference_2
                top =  0 if not width_lt_height else difference_2
                bottom = smaller_dimension if not width_lt_height else smaller_dimension + difference_2

                # Resize the image
                cropped_image = image.crop((left, top, right, bottom))
                resized_image = cropped_image.resize((size, size))

                # Ensure the output directory exists
                os.makedirs(self.output, exist_ok=True)

                # Save the resized image
                output_path = os.path.join(self.output, filename)
                resized_image.save(output_path)

def main():
    parser = argparse.ArgumentParser(description='Image Preprocessor')
    parser.add_argument('--path', type=str, required=True, help='Path to the folder containing images')
    parser.add_argument('--output', type=str, required=True, help='Path to the output folder')
    parser.add_argument('--size', type=int, default=256, help='Size for resizing images (default: 256)')

    args = parser.parse_args()

    processor = Preprocessor(PATH=args.path, output=args.output)
    processor.process_images(size=args.size)

if __name__ == "__main__":
    main()

