import pandas as pd
from pathlib import Path
import shutil

#used to copy needed images to a special folder in order to choose this one
#not really neccessary anymore 


# Set the paths to your folders
data_folder = Path("data/area_volume/")
image_folder = Path("data/images_no_bar")
output_folder = Path("data/validation_set")

# Read your dataframe
df = pd.read_csv(data_folder / "volume.csv")  # Adjust file format if necessary

# List the images in the image folder
images = list(image_folder.glob("*.jpg"))

# Extract the common part of the image filenames
common_part_images = [image.stem.split("_")[:3] for image in images]

# Extract the common part of the filenames in the dataframe
common_part_df = [filename.split("_")[:3] for filename in df["Plant_id"]]

# Find the matching common parts
matching_common_parts = [part for part in common_part_images if part in common_part_df]

# Filter the images based on the matching common parts
images_to_copy = [image for image in images if image.stem.split("_")[:3] in matching_common_parts]

# Create the output folder if it doesn't exist
output_folder.mkdir(parents=True, exist_ok=True)

# Copy the filtered images to the output folder
for image in images_to_copy:
    shutil.copy(image, output_folder / image.name)