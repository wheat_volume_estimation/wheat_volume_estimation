from pathlib import Path
import torch
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader, TensorDataset
from torch.utils.data import DataLoader, random_split
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from torchvision.models import resnet50, ResNet50_Weights
import numpy as np 
import torch.nn.functional as F
from PIL import Image
import pandas as pd
from icecream import ic

from sklearn.model_selection import train_test_split
import wandb

# Define a debug flag
debug = False

# Initialize wandb conditionally
if debug:
    wandb.init(project='spike-classification', mode='disabled')
else:
    wandb.init(project='spike-classification')
    
    
device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
if torch.cuda.is_available():
    print(torch.cuda.get_device_name())
else:
    print("Using CPU!")
    
    
data_dir = 'data/spikes_real_images'
batch_size = 8

seed = 223
torch.manual_seed(seed)

"""
Transform, resize and normalize the images and then use a pretrained model to extract 
the embeddings.
"""

# Define the transformations
transform = transforms.Compose([transforms.Resize(size=232), 
                                    transforms.CenterCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize(mean=[0.485,0.456,0.406], std=[0.229, 0.224, 0.225])
                                    ])


# Load the dataset
dataset = ImageFolder(root=data_dir, transform=transform)

#imiagefolder: stores paths to the image files in its samples 
#stores each class as index, in each class we have the image paths

# Define the split sizes
train_size = int(0.9 * len(dataset))
test_size = len(dataset) - train_size

# Split the dataset
train_dataset, test_dataset = random_split(dataset, [train_size, test_size])



#save test data set to check that everything is fine
df_class_indices = [dataset.targets[i] for i in test_dataset.indices]
df_filenames = [dataset.samples[s][0].split('/')[-1].replace('.jpg', '') for s in test_dataset.indices]


#save training data set to check that everything is fine
df_class_indices_train = [dataset.targets[i] for i in train_dataset.indices]
df_filenames_train = [dataset.samples[s][0].split('/')[-1].replace('.jpg', '') for s in train_dataset.indices]



# Create a DataFrame from the lists
data_test = {
    'Filename': df_filenames,
    'Class': df_class_indices,
    'Index': test_dataset.indices
}
df_test = pd.DataFrame(data_test)

# Save the DataFrame to a CSV file
df_test.to_csv('data/spikes_real_images/test_set.csv', index=False)


# Create a DataFrame from the lists
data_train = {
    'Filename': df_filenames_train,
    'Class': df_class_indices_train,
    'Index': train_dataset.indices
}
df_train = pd.DataFrame(data_train)

# Save the DataFrame to a CSV file
df_train.to_csv('data/spikes_real_images/train_set.csv', index=False)


#ImageFolder will assign labels as follows:
#spike_side_view will be assigned label 0.
#spike_front_view will be assigned label 1.
#spike_mixed_view will be assigned label 2.

loader = DataLoader(dataset=dataset,
                        batch_size=batch_size,
                        shuffle=False,
                        pin_memory=True, num_workers=1)



def generate_embeddings():
    """
    Transform, resize and normalize the images and then use a pretrained model to extract 
    the embeddings.
    """
    model = torch.hub.load("facebookresearch/dinov2", "dinov2_vitl14")
    embedding_size = 1024
    # pick your model
    num_images = len(dataset)
    embeddings = np.zeros((num_images, embedding_size))
    # TODO: Use the model to extract the embeddings. Hint: remove the last layers of the 
    # model to access the embeddings the model generates. 
    model.fc = torch.nn.Identity()
    model.to(device)

    embed = []
    with torch.no_grad():
        for i, (batch, _) in enumerate(loader):
            batch = batch.to(device)
            output = model(batch)        
            embed.append(output)
            
            if i % 5 == 0: 
                    print("epoch = ", i)
            
    embeddings = torch.cat(tensors=embed, dim=0)   
    np.save('data/spikes_real_images/embeddings.npy', embeddings.cpu())
    
#generate_embeddings()


def get_data(data_dir, train=True):
    """
    Load the images and generate the features and labels.
    input: file: string, the path to the file containing the triplets
          train: boolean, whether the data is for training or testing

    output: X: numpy array, the features
            y: numpy array, the labels
    """
 

    #save labels
    # Extract class indices
    # Load the dataset
    dataset = ImageFolder(root=data_dir, transform=None)
    
    if train==True:
        # Extract class indices for the training set
        class_indices = [dataset.targets[i] for i in train_dataset.indices]
        filenames = [dataset.samples[s][0].split('/')[-1].replace('.jpg', '') for s in train_dataset.indices] #splitted pfad bei / und nimmt das letzte (filename)
    else: 
        # Extract class indices for the test set
        class_indices = [dataset.targets[i] for i in test_dataset.indices]
        filenames = [dataset.samples[s][0].split('/')[-1].replace('.jpg', '') for s in test_dataset.indices]
  
    #save embeddings 
    embeddings = np.load('data/spikes_real_images/embeddings.npy')
    # TODO: Normalize the embeddings
    
    mean = np.mean(embeddings, axis=0)
    std = np.std(embeddings, axis=0)
    embeddings = (embeddings - mean) / std
    
 
    file_to_embedding = {}
    for i in range(len(filenames)):
        file_to_embedding[filenames[i]] = embeddings[i]
    X = []
    y = []
    # use the individual embeddings to generate the features and labels for triplets
    #TODO shuffle triplets before the loop
    # triplets = np.random.shuffle(triplets)
    
    emb = [file_to_embedding[a] for a in filenames] #a: filenames, wir nehmen die embeddings von diesen filenames, liste mit 3 elementen: die 3 embeddings 
    X.append(emb) #concat: 1 dimension, alle aneinander , horizontal stack: 2 dimension, untereinander angefügt oder hintereinander


    X = np.array(X)
    X = np.squeeze(X)

    y = np.array(class_indices)
    y = np.squeeze(y)
    return X, y

X, y = get_data(data_dir, train=True)



#split the dataset into trainingsdataset and validationset
X_train, X_val, y_train, y_val = train_test_split(X,y, test_size=0.2, random_state=42, shuffle=True) #X_train: teil der bilder (# bilder), 1024 embedding size
training_size = len(X_train)
validation_size = len(X_val) 
ic(training_size)
ic(validation_size)


def create_loader_from_np(X, y = None, train = True, batch_size=64, shuffle=True, num_workers = 4):
    """
    Create a torch.utils.data.DataLoader object from numpy arrays containing the data.

    input: X: numpy array, the features
           y: numpy array, the labels
    
    output: loader: torch.data.util.DataLoader, the object containing the data
    """
    if train:
        # Attention: If you get type errors you can modify the type of the
        # labels here
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float), 
                                torch.from_numpy(y).type(torch.long))
    else:
        dataset = TensorDataset(torch.from_numpy(X).type(torch.float), 
                                torch.from_numpy(y).type(torch.long))
    loader = DataLoader(dataset=dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        pin_memory=True, num_workers=num_workers)
    return loader



train_loader = create_loader_from_np(X=X_train, y=y_train, train = True, batch_size=batch_size)
val_loader = create_loader_from_np(X=X_val, y=y_val, train = True, batch_size=batch_size)

del X
del y

class Net(nn.Module):
    """
    The model class, which defines our classifier.
    """
    def __init__(self):
        """
        The constructor of the model.
        """
        super().__init__()
        self.fc1 = nn.Linear(1024, 2048) #dinov2
        #self.fc2 = nn.Linear(2048, 1024) 
        self.fc2 = nn.Linear(2048, 1024) 
        self.fc3 = nn.Linear(1024, 512) 
        self.fc4 = nn.Linear(512, 256) 
        self.fc5 = nn.Linear(256, 2)
        #self.fc4 = nn.Linear(512, 3)
        
        self.dropout = nn.Dropout(0.7)  # Dropout layer with p=0.2
        self.batch_norm1 = nn.BatchNorm1d(2048)  # BatchNorm1d for fc1 output
        self.batch_norm2 = nn.BatchNorm1d(1024)  # BatchNorm1d for fc2 output
        self.batch_norm3 = nn.BatchNorm1d(512)   # BatchNorm1d for fc3 output
        self.batch_norm4 = nn.BatchNorm1d(256)   # BatchNorm1d for fc3 output

    def forward(self, x):
        """
        The forward pass of the model.

        input: x: torch.Tensor, the input to the model

        output: x: torch.Tensor, the output of the model
        """
        
        x = self.fc1(x)
        x = self.batch_norm1(x)  # Apply batch normalization
        x = F.relu(x)
        x = self.dropout(x)  # Apply dropout
        
        x = self.fc2(x)
        x = self.batch_norm2(x)  # Apply batch normalization
        x = F.relu(x)
        x = self.dropout(x)  # Apply dropout
        
        x = self.fc3(x)
        x = self.batch_norm3(x)  # Apply batch normalization
        x = F.relu(x)
        x = self.dropout(x)  # Apply dropout
        
        x = self.fc4(x)
        x = self.batch_norm4(x)  # Apply batch normalization
        x = F.relu(x)
        x = self.dropout(x)  # Apply dropout
        
        x = self.fc5(x)
        
        #dropout 
        #batchnorm
      
        
        return x


# repeat for testing data
X_test, y_test = get_data(data_dir, train=False)
test_loader = create_loader_from_np(X_test, y_test, train = False, batch_size=batch_size, shuffle=False)
del X_test
del y_test


model = Net()

# Define loss function and optimizer
criterion = nn.CrossEntropyLoss()


# Training loop
num_epochs = 500

# ---- Training ----- #
optimizer_params = {
    'lr' : 0.005,
}
optimizer = torch.optim.Adam(model.parameters(), **optimizer_params)

scheduler_params = {
    'start_factor' : 1.0,
    'end_factor' : 0.001,
    'total_iters' : num_epochs,
}
scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, **scheduler_params)
            #scheduler = None 

model = model.to(device)

for epoch in range(num_epochs):
    model.train()
    training_loss = 0.0
    epoch_train_loss = 0.0
    epoch_accuracy=0.0 
    accuracy_train = []  
    predictions = []
    truths = [] 

    for X_train, y_train in train_loader:
        training_accuracy = 0.0
        correct = 0
        total = 0
        
        optimizer.zero_grad()
        X_train = X_train.to(device)
        y_train = y_train.to(device)
        model = model.to(device)
        
        outputs = model(X_train)
        loss = criterion(outputs, y_train)
        #loss = torch.mean(losses) ???
        loss.backward()
        optimizer.step()
        training_loss += loss.item() 
        
        _, predicted = torch.max(outputs, 1)
        # Save predictions and truths for logging
        predictions.extend(predicted.cpu().numpy())
        truths.extend(y_train.cpu().numpy())
    
    total_training = training_size
    predictions = np.array(predictions)
    truths = np.array(truths)
    correct += (predictions == truths).sum()
   
    training_accuracy = correct / total_training
    epoch_train_loss = training_loss / len(train_loader)

    n=1
    if epoch % n==n -1:
        print(f'Epoch {epoch+1}/{num_epochs}, Train Loss: {epoch_train_loss:.4f}, Train Acc: {training_accuracy:.4f}')
        training_loss = 0.0




    # Validation
    model.eval()
    
    with torch.no_grad():
        val_loss = 0.0
        validation_accuracy = 0.0
        correct = 0
        total = 0
        accuracy_val = []
        predictions = []
        truths = []
        
        for inputs, labels in val_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)
            model = model.to(device)

            
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            val_loss += loss.item() 
            
            
            _, predicted = torch.max(outputs, 1)
            
            # Save predictions and truths for logging
            predictions.extend(predicted.cpu().numpy())
            truths.extend(labels.cpu().numpy())
        
        total_validation = validation_size
        predictions = np.array(predictions)
        truths = np.array(truths)
        correct += (predictions == truths).sum()
    
          
        validation_accuracy = correct / total_validation
        epoch_val_loss = val_loss / len(val_loader)
      
    
        n=1
        if epoch % n==n -1:
            print(f'Val Loss: {epoch_val_loss:.4f}, Val Acc: {validation_accuracy:.4f}')
    
    scheduler.step()
    
    # Log test loss and accuracy to wandb
    wandb.log({
        "Training Loss": epoch_train_loss,
        "Validation Loss": epoch_val_loss,
        "Validation Accuracy": validation_accuracy,
        "epoch": epoch + 1
    })




#test
model.eval()
test_images = []
with torch.no_grad():
    test_loss = 0.0
    correct = 0
    total = 0
    accuracy_test = []
    predictions = []
    truths = []
   
    
    
    for inputs, labels in test_loader: #es kommt ein tuple raus
        inputs=inputs.to(device)
        labels=labels.to(device)
        model = model.to(device)

        outputs = model(inputs)
        _, predicted = torch.max(outputs, 1) 
        
        # Save predictions and truths for logging
        predictions.extend(predicted.cpu().numpy())
        truths.extend(labels.cpu().numpy())
        
    
    total = len(df_filenames)
 
    predictions = np.array(predictions)
    truths = np.array(truths)
    correct += (predictions == truths).sum()

    
    test_accuracy = correct / total

    print(f', Test Accuracy: {test_accuracy:.4f}')

# Log test loss and accuracy to wandb
wandb.log({"Test Accuracy": test_accuracy})


# Log predictions and images to wandb
class_labels = dataset.classes  # Get class labels from ImageFolder


    
#index: 


# test_images=[]
# subfolders = ['spikes_front_view', 'spikes_mixed_view']  
# data_dir = Path(data_dir)

# for filename in df_filenames: 
#     image_found = False
    
#     for subfolder in subfolders:
#         folder_path = data_dir / subfolder
#         image_extension = filename + ".jpg"
#         file_path = folder_path / image_extension
#         if file_path.exists():
#             image = Image.open(file_path)
#             test_images.append(image)
#             image_found = True
#     if not image_found: 
#         print("f'Image {filename} not found in any subfolder")


#for i in range(len(test_images)):
#    wandb.log({"Image": [wandb.Image(test_images[i], #channels, y, x 
#                    caption=f"Filename: {df_filenames[i]} Pred: {class_labels[predictions[i]]}, Truth: {class_labels[truths[i]]}")]})
    


print(df_filenames)
print(predictions)
print(truths)



#predictions: 0,1, or 2. 

#to do:

##accuracy per class 

# von 1. künstlichen, orientierung, die restlichen 5 davon ableiten 
# 6000 mehr trainingsdaten 

