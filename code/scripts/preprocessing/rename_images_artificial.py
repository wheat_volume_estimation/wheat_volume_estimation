from pathlib import Path
import pandas as pd
from icecream import ic

IMAGE_EXTENSIONS = [".jpg", ".png", ".jpeg"]

def rem_min_one():
    df_volumes = pd.read_csv("/home/zumstego/public/Evaluation/Projects/KP0032_zumstego/KP0032_2/2024/Lab_fotos/large_set/artificial_dataset/vol_mapping.csv")

    folder = "/home/zumstego/3d_link/special_scans_no_cone"
    folder = Path(folder)
    folder_files = [f.name for f in folder.iterdir()]

    for i, row in df_volumes.iterrows():
        
        plant_id = row['plant_id']
        volume = row['volume']
        txt_file = f'{plant_id}.txt'
        if txt_file in folder_files:
            volume_in_file = (folder / txt_file).read_text()
            vol = float(volume_in_file)
            if vol < 0:
                (folder / txt_file).write_text(str(volume))
                print(txt_file)
                print(str(volume))
                

def add_letter():
    
    folder = "/home/zumstego/public/Evaluation/Projects/KP0032_zumstego/KP0032_2/2024/Lab_fotos/large_set/lab_images_no_bar"
    folder = Path(folder)
    for file in folder.iterdir():
        if file.suffix == '.jpg':
            fname = f"{file.stem}_a{file.suffix}"
            print(fname)
            file.rename(file.parent / fname)

if __name__ == "__main__":
    add_letter()