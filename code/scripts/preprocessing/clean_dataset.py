
import os
from collections import defaultdict
import shutil
import pandas as pd
import re
from collections import Counter
#read in dataset containing images 
folder_path = "/projects/zumstego/1_datasets/images_no_bar_2023_clean/"



#extract names of images
image_groups = defaultdict(set)

for filename in os.listdir(folder_path): 
    
    if filename.endswith((".jpg")):
        parts = filename.split("_")
        group_key = "_".join(parts[:3])
        unique_id = parts[3].split(".")[0]
        
        image_groups[group_key].add(unique_id)

#new dictionary
image_counts = {group: len(ids) for group, ids in image_groups.items()}

#for group, count in image_counts.items():
    #print(f"Group {group}: {count} unique images")
    
    
#now save all plants with less than 6 images in order to check them! 
#after this I would remove plants which have less than 5 images 
# Store groups where the count is <= 5
small_groups = {group: count for group, count in image_counts.items() if count <= 4}
print(small_groups)

large_groups = {group: count for group, count in image_counts.items() if count == 5}
print(large_groups)

largest_groups = {group: count for group, count in image_counts.items() if count >= 7}
print(largest_groups)

valid_groups = {group: count for group, count in image_counts.items() if count == 6}
#print(valid_groups)

clean_folder_path = "/projects/zumstego/1_datasets/images_no_bar_2023_clean/"

copy_images = False
if copy_images == True:
    # Move only valid images to the new clean folder
    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg"):
            group_key = "_".join(filename.split("_")[:3])  # Extract group name
            if group_key in valid_groups:  # Only move images from groups with exactly 6 images
                shutil.copy2(os.path.join(folder_path, filename), os.path.join(clean_folder_path, filename))
    #print("Filtered images moved to:", clean_folder_path)

##************************************************************************************************************
#remove entries from mapping_file: 
#14_10_3 is an  outlier!! Missing parts or spike! 

k2023 = False
k2024 = False

if k2023 == True:
    groups_to_remove = {
        '17_7_4', '9_7_10', '9_7_1',  # First group
        '4_6_7', '17_8_1', '15_6_7', '12_10_9', '12_9_3', '12_10_10',  # Second group
        '3_9_1', '11_6_2', '2_8_7', '7_10_1', '13_9_5', '21_11_6', '8_6_2', '21_12_3', 
        '4_10_5', '11_6_4', '14_8_1', '12_8_5', '16_10_4', '18_10_9', '2_10_6', '15_8_6', 
        '2_9_5', '23_14_6', '4_10_6', '6_6_2', '15_7_6', '19_7_2', '11_8_8', '17_7_10', '7_8_9', "14_10_3", "6_6_1", "4_10_6", "23_5_2", "19_8_1"
    }

if k2024 == True: 
    groups_to_remove = {
    '3_18_8', '3_19_8', '7_18_5', 
    '7_19_8', '7_19_6', '8_19_6', '8_19_7', '8_19_10', '8_19_8', '8_19_9', 
    '12_15_9', '16_17_6'
    }   
    
new_group = {'18_9_3', '8_7_1'}

remove_plants_from_csv_from_image_folder = True
if remove_plants_from_csv_from_image_folder == True:
    
    # Load your CSV file
    df = pd.read_csv("/projects/zumstego/1_datasets/images_no_bar_2023_clean/vol_mapping_cleaned.csv")

    # Extract the first three parts of img_name and check if they are in the removal list
    df_filtered = df[~df['img_name'].str.extract(r'(^\d+_\d+_\d+)')[0].isin(new_group)]


    # Remove fully duplicated rows from df_filtered
    df_filtered_cleaned = df_filtered.drop_duplicates()

    # Print the cleaned DataFrame
    print("final length")
    print(len(df_filtered_cleaned))
    
    #add _a to mapping file: 
    #df_filtered_cleaned['img_name'] = df_filtered_cleaned['img_name'].str.replace(r'(\.jpg)$', r'_a\1', regex=True)



    for filename in os.listdir(clean_folder_path):
        if filename.endswith(".jpg") and not filename.endswith("_a.jpg"):
            os.rename(
                os.path.join(clean_folder_path, filename),
                os.path.join(clean_folder_path, filename.replace(".jpg", "_a.jpg"))
            )

    
    # Get all image names from the folder
    existing_images = set(filename for filename in os.listdir(clean_folder_path) if filename.endswith(".jpg"))
    #print(existing_images)

    # Get all image names from the DataFrame
    df_image_names = set(df_filtered_cleaned['img_name'])
    print("image names from mapping")
    #print(df_image_names)

    # Find images in the DataFrame that are NOT in the folder
    missing_images = df_image_names - existing_images
    print("missing images")
    #print(missing_images)
    (print(len(missing_images)))
    
    #find rows in mapping file that dont have images: 
    # Find images in the DataFrame that are NOT in the folder
    missing_rows =  existing_images - df_image_names
    print("missing row entries")
    #print(missing_rows)
    (print(len(missing_rows)))
    
    #extract first three digits: 
    # Convert set to a DataFrame
    missing_images_list = list(missing_images)  # Convert set to list
    missing_images_df = pd.DataFrame(missing_images_list, columns=["img_name"])

    # Extract the first three digits (X_X_X)
    missing_plants = missing_images_df['img_name'].str.extract(r'(^\d+_\d+_\d+)')[0].unique()

    # Convert to a set to ensure uniqueness
    missing_plants = set(missing_plants)
    print("missing plants (images, rows that need to be removed)")
    print(missing_plants)
    
   
    df_filtered_missing = df_filtered_cleaned[~df_filtered_cleaned['img_name'].str.extract(r'(^\d+_\d+_\d+)')[0].isin(missing_plants)]

    # Save the cleaned dataframe
    df_filtered_missing.to_csv("/projects/zumstego/1_datasets/images_no_bar_2023_clean/vol_mapping_cleaned.csv", index=False)

    #remove specific images
    # Select groups that contain exactly 6 images
    #to_remove = groups_to_remove
    #print(to_remove)
    clean_folder_path_new = "/projects/zumstego/1_datasets/images_no_bar_2023_clean/"
    
    #find duplicated images
    # Get all image names in the folder
    image_files = [filename for filename in os.listdir(clean_folder_path_new) if filename.endswith(".jpg")]

    # Count occurrences of each filename
    image_counts = Counter(image_files)

    # Find duplicates (files appearing more than once)
    duplicates = [img for img, count in image_counts.items() if count > 1]

    # Print duplicate image names
    print("Duplicate Images:", duplicates)
    
    


    remove_images = True
    if remove_images == True: 
        
        # Move only valid images to the new clean folder
        for filename in os.listdir(clean_folder_path_new):
            if filename.endswith(".jpg"):
                group_key = "_".join(filename.split("_")[:3])  # Extract group name
                #if group_key in to_remove:  # Only remove images from groups with specific image name
                if group_key in new_group:
                    #os.remove(os.path.join(clean_folder_path_new, filename))
                    print(group_key)
            
            




check_images_availability = False
if check_images_availability == True: 
    found_images=[]
    
    for filename in os.listdir(clean_folder_path): 
        if filename.endswith("jpg"):
            group_key = "_".join(filename.split("_")[:3])
            if group_key in groups_to_remove:
                found_images.append(filename)
                
    if found_images:
        print("found images")
        for img in found_images:
            print(img)
    else: 
        print("no image found")
        
        
#compare two folders: 
compare_folders = False
if compare_folders == True:
    # Define folder paths
    folder1 = "/projects/zumstego/1_datasets/images_no_bar_2023_clean/"
    folder2 = "/projects/zumstego/1_datasets/images_no_bar_crop_2023_clean/"

    # Regex pattern to extract the first four numbers separated by "_"
    pattern = re.compile(r"^(\d+_\d+_\d+_\d+)")  # Matc

    # Function to extract the first 4 digits from a filename
    def extract_number(filename):
        match = pattern.match(filename)
        return match.group(1) if match else None

    # Get sets of extracted numbers from filenames
    numbers1 = {extract_number(f) for f in os.listdir(folder1) if extract_number(f)}
    numbers2 = {extract_number(f) for f in os.listdir(folder2) if extract_number(f)}

    # Find images only in folder1
    only_in_folder1 = sorted(numbers1 - numbers2)
    print(only_in_folder1)

    # Find images only in folder1
    #print("only in clean folder")
    only_in_folder2 = sorted(numbers2 - numbers1)
    print(only_in_folder2)

    # Regex to extract the first **three** numbers
    pattern_three_digits = re.compile(r"^(\d+_\d+_\d+)")

    # Step 1: Extract first three digits
    extracted_prefixes = [pattern_three_digits.match(prefix).group(1) for prefix in only_in_folder1]

    # Step 2: Filter for unique entries
    unique_prefixes = set(extracted_prefixes)
    print("extracted 3 digits")
    print(unique_prefixes)
    print(len(unique_prefixes))

    # Keep only the ones that are in groups_to_remove
    filtered_prefixes = unique_prefixes.intersection(groups_to_remove)
    # Print filtered images
    print("Filtered unique prefixes (not in groups_to_remove):")
    print(filtered_prefixes)
    print(len(filtered_prefixes))

    #not give me all that are not in groups to remove: 
    # Keep only the ones that are NOT in groups_to_remove
    not_in_groups = unique_prefixes - groups_to_remove

    # Print result
    print("Unique prefixes that are NOT in groups_to_remove:")
    print(not_in_groups)





