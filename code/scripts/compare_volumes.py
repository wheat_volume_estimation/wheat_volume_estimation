import click
import lib.stats_lib
import pandas as pd
import re

from icecream import ic
from pathlib import Path

@click.command()
@click.option("--volumes", type=str, required=True, help="Path to json file containing measured volumes")
@click.option("--predictions", type=str, default=None, help="Path to json file containing estimated volumes")
@click.option("--value-column", type=click.Choice(["volume", "total"], case_sensitive=False), default="volume", help="Define what to predict: volume or number of spikelets (total)")
@click.option("--baseline", is_flag = True, type=bool, help="Add if one file is the baseline file")
@click.option("--name", type=str, required=True, help="Name for image and statistic table to save")
@click.option("--output-folder", type=str, required=True, help="Folder in which statistics and images are saved")
def main(volumes : str, predictions : str | None, value_column : str, baseline : bool, name : str, output_folder : str):
    volumes = Path(volumes)
    value_column = value_column.lower()
    output_folder = Path(output_folder)
    output_folder.mkdir(parents=True, exist_ok=True)
    if not volumes.exists():
        print(f"Error file {volumes} not found")
    
    if predictions is not None:
        predictions = Path(predictions)
        if not predictions.exists():
            print(f"Error file {predictions} not found")
        else:
            df_true = pd.read_json(volumes, orient='index', convert_axes=False)
            df_estim = pd.read_json(predictions, orient='index', dtype={'plant_id' : str})
            lib.stats_lib.compare(mapping_true=df_true, mapping_predicted=df_estim, output_name=value_column, baseline=baseline, name=name, output_folder=output_folder)
    else:
        df_volumes = pd.read_json(volumes, orient='index', convert_axes=False)
        lib.stats_lib.volume_statistics(df_volumes, name=name, output_folder=output_folder)
        
if __name__ == "__main__":
    #main(true_volumes="data/vol_mapping.csv", estimations="data/baseline_volumes.csv")
    main()