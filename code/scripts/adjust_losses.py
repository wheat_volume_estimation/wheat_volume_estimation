import torch
import numpy as np
import os
import argparse
from icecream import ic

def adjust(path):

    y_all_mean = 4630 # FIXED on empirical values
    y_all_std = 1163 # FIXED on empiriccal values

    config = torch.load(path)

    train_loss = config['train_loss']
    new_train_loss = []
    
    for eps in train_loss:
        adj = np.sqrt((eps - y_all_mean)/y_all_std * 10)*y_all_std / 10
        new_train_loss.append(adj)

    config['train_loss'] = new_train_loss

    val_loss = config['val_loss']
    new_val_loss = []
    
    for eps in val_loss:
        adj = np.sqrt((eps - y_all_mean)/y_all_std * 10)*y_all_std / 10
        new_val_loss.append(adj)
    
    ic(new_val_loss)
    config['val_loss'] = new_val_loss
    
    #Add new entry with normalized and set it to false
    config['normalize'] = False 

    # Split the path into directory and filename
    directory, filename = os.path.split(path)

    # Append "_new" to the filename
    new_filename = os.path.splitext(filename)[0] + '_new' + os.path.splitext(filename)[1]

    # Construct the new file path
    new_path = os.path.join(directory, new_filename)

    torch.save(config, new_path)

def main():
        parser = argparse.ArgumentParser(description='Takes a .pth file and it corrects the train_loss and the val_loss values. Then a new file is created with "_new" added to the filename')
        parser.add_argument('--path', type=str, required=True, help='Path to the .pth file with the wrong cal and train losses')
        

        args = parser.parse_args()
        adjust(args.path)


