import data_lib.split_dataset as split_dataset
import numpy as np
import pandas as pd
import pytest

from icecream import ic
from pathlib import Path


class TestComputePlantMapping:
    """Test some usecases of the compute plant mapping method in the split_dataset.py file"""

    def test_compute_plant_mapping_single(self):

        data_in = [
                ['10_10_1_2_b.jpg', 5000],
               ]
        columns_in = ['img_name', 'volume']
        df_mapping = pd.DataFrame(data=data_in, columns=columns_in)
        test_out = split_dataset.compute_plant_mapping_(df_mapping=df_mapping, value_column='volume')
        data_out = [
                ['10_10_1', 5000.0, ['10_10_1_2_b.jpg'], [True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_expected_out = pd.DataFrame(data=data_out, columns=columns_in).set_index('index').rename_axis(None)
        assert df_expected_out.equals(test_out)

    def test_compute_plant_mapping_arti_only(self):

        data_in = [
                ['10_10_1_2_b.jpg', 5000],
                ['10_10_1_3_c.jpg', 5000],
                ['10_9_2_2_b.jpg', 4000],
                ['10_9_2_3_c.jpg', 4000],
                ['8_9_2_2_b.jpg', 3000],
                ['8_9_2_3_c.jpg', 3000],
               ]
        columns_in = ['img_name', 'volume']
        df_mapping = pd.DataFrame(data=data_in, columns=columns_in)
        test_out = split_dataset.compute_plant_mapping_(df_mapping=df_mapping, value_column='volume')
        data_out = [
                [ '10_10_1', 5000.0, ['10_10_1_2_b.jpg', '10_10_1_3_c.jpg'], [True, True]],
                [ '10_9_2', 4000.0, ['10_9_2_2_b.jpg', '10_9_2_3_c.jpg'], [True, True]],
                [ '8_9_2', 3000.0, ['8_9_2_2_b.jpg', '8_9_2_3_c.jpg'], [True, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_expected_out = pd.DataFrame(data=data_out, columns=columns_in).set_index('index').rename_axis(None)
        assert df_expected_out.equals(test_out)

    def test_compute_plant_mapping_orig_only(self):

        data_in = [
                ['10_10_1_2_a.jpg', 5000],
                ['10_10_1_3_a.jpg', 5000],
                ['10_9_2_2_a.jpg', 4000],
                ['10_9_2_3_a.jpg', 4000],
                ['8_9_2_2_a.jpg', 3000],
                ['8_9_2_3_a.jpg', 3000],
               ]
        columns_in = ['img_name', 'volume']
        df_mapping = pd.DataFrame(data=data_in, columns=columns_in)
        test_out = split_dataset.compute_plant_mapping_(df_mapping=df_mapping, value_column='volume')
        data_out = [
                [ '10_10_1', 5000.0, ['10_10_1_2_a.jpg', '10_10_1_3_a.jpg'], [False, False]],
                [ '10_9_2', 4000.0, ['10_9_2_2_a.jpg', '10_9_2_3_a.jpg'], [False, False]],
                [ '8_9_2', 3000.0, ['8_9_2_2_a.jpg', '8_9_2_3_a.jpg'], [False, False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_expected_out = pd.DataFrame(data=data_out, columns=columns_in).set_index('index').rename_axis(None)
        assert df_expected_out.equals(test_out)

    def test_compute_plant_mapping_mixed(self):

        data_in = [
                ['10_10_1_2_b.jpg', 5000],
                ['10_10_1_3_a.jpg', 5000],
                ['10_9_2_2_c.jpg', 4000],
                ['10_9_2_3_a.jpg', 4000],
                ['8_9_2_2_c.jpg', 3000],
                ['8_9_2_3_a.jpg', 3000],
               ]
        columns_in = ['img_name', 'volume']
        df_mapping = pd.DataFrame(data=data_in, columns=columns_in)
        test_out = split_dataset.compute_plant_mapping_(df_mapping=df_mapping, value_column='volume')
        data_out = [
                ['10_10_1', 5000.0, ['10_10_1_2_b.jpg', '10_10_1_3_a.jpg'], [True, False]],
                ['10_9_2', 4000.0, ['10_9_2_2_c.jpg', '10_9_2_3_a.jpg'], [True, False]],
                ['8_9_2', 3000.0, ['8_9_2_2_c.jpg', '8_9_2_3_a.jpg'], [True, False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_expected_out = pd.DataFrame(data=data_out, columns=columns_in).set_index('index').rename_axis(None)
        assert df_expected_out.equals(test_out)

    def test_compute_plant_mapping_mixed_different_size(self):

        data_in = [
                ['10_10_1_2_b.jpg', 5000],
                ['10_10_1_3_a.jpg', 5000],
                ['10_10_1_4_b.jpg', 5000],
                ['10_9_2_2_c.jpg', 4000],
                ['10_9_2_3_a.jpg', 4000],
               ]
        columns_in = ['img_name', 'volume']
        df_mapping = pd.DataFrame(data=data_in, columns=columns_in)
        test_out = split_dataset.compute_plant_mapping_(df_mapping=df_mapping, value_column='volume')
        data_out = [
                ['10_10_1', 5000.0, ['10_10_1_2_b.jpg', '10_10_1_3_a.jpg', '10_10_1_4_b.jpg'], [True, False, True]],
                ['10_9_2', 4000.0, ['10_9_2_2_c.jpg', '10_9_2_3_a.jpg'], [True, False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_expected_out = pd.DataFrame(data=data_out, columns=columns_in).set_index('index').rename_axis(None)
        assert df_expected_out.equals(test_out)
    
    
class TestSplit_:
    """Some tests for the Split method in the split dataset method"""

    def test_split_10(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_2', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_3', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_4', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_5', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_6', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_7', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_8', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_9', 5000.0, ['2_6_1_0.jpg'], [False]],
                ['2_6_10', 5000.0, ['2_6_1_0.jpg'], [False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        train, val, test = split_dataset.split_(plant_mapping=df_in,
                                       rel_test_set_size=0.2,
                                       rel_val_set_size=0.1,
                                       random_generator=np.random.default_rng(0),
                                       verbose=True,
                                    )

        assert tuple(train.shape) == (7, 3), "Wrong train size."
        assert tuple(val.shape) == (1,3), "Wrong validation size."
        assert tuple(test.shape) == (2,3), "Wrong test size."

        assert np.intersect1d(train.index, val.index).shape[0] == 0, "Some plants are in the train set as well as in the val set."
        assert np.intersect1d(train.index, test.index).shape[0] == 0, "Some plants are in the train set as well as in the test set."
        assert np.intersect1d(val.index, test.index).shape[0] == 0, "Some plants are in the val set as well as in the test set."

    def test_split_remove_arti(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_2', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_3', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_4', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_5', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_6', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_7', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_8', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_9', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_10', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        train, val, test = split_dataset.split_(plant_mapping=df_in,
                                       rel_test_set_size=0.2,
                                       rel_val_set_size=0.1,
                                       random_generator=np.random.default_rng(0)
                                    )
        assert tuple(train.shape) == (7, 3), "Wrong train size."
        assert tuple(val.shape) == (1,3), "Wrong validation size."
        assert tuple(test.shape) == (2,3), "Wrong test size."

        assert np.intersect1d(train.index, val.index).shape[0] == 0, "Some plants are in the train set as well as in the val set."
        assert np.intersect1d(train.index, test.index).shape[0] == 0, "Some plants are in the train set as well as in the test set."
        assert np.intersect1d(val.index, test.index).shape[0] == 0, "Some plants are in the val set as well as in the test set."
        
        assert len(test.iloc[0,1]) == 1, "Did not remove artificial image in test"
        assert len(test.iloc[1,1]) == 1, "Did not remove artificial image in test"
        assert len(val.iloc[0,1]) == 1, "Did not remove artificial image in val"
        assert len(train.iloc[0,1]) == 2, "Did remove artificial images form training"

    def test_split_all_arti(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_2', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_3', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_4', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_5', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_6', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_7', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_8', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_9', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_10', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        train, val, test = split_dataset.split_(plant_mapping=df_in,
                                       rel_test_set_size=0.2,
                                       rel_val_set_size=0.1,
                                       random_generator=np.random.default_rng(0)
                                    )
        assert tuple(train.shape) == (7, 3), "Wrong train size."
        assert tuple(val.shape) == (1,3), "Wrong validation size."
        assert tuple(test.shape) == (2,3), "Wrong test size."

        assert np.intersect1d(train.index, val.index).shape[0] == 0, "Some plants are in the train set as well as in the val set."
        assert np.intersect1d(train.index, test.index).shape[0] == 0, "Some plants are in the train set as well as in the test set."
        assert np.intersect1d(val.index, test.index).shape[0] == 0, "Some plants are in the val set as well as in the test set."
        
        assert len(test.iloc[0,1]) == 2, "Did remove artificial image in test"
        assert len(test.iloc[1,1]) == 2, "Did remove artificial image in test"
        assert len(val.iloc[0,1]) == 2, "Did remove artificial image in val"
        assert len(train.iloc[0,1]) == 2, "Did remove artificial images in training"
        
    def test_split_too_little_orig(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_2', 5000.0, ['2_6_1_0.jpg', 'arti'], [False, True]],
                ['2_6_3', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_4', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_5', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_6', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_7', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_8', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_9', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
                ['2_6_10', 5000.0, ['2_6_1_0.jpg', 'arti'], [True, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        train, val, test = split_dataset.split_(plant_mapping=df_in,
                                       rel_test_set_size=0.2,
                                       rel_val_set_size=0.1,
                                       random_generator=np.random.default_rng(0)
                                    )
        assert tuple(train.shape) == (8, 3), "Wrong train size."
        assert tuple(val.shape) == (1,3), "Wrong validation size."
        assert tuple(test.shape) == (1,3), "Wrong test size."

        assert np.intersect1d(train.index, val.index).shape[0] == 0, "Some plants are in the train set as well as in the val set."
        assert np.intersect1d(train.index, test.index).shape[0] == 0, "Some plants are in the train set as well as in the test set."
        assert np.intersect1d(val.index, test.index).shape[0] == 0, "Some plants are in the val set as well as in the test set."
        
if __name__ == "__main__":
    test = TestSplit_()
    test.test_split_10()