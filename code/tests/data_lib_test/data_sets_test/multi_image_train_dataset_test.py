import data_lib.data_sets as data_sets
import numpy as np
import pandas as pd
import pytest

from pathlib import Path


class TestGetItem:
    """Some tests for the get item method in the multi image train dataset"""

    def test_getitem_single(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg'], [True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        image_dir = Path(__file__).parent / "test_imgs"
        df_in_file = image_dir / "single_test.json"
        df_in.to_json(df_in_file, orient='index')
        dataset = data_sets.MultiImageTrainDataset(image_dir=image_dir,
                                         plant_mapping_path=df_in_file,
                                         value_column='volume',
                                         max_seq_len=1,
                                         min_seq_len=1,
                                         random_seq_len=True,
                                         rotate=True,
                                         Y_SCALE=2500,
                                         Y_SHIFT=2500,
                                        )
        out = dataset[0]
        assert tuple(out[0].shape) == (1, 3, 256, 256)
        assert tuple(out[2].shape) == tuple([1])
        assert out[1] == 1

    def test_getitem_multiple(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', '2_6_1_1.jpg'], [True,False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        image_dir = Path(__file__).parent / "test_imgs"
        df_in_file = image_dir / "multi_test.json"
        df_in.to_json(df_in_file, orient='index')
        dataset = data_sets.MultiImageTrainDataset(image_dir=image_dir,
                                         plant_mapping_path=df_in_file,
                                         value_column='volume',
                                         max_seq_len=2,
                                         min_seq_len=2,
                                         random_seq_len=True,
                                         rotate=True,
                                         Y_SCALE=2500,
                                         Y_SHIFT=2500,
                                        )
        out = dataset[0]
        assert tuple(out[0].shape) == (2, 3, 256, 256)
        assert tuple(out[2].shape) == tuple([2])
        assert out[1] == 1

    def test_getitem_multiple_random(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', '2_6_1_1.jpg'], [True,False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        image_dir = Path(__file__).parent / "test_imgs"
        df_in_file = image_dir / "multi_test.json"
        df_in.to_json(df_in_file, orient='index')
        dataset = data_sets.MultiImageTrainDataset(image_dir=image_dir,
                                         plant_mapping_path=df_in_file,
                                         value_column='volume',
                                         max_seq_len=2,
                                         min_seq_len=1,
                                         random_seq_len=True,
                                         rotate=True,
                                         Y_SCALE=2500,
                                         Y_SHIFT=2500,
                                         random_generator=np.random.default_rng(10)
                                        )
        out = dataset[0]
        assert tuple(out[0].shape) == (1, 3, 256, 256)
        assert tuple(out[2].shape) == tuple([2])
        assert out[1] == 1

    def test_getitem_missing_img(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', '2_6_1_1.jpg', '2_6_1_6.jpg'], [True,False, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        image_dir = Path(__file__).parent / "test_imgs"
        df_in_file = image_dir / "multi_test.json"
        df_in.to_json(df_in_file, orient='index')
        dataset = data_sets.MultiImageTrainDataset(image_dir=image_dir,
                                         plant_mapping_path=df_in_file,
                                         value_column='volume',
                                         max_seq_len=3,
                                         min_seq_len=3,
                                         random_seq_len=True,
                                         rotate=True,
                                         Y_SCALE=2500,
                                         Y_SHIFT=2500,
                                        )
        out = dataset[0]
        assert tuple(out[0].shape) == (3, 3, 256, 256)
        assert tuple(out[2].shape) == tuple([3])
        assert out[1] == 1

    def test_exceptions(self):

        data_in = [
                ['2_6_1', 5000.0, ['2_6_1_0.jpg', '2_6_1_1.jpg', '2_6_1_6.jpg'], [True,False, True]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        image_dir = Path(__file__).parent / "test_imgs"
        df_in_file = image_dir / "multi_test.json"
        df_in.to_json(df_in_file, orient='index')
        try:
            dataset = data_sets.MultiImageTrainDataset(image_dir="",
                                            plant_mapping_path=df_in_file,
                                            value_column='volume',
                                            max_seq_len=3,
                                            min_seq_len=4,
                                            random_seq_len=True,
                                            rotate=True,
                                            Y_SCALE=2500,
                                            Y_SHIFT=2500,
                                            )
            assert False, "Index error was not thrown!"
        except IndexError:
            pass

        try:
            dataset = data_sets.MultiImageTrainDataset(image_dir="/alsdfjakldsj",
                                            plant_mapping_path=df_in_file,
                                            value_column='volume',
                                            max_seq_len=2,
                                            min_seq_len=2,
                                            random_seq_len=True,
                                            rotate=True,
                                            Y_SCALE=2500,
                                            Y_SHIFT=2500,
                                            )
            assert False, "File Exists error was not thrown!"
        except FileExistsError:
            pass
        
        
if __name__ == "__main__":
    test = TestGetItem()
    test.test_getitem_single()
    test.test_getitem_multiple()
    test.test_getitem_multiple_random()