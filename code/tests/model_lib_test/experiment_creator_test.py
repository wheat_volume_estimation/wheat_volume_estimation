import model_lib.inferrer as inferrer
import numpy as np
import pandas as pd
import pytest

from icecream import ic
from pathlib import Path

class TestSingleExperimentCreator:
    
    def test_single_experiment_creator_keep_arti(self):

        data_in = [
                ['10_10_1', 5000.0, ['10_10_1_2_b.jpg', '10_10_1_3_a.jpg'], [True, False]],
                ['10_9_2', 4000.0, ['10_9_2_2_c.jpg', '10_9_2_3_a.jpg'], [True, False]],
                ['8_9_2', 3000.0, ['8_9_2_2_c.jpg', '8_9_2_3_a.jpg'], [True, False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        
        experiment_creator = inferrer.SingleImageExperimentCreator(ignore_artificial=False)
        experiment = experiment_creator.create_experiment(df_in)

        expected_experiment = [
                ['10_10_1', ['10_10_1_2_b.jpg']],
                ['10_10_1', ['10_10_1_3_a.jpg']],
                ['10_9_2', ['10_9_2_2_c.jpg']],
                ['10_9_2', ['10_9_2_3_a.jpg']],
                ['8_9_2', ['8_9_2_2_c.jpg']],
                ['8_9_2',  ['8_9_2_3_a.jpg']],
               ]
        columns_expected = ['plant_id', 'images']
        expected_experiment = pd.DataFrame(data=expected_experiment, columns=columns_expected)
        
        assert expected_experiment.equals(experiment)

    def test_single_experiment_creator_no_arti(self):

        data_in = [
                ['10_10_1', 5000.0, ['10_10_1_2_b.jpg', '10_10_1_3_a.jpg'], [True, False]],
                ['10_9_2', 4000.0, ['10_9_2_2_c.jpg', '10_9_2_3_a.jpg'], [True, False]],
                ['8_9_2', 3000.0, ['8_9_2_2_c.jpg', '8_9_2_3_a.jpg'], [True, False]],
               ]
        columns_in = ['index', 'volume', 'images', 'artificial_mask']
        df_in = pd.DataFrame(data=data_in, columns=columns_in).set_index('index').rename_axis(None)
        
        experiment_creator = inferrer.SingleImageExperimentCreator(ignore_artificial=True)
        experiment = experiment_creator.create_experiment(df_in)

        expected_experiment = [
                ['10_10_1', ['10_10_1_3_a.jpg']],
                ['10_9_2', ['10_9_2_3_a.jpg']],
                ['8_9_2',  ['8_9_2_3_a.jpg']],
               ]
        columns_expected = ['plant_id', 'images']
        expected_experiment = pd.DataFrame(data=expected_experiment, columns=columns_expected)
        
        ic(experiment)
        ic(expected_experiment)
        assert expected_experiment.equals(experiment)
        
class TestInferrer:
    pass