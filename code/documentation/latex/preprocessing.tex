\section{Bar Removal}
\label{bar_removal}

When the pictures of wheat spikes in the lab were taken, the spikes were
fixated with two wooden bars (see \cref{fig:cellphone_image})
for better control over the exact angle
of the picture.

To enhance the quality of the images, with the outlook
for the application in the field, we want get rid of the
bars.

Moreover, with regards to depth maps and possible
subsequent 3D reconstruction, the removal of the bars
becomes indispensable.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{figures/preprocessing/21_2_1_3}}
\caption{Cellphone image (21\_2\_1\_3.jpg) of wheat spike, with wooden
bars for fixation.}
\label{fig:cellphone_image}
\end{center}
\vskip -0.2in
\end{figure}

To remove the bars we generally have two steps:
\begin{enumerate}
    \item Detect and mask bars in the image.
    \item Remove and inpaint image or estimated cut out volume
        after editing 3 dimensional reconstruction.
\end{enumerate}

\subsection{Detect and Mask}\label{sec:prep.dect_mask}

\subsubsection{Color range detection}
The first approach chosen for this was purely
algorithmic using color range detection.
The code for this can be found in the file
\emph{src/lib/obj\_remove.py} which was added in commit
\emph{aeca9c6faf1a439f58683a3e949f1124fa7a5fc8}.
Note that this file was removed in later commits.

The main drawback of this method is its dependence on
the light setting. Unfortunately the light setting
is not consistent over all the training data, which
makes this approach fail for a small subset of samples.
A non conclusive list of such images are:
\begin{itemize}
    \item 21\_5\_5\_6.jpg
    \item 21\_14\_4\_4.jpg
    \item 21\_15\_3\_2.jpg
    \item 21\_15\_4\_7.jpg
    \item 23\_5\_3\_2.jpg
    \item 23\_5\_3\_3.jpg
    \item 23\_5\_4\_3.jpg
    \item 23\_5\_5\_6.jpg
    \item 23\_6\_4\_6.jpg
    \item 23\_14\_4\_2.jpg
    \item 23\_14\_4\_3.jpg
\end{itemize}
At this point it's worth to point out that these experiments
were only conducted on a subset of all the available data.
More specifically only about 10 percent were included.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{figures/preprocessing/light_setting_1}}
\caption{Cellphone image (23\_14\_4\_3.jpg) in cold artificial light setting.}
\label{fig:cellphone_image_bright}
\end{center}
\vskip -0.2in
\end{figure}

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{figures/preprocessing/light_setting_2}}
\caption{Cellphone image (23\_5\_3\_2.jpg) in warm artificial light setting.}
\label{fig:cellphone_image_dark}
\end{center}
\vskip -0.2in
\end{figure}

The different light settings can be seen by comparing
\cref{fig:cellphone_image_bright} and \cref{fig:cellphone_image_dark}
in the first first setting of the two, the image was taken with
cold, direct artificial light, whereas the second was take with
warm artificial light. The important detail for the task at hand
is how this affects the tone of the fixating bars.

The algorithm base on a color range for detecting these bars hence
fails as we have to adapt it on the most usual light setting which
is represented by \cref{fig:cellphone_image}.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/preprocessing/bar_det_warm.jpg}}
\caption{Bar detection with color range algorithm on 23\_5\_3\_2.jpg in 
    warm artificial light setting. 
    White area depicts the area recognised
    as bars by the algorithm.}
\label{fig:bar_detection_warm}
\end{center}
\vskip -0.2in
\end{figure}

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/preprocessing/bar_det_usual}}
\caption{Bar detection with color range algorithm on 21\_2\_1\_2.jpg in 
    usual light setting.
    White area depicts the area recognised
    as bars by the algorithm.}
\label{fig:bar_detection_usual}
\end{center}
\vskip -0.2in
\end{figure}
Detecting and removing bars for usual light setting works quite
reliably as represented in \cref{fig:bar_detection_usual} but in the warm
setting it then fails \cref{fig:bar_detection_warm}.

A better, but more computationally intensive method is presented in the next
section.

\subsubsection{Segment Anything}

The segment anything model developed by
%end of this document give examples for journal articles \yrcite{Samuel59},
\cite{kirillov_segment_2023}. Using it's pre-trained
weights, we were able to get almost perfect bar recognition for
all the light settings. Segment anything provides an API which lets
the caller specify a point on the object which has to be segmented.
So with the use of this algorithm the problem reduces form
from matching all points corresponding to a bar to finding one
point on each bar.

To achieve this, we used the knowledge that the bars are always
in the region of the image, so we can identify one
point on the bars by picking the
darkest point in the region, where the bar is expected.

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/preprocessing/sam_warm}}
\caption{Bar detection with segment anything on 23\_5\_3\_2.jpg in
    warm artificial light setting.
    White area depicts the area recognised
    as bars by the algorithm.}
\label{fig:sam_warm}
\end{center}
\vskip -0.2in
\end{figure}

This approach, as depicted in \cref{fig:sam_warm}, then fixes the problems
we initially had for the bar detection
up to some small artefacts in some images which can be removed easily with
some iterations of opening and closing (erosion followed by dilation /
dilation followed by erosion).

\subsection{Inpaining}

After removing the bars we want to get a photorealistic image back
which only contains the spike and which is not changed
in the rest of the image. For this we use a reimplementation
in using \cite{paszke_pytorch_2019} of the paper by
\cite{yu_free-form_2019}.

The results then look very promising:
\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/preprocessing/no_bar_21_5_5_6}}
\caption{Inpainted wheat spike on 21\_5\_5\_6.jpg. The original image
contained two bars which were segmented with segment anything subsequently
inpainted with deepfill.}
\label{fig:deepfill_proising}
\end{center}
\vskip -0.2in
\end{figure}

Although in most images it works almost perfectly, there are still
some cases, in which we recognise some artefacts. It is to point out
tough that the whet spike itself is inpainted very realistically,
the only downside seems to be with the two bars. So the hope is to
fix that with broader masking.
\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[angle=90,width=\columnwidth]{figures/preprocessing/no_bar_23_14_4_2}}
\caption{Inpainting on 23\_14\_4\_2.jpg with some artefacts.}
\label{fig:deepfill_artefacts}
\end{center}
\vskip -0.2in
\end{figure}

\subsubsection{Some remarks on stronger models}

We also tested more powerful diffusion models for inpainting.
For example we conducted experiments with network and weights
by \cite{rombach_high-resolution_2022}.
The expressive power of these method is astonishing but
this comes with the downside of hight inference coasts. Due
to the limited resources and time, we then stuck with
the models by
\cite{yu_free-form_2019}.

