# Data Folder

This folder must be ignored by git. As the images take up a lot
of space we want to keep them in here and not track them.
Moreover it is an extra protection against data leakage if
we only store the data locally.

As a drawback we will have to add this folder to the
cluster manually.
