# Report Folder

This folder contains the submission report folder
with all tex documents, and plots to include in it.
Moreover we can also store the reference papers in here
to have them available at all time.
